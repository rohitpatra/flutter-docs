import React, { Suspense } from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { makeStyles } from "@material-ui/core/styles";
import routes from './routes';

import {
  Drawer, List, ListItem,
  ListItemIcon, ListItemText,
  Container, Typography,
} from "@material-ui/core";
import { NotFound } from './ui/notfound';

const useStyles = makeStyles((theme) => ({
  drawerPaper: { width: 'inherit' },
  link: {
    textDecoration: 'none',
    color: theme.palette.text.primary
  }
}))
const NotFoundComponent = React.lazy(() => import('./ui/notfound'));


function App() {
  const classes = useStyles();
  const routePaths = routes.map((route, index) => {
    return (route.component) ? (
      <Route
        key={index}
        path={route.path}
        exact={route.exact}
        name={route.name}
        render={props => (
          <route.component {...props} />
        )}
      />
    ) : (NotFoundComponent);
  });
  return (
    <div className="p-5">
      <Router>
        <div style={{ display: 'flex' }}>
          <Drawer
            style={{ width: '220px' }}
            variant="persistent"
            anchor="left"
            open={true}
            classes={{ paper: classes.drawerPaper }}
        >
            <List>
              <Link to="/BottomNavBar" className={classes.link}>
                <ListItem button>
                  <ListItemText primary={"BottomNavBar"} />
                </ListItem>
              </Link>
              <Link to="/DateTime" className={classes.link}>
                <ListItem button>
                  <ListItemText primary={"DateTime"} />
                </ListItem>
              </Link>
              <Link to="/Drawer" className={classes.link}>
                <ListItem button>
                  <ListItemText primary={"Drawer"} />
                </ListItem>
              </Link>
              <Link to="/SliverAppBar" className={classes.link}>
                <ListItem button>
                  <ListItemText primary={"SliverAppBar"} />
                </ListItem>
              </Link>
              <Link to="/TabBar" className={classes.link}>
                <ListItem button>
                  <ListItemText primary={"TabBar"} />
                </ListItem>
              </Link>
              <Link to="/Dropdown" className={classes.link}>
                <ListItem button>
                  <ListItemText primary={"Dropdown"} />
                </ListItem>
              </Link>
              <Link to="/MaterialButtons" className={classes.link}>
                <ListItem button>
                  <ListItemText primary={"MaterialButtons"} />
                </ListItem>
              </Link>
              <Link to="/OutlineButtons" className={classes.link}>
                <ListItem button>
                  <ListItemText primary={"OutlineButtons"} />
                </ListItem>
              </Link>
              <Link to="/PopUpMenuButtons" className={classes.link}>
                <ListItem button>
                  <ListItemText primary={"PopUpMenuButtons"} />
                </ListItem>
              </Link>
              <Link to="/FloatingActionButtons" className={classes.link}>
                <ListItem button>
                  <ListItemText primary={"FloatingActionButtons"} />
                </ListItem>
              </Link>
            </List>
          </Drawer>
        </div>
        <Suspense fallback={NotFoundComponent}>
          <Switch>
            <div style={{ textAlign: 'center', paddingLeft: '20em' }}>
              {routePaths}
            </div>
          </Switch>
        </Suspense>
      </Router>
    </div>
  );
}

export default App;
