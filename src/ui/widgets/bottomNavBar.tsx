import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import bnb_1 from "../../assets/BottomNavBar/bnb_1.png";
import bnb_2 from "../../assets/BottomNavBar/bnb_2.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const styles = {
  width: "25%",
};

const BottomNavBar = (props: Props) => {
  return (
    <>
      <h2>
        <u>Bottom Navigation Bar</u>
      </h2>
      <img src={bnb_1} alt="bnb_1" style={{ width: "40%" }} />

      <h5>
        A bottom navigation bar is usually used in conjunction with a Scaffold.
      </h5>
      <h5>
        Bottom navigation bar consists of multiple items in the form of text
        labels, icons, or both.
      </h5>
      <h5>This example consists of icons and label both.</h5>
      <h5>Bottom Navigation type is Fixed (Default Type).</h5>
      <h5>Use when there are less than five items .</h5>

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            bottomNavigationBar: BottomNavigationBar( onTap: (int index)
            &rbrace; setState(() &rbrace; currentIndex1 = index; &rbrace;);
            &rbrace;, currentIndex: currentIndex1, type:
            BottomNavigationBarType.fixed, selectedItemColor: iconColor,
            unselectedItemColor: textSecondaryColor, backgroundColor: Color,
            items: [ BottomNavigationBarItem( icon: Image.asset( 'images.png',
            height: 25, width: 25, color: iconSecondaryColor), activeIcon:
            Image.asset( 'images.png', height: 25, width: 25, color: iconColor),
            title: Text('Home'), backgroundColor: Color, ),
            BottomNavigationBarItem( icon: Image.asset( 'images.png', height:
            25, width: 25, color: iconSecondaryColor), activeIcon: Image.asset(
            'images.png', height: 25, width: 25, color: iconColor), title:
            Text('Reels'), ), BottomNavigationBarItem( icon: Image.asset(
            'images.png', height: 25, width: 25, color: iconSecondaryColor),
            activeIcon: Image.asset( 'images.png', height: 25, width: 25, color:
            iconColor), title: Text('Gallery'), ), BottomNavigationBarItem(
            icon: Image.asset( 'images.png', height: 25, width: 25, color:
            iconSecondaryColor), activeIcon: Image.asset( 'images.png', height:
            25, width: 25, color: iconColor), title: Text('Activity'), ),
            BottomNavigationBarItem( icon: Container( decoration: BoxDecoration(
            shape: BoxShape.circle, border: Border.all(color:
            iconSecondaryColor, width: 1)), child: Image.asset( 'images.png',
            height: 30, width: 30) .cornerRadiusWithClipRRect(20), ),
            activeIcon: Container( decoration: BoxDecoration( shape:
            BoxShape.circle, border: Border.all(color: iconColor, width: 1)),
            child: Image.asset( 'images.png', height: 30, width: 30)
            .cornerRadiusWithClipRRect(20), ), title: Text('Profile'), ), ], ),
          </SyntaxHighlighter>
        </Typography>
      </Container>
      <br />
      <h5>
        1. For this Navigation Bar an integer currentIndex1 is initialized to
        zero and passed to onTap props using a setState method
      </h5>

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <div
            style={{
              background: "#ffffff",
              overflow: "auto",
              width: "auto",
              border: "solid gray",
              borderWidth: ".1em .1em .1em .8em",
              padding: ".2em .6em",
            }}
          >
            <pre style={{ margin: 0, lineHeight: "125%" }}>
              <span style={{ color: "#333399", fontWeight: "bold" }}>int</span>{" "}
              currentIndex1 <span style={{ color: "#333333" }}>=</span>{" "}
              <span style={{ color: "#6600EE", fontWeight: "bold" }}>0</span>;
              {"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                bottomNavigationBar:
              </span>{" "}
              BottomNavigationBar({"\n"}
              {"        "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                onTap:
              </span>{" "}
              (<span style={{ color: "#333399", fontWeight: "bold" }}>int</span>{" "}
              index) {"{"}
              {"\n"}
              {"          "}setState(() {"{"}
              {"\n"}
              {"            "}currentIndex1{" "}
              <span style={{ color: "#333333" }}>=</span> index;{"\n"}
              {"          "}
              {"}"});{"\n"}
              {"        "}
              {"}"},{"\n"});{"\n"}
            </pre>
          </div>
          ;
        </Typography>
      </Container>
      <hr />

      <img src={bnb_2} alt="bnb_2" style={{ width: "40%" }} />

      <h5>
        A bottom navigation bar is usually used in conjunction with a Scaffold.
      </h5>
      <h5>
        Bottom navigation bar consists of multiple items in the form of text
        labels, icons, or both.
      </h5>
      <h5>
        This example consists of only icons and label but only the selected item
        shows the label.
      </h5>
      <h5>Bottom Navigation type is shifting.</h5>
      <h5>Use when there are less than five items .</h5>

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            bottomNavigationBar: BottomNavigationBar( onTap: (int index)
            &rbrace; setState(() &rbrace; currentIndex1 = index; &rbrace;);
            &rbrace;, currentIndex: currentIndex1, type:
            BottomNavigationBarType.shifting, selectedItemColor:
            appStore.iconColor, unselectedItemColor:
            appStore.textSecondaryColor, showSelectedLabels: true,
            showUnselectedLabels: false, items: [ BottomNavigationBarItem( icon:
            Image.asset(
            'images/widgets/materialWidgets/mwAppStructureWidgets/BottomNavigation/home.png',
            height: 25, width: 25, color: appStore.iconSecondaryColor),
            activeIcon: Image.asset(
            'images/widgets/materialWidgets/mwAppStructureWidgets/BottomNavigation/home_fill.png',
            height: 25, width: 25, color: appStore.iconColor), title:
            Text('Home'), ), BottomNavigationBarItem( icon: Image.asset(
            'images/widgets/materialWidgets/mwAppStructureWidgets/BottomNavigation/reel.png',
            height: 25, width: 25, color: appStore.iconSecondaryColor),
            activeIcon: Image.asset(
            'images/widgets/materialWidgets/mwAppStructureWidgets/BottomNavigation/reel_fill.png',
            height: 25, width: 25, color: appStore.iconColor), title:
            Text('Reels'), ), BottomNavigationBarItem( icon: Image.asset(
            'images/widgets/materialWidgets/mwAppStructureWidgets/BottomNavigation/gallery.png',
            height: 25, width: 25, color: appStore.iconSecondaryColor),
            activeIcon: Image.asset(
            'images/widgets/materialWidgets/mwAppStructureWidgets/BottomNavigation/gallery_fill.png',
            height: 25, width: 25, color: appStore.iconColor), title:
            Text('Gallery'), ), BottomNavigationBarItem( icon: Image.asset(
            'images/widgets/materialWidgets/mwAppStructureWidgets/BottomNavigation/heart.png',
            height: 25, width: 25, color: appStore.iconSecondaryColor),
            activeIcon: Image.asset(
            'images/widgets/materialWidgets/mwAppStructureWidgets/BottomNavigation/heart_fill.png',
            height: 25, width: 25, color: appStore.iconColor), title:
            Text('Activity'), ), BottomNavigationBarItem( icon: Container(
            decoration: BoxDecoration( shape: BoxShape.circle, border:
            Border.all( color: appStore.iconSecondaryColor, width: 1)), child:
            Image.asset(
            'images/widgets/materialWidgets/mwAppStructureWidgets/BottomNavigation/profile1.png',
            height: 30, width: 30) .cornerRadiusWithClipRRect(20), ),
            activeIcon: Container( decoration: BoxDecoration( shape:
            BoxShape.circle, border: Border.all(color: appStore.iconColor,
            width: 1)), child: Image.asset(
            'images/widgets/materialWidgets/mwAppStructureWidgets/BottomNavigation/profile1.png',
            height: 30, width: 30) .cornerRadiusWithClipRRect(20), ), title:
            Text('Profile'), ), ], ),
          </SyntaxHighlighter>
        </Typography>
      </Container>

      <br />
      <h5>
        1. For this Navigation Bar an integer currentIndex1 is initialized to
        zero and passed to onTap props using a setState method
      </h5>
      <h5>
        2. Set type props to BottomNavigationBarType.shifting to get shift
        labels and icons.
      </h5>

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <div
            style={{
              background: "#ffffff",
              overflow: "auto",
              width: "auto",
              border: "solid gray",
              borderWidth: ".1em .1em .1em .8em",
              padding: ".2em .6em",
            }}
          >
            <pre style={{ margin: 0, lineHeight: "125%" }}>
              <span style={{ color: "#333399", fontWeight: "bold" }}>int</span>{" "}
              currentIndex1 <span style={{ color: "#333333" }}>=</span>{" "}
              <span style={{ color: "#6600EE", fontWeight: "bold" }}>0</span>;
              {"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                bottomNavigationBar:
              </span>{" "}
              BottomNavigationBar({"\n"}
              {"        "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                onTap:
              </span>{" "}
              (<span style={{ color: "#333399", fontWeight: "bold" }}>int</span>{" "}
              index) {"{"}
              {"\n"}
              {"          "}setState(() {"{"}
              {"\n"}
              {"            "}currentIndex1{" "}
              <span style={{ color: "#333333" }}>=</span> index;{"\n"}
              {"          "}
              {"}"});{"\n"}
              {"        "}
              {"}"},{"\n"}
              {"        "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                currentIndex:
              </span>{" "}
              currentIndex1,{"\n"}
              {"        "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                type:
              </span>{" "}
              BottomNavigationBarType.shifting,{"\n"});{"\n"}
            </pre>
          </div>
          ;
        </Typography>
      </Container>
    </>
  );
};

export default BottomNavBar;
