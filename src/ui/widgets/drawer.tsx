import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Drawer_1 from "../../assets/Drawer/Drawer_1.png";
import Drawer_2 from "../../assets/Drawer/Drawer_2.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const Drawer = (props: Props) => {
  return (
    <>
      <div className="container m-0">
        <div className="row">
          <div className="col-lg-6">
            <h2>
              <u>Drawer</u>
            </h2>
            <img src={Drawer_1} alt="Drawer_1" />
          </div>
          <div className="col-lg-6 pt-5">
            <h5>1.This example is a Drawer with Mutiple Account Selection.</h5>

            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  Drawer( child: Column( mainAxisAlignment:
                  MainAxisAlignment.start, crossAxisAlignment:
                  CrossAxisAlignment.start, children: [
                  UserAccountsDrawerHeader( currentAccountPicture:
                  CachedNetworkImage( placeholder: placeholderWidgetFn(),
                  imageUrl:
                  'https://miro.medium.com/max/2048/0*0fClPmIScV5pTLoE.jpg',
                  fit: BoxFit.cover, ).cornerRadiusWithClipRRect(100),
                  accountName: Text('john Doe'), otherAccountsPictures: [
                  CachedNetworkImage( placeholder: placeholderWidgetFn(),
                  imageUrl:
                  'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTkynekYrn6_eDaVJG5l_DTiD_5LAm2_osI0Q&usqp=CAU',
                  fit: BoxFit.cover, ).cornerRadiusWithClipRRect(100),
                  CachedNetworkImage( placeholder: placeholderWidgetFn(),
                  imageUrl:
                  'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTD8u1Nmrk78DSX0v2i_wTgS6tW5yvHSD7o6g&usqp=CAU',
                  fit: BoxFit.cover, ).cornerRadiusWithClipRRect(100), ],
                  accountEmail: Text('johndoe@gmail.com'), ), ListTile( title:
                  Text( "Home", style: boldTextStyle(color: black), ), leading:
                  Icon(Icons.home, color: Colors.black.withOpacity(0.7)), ),
                  ListTile( title: Text( "Photos", style: boldTextStyle(color:
                  black), ), leading: Icon(Icons.photo, color:
                  Colors.black.withOpacity(0.7)), ), ListTile( title: Text(
                  "Movies", style: boldTextStyle(color: black), ), leading:
                  Icon(Icons.movie, color: Colors.black.withOpacity(0.7)), ),
                  ListTile( title: Text( "Notification", style:
                  boldTextStyle(color: black), ), leading:
                  Icon(Icons.notifications, color:
                  Colors.black.withOpacity(0.7)), ), ListTile( title: Text(
                  "Settings", style: boldTextStyle(color: black), ), leading:
                  Icon(Icons.settings, color: Colors.black.withOpacity(0.7)), ),
                  Divider(), Text('Other').paddingLeft(12.0), Text( 'About Us',
                  style: boldTextStyle(color: black), ).paddingAll(12.0), Text(
                  'Privacy Policy', style: boldTextStyle(color: black),
                  ).paddingAll(12.0), ], ), ),
                </SyntaxHighlighter>
              </Typography>
            </Container>
            <h5>2.The Required Imports used here as follows.</h5>

            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <div
                  style={{
                    background: "#ffffff",
                    overflow: "auto",
                    width: "auto",
                    border: "solid gray",
                    borderWidth: ".1em .1em .1em .8em",
                    padding: ".2em .6em",
                  }}
                >
                  <pre style={{ margin: 0, lineHeight: "125%" }}>
                    <span style={{ color: "#008800", fontWeight: "bold" }}>
                      import
                    </span>{" "}
                    <span style={{ backgroundColor: "#fff0f0" }}>
                      'package:cached_network_image/cached_network_image.dart'
                    </span>
                    ;{"\n"}
                    <span style={{ color: "#008800", fontWeight: "bold" }}>
                      import
                    </span>{" "}
                    <span style={{ backgroundColor: "#fff0f0" }}>
                      'package:flutter/material.dart'
                    </span>
                    ;{"\n"}
                  </pre>
                </div>
              </Typography>
            </Container>
          </div>
        </div>
      </div>
      <hr />
      <div className="container m-0">
        <div className="row">
          <div className="col-lg-6">
            <img src={Drawer_2} alt="Drawer_2" />
          </div>
          <div className="col-lg-6 pt-5">
            <h5>1. This example is a Drawer with Custom Shape.</h5>

            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  drawer: ClipPath( clipper: OvalRightBorderClipper(),
                  clipBehavior: Clip.antiAliasWithSaveLayer, child: Drawer(
                  child: Container( padding: const EdgeInsets.only(left: 16.0,
                  right: 40), decoration: BoxDecoration( color: Color, ), width:
                  300, child: SafeArea( child: SingleChildScrollView( child:
                  Column( children: [ Container( alignment:
                  Alignment.centerRight, child: IconButton( icon: Icon(
                  Icons.power_settings_new, color: appStore.textPrimaryColor, ),
                  ), ), Container( height: 90, width: 90, alignment:
                  Alignment.center, decoration: BoxDecoration( shape:
                  BoxShape.circle, border: Border.all(width: 2, color:
                  Colors.orange), image: DecorationImage(image:
                  CachedNetworkImageProvider('https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTD8u1Nmrk78DSX0v2i_wTgS6tW5yvHSD7o6g&usqp=CAU')),
                  ), ), SizedBox(height: 5.0), Text( "John Dow", style:
                  TextStyle(color: appStore.textPrimaryColor, fontSize: 18.0,
                  fontWeight: FontWeight.w600), ), Text("JohnDoe@gmail.com",
                  style: TextStyle(color: appStore.textPrimaryColor, fontSize:
                  16.0)), 30.height, Icon(Icons.home, color:
                  appStore.iconColor), "Home"), Divider(), 15.height,
                  Icon(Icons.person_pin, color: appStore.iconColor), "My
                  profile"), Divider(), 15.height, Icon(Icons.message, color:
                  appStore.iconColor), "Messages"), Divider(), 15.height,
                  Icon(Icons.notifications, color: appStore.iconColor),
                  "Notifications"), Divider(), 15.height, Icon(Icons.settings,
                  color: appStore.iconColor), "Settings"), Divider(), 15.height,
                  Icon(Icons.email, color: appStore.iconColor), "Contact us"),
                  Divider(), 15.height, Icon(Icons.info_outline, color:
                  appStore.iconColor), "Help"), Divider(), 15.height, ], ), ),
                  ), ), ), ),
                </SyntaxHighlighter>
              </Typography>
            </Container>
            <hr />

            <h5>2. The Required Imports used here as follows.</h5>

            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <div
                  style={{
                    background: "#ffffff",
                    overflow: "auto",
                    width: "auto",
                    border: "solid gray",
                    borderWidth: ".1em .1em .1em .8em",
                    padding: ".2em .6em",
                  }}
                >
                  <pre style={{ margin: 0, lineHeight: "125%" }}>
                    <span style={{ color: "#008800", fontWeight: "bold" }}>
                      import
                    </span>{" "}
                    <span style={{ backgroundColor: "#fff0f0" }}>
                      'package:cached_network_image/cached_network_image.dart'
                    </span>
                    ;{"\n"}
                    <span style={{ color: "#008800", fontWeight: "bold" }}>
                      import
                    </span>{" "}
                    <span style={{ backgroundColor: "#fff0f0" }}>
                      'package:flutter/material.dart'
                    </span>
                    ;{"\n"}
                  </pre>
                </div>
              </Typography>
            </Container>
            <hr />

            <h5>
              3. A custom method OvalRightBorderClipper() is passed to clipper
              props to provide desired shape.
            </h5>

            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <div
                  style={{
                    background: "#ffffff",
                    overflow: "auto",
                    width: "auto",
                    border: "solid gray",
                    borderWidth: ".1em .1em .1em .8em",
                    padding: ".2em .6em",
                  }}
                >
                  <pre style={{ margin: 0, lineHeight: "125%" }}>
                    <span style={{ color: "#008800", fontWeight: "bold" }}>
                      class
                    </span>{" "}
                    <span style={{ color: "#BB0066", fontWeight: "bold" }}>
                      OvalRightBorderClipper
                    </span>{" "}
                    <span style={{ color: "#008800", fontWeight: "bold" }}>
                      extends
                    </span>{" "}
                    CustomClipper<span style={{ color: "#333333" }}>&lt;</span>
                    Path
                    <span style={{ color: "#333333" }}>&gt;</span> {"{"}
                    {"\n"}
                    {"  "}
                    <span
                      style={{ color: "#FF0000", backgroundColor: "#FFAAAA" }}
                    >
                      @
                    </span>
                    override{"\n"}
                    {"  "}Path getClip(Size size) {"{"}
                    {"\n"}
                    {"    "}
                    <span style={{ color: "#008800", fontWeight: "bold" }}>
                      var
                    </span>{" "}
                    path <span style={{ color: "#333333" }}>=</span> Path();
                    {"\n"}
                    {"    "}path.lineTo(
                    <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                      0
                    </span>
                    ,{" "}
                    <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                      0
                    </span>
                    );{"\n"}
                    {"    "}path.lineTo(size.width{" "}
                    <span style={{ color: "#333333" }}>-</span>{" "}
                    <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                      50
                    </span>
                    ,{" "}
                    <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                      0
                    </span>
                    );{"\n"}
                    {"    "}path.quadraticBezierTo(size.width, size.height{" "}
                    <span style={{ color: "#333333" }}>/</span>{" "}
                    <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                      4
                    </span>
                    , size.width, size.height{" "}
                    <span style={{ color: "#333333" }}>/</span>{" "}
                    <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                      2
                    </span>
                    );{"\n"}
                    {"    "}path.quadraticBezierTo(size.width, size.height{" "}
                    <span style={{ color: "#333333" }}>-</span> (size.height{" "}
                    <span style={{ color: "#333333" }}>/</span>{" "}
                    <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                      4
                    </span>
                    ), size.width <span style={{ color: "#333333" }}>-</span>{" "}
                    <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                      40
                    </span>
                    , size.height);{"\n"}
                    {"    "}path.lineTo(
                    <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                      0
                    </span>
                    , size.height);{"\n"}
                    {"    "}
                    <span style={{ color: "#008800", fontWeight: "bold" }}>
                      return
                    </span>{" "}
                    path;
                    {"\n"}
                    {"  "}
                    {"}"}
                    {"\n"}
                    {"\n"}
                    {"  "}
                    <span
                      style={{ color: "#FF0000", backgroundColor: "#FFAAAA" }}
                    >
                      @
                    </span>
                    override{"\n"}
                    {"  "}
                    <span style={{ color: "#333399", fontWeight: "bold" }}>
                      bool
                    </span>{" "}
                    shouldReclip(CustomClipper
                    <span style={{ color: "#333333" }}>&lt;</span>Path
                    <span style={{ color: "#333333" }}>
                      &gt;
                    </span> oldClipper) {"{"}
                    {"\n"}
                    {"    "}
                    <span style={{ color: "#008800", fontWeight: "bold" }}>
                      return
                    </span>{" "}
                    <span style={{ color: "#008800", fontWeight: "bold" }}>
                      true
                    </span>
                    ;{"\n"}
                    {"  "}
                    {"}"}
                    {"\n"}
                    {"}"}
                    {"\n"}
                  </pre>
                </div>
              </Typography>
            </Container>
            <hr />
          </div>
        </div>
      </div>
    </>
  );
};

export default Drawer;
