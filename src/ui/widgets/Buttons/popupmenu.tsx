import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import PB_1 from "../../../assets/Buttons/PB_1.png";
import PB_2 from "../../../assets/Buttons/PB_2.png";
import PB_3 from "../../../assets/Buttons/PB_3.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const PopUpMenuButtons = (props: Props) => {
  return (
    <>
      <div className="container m-0">
        <div className="row">
          <div className="col">
            <h2>
              <u>PopUp Menu Buttons </u>
            </h2>
            <img src={PB_1} alt="PB_1" />

            <h5>1. This example is a Default PopUp Menu with Button.</h5>

            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  Card( elevation: 4, child: ListTile( title: Text( 'Default
                  Popup menu', style: TextStyle(), ), trailing: PopupMenuButton(
                  icon: Icon( Icons.more_vert, color: Color, ), onSelected:
                  (value) &braces; Do Something &braces;, itemBuilder: (context)
                  &braces; var list = List(); list.add( PopupMenuItem( child:
                  Text("Mark as read"), value: 'Mark as read', ), ); list.add(
                  PopupMenuItem( child: Text("Mute Notification"), value: ' Mute
                  Notification', ), ); list.add( PopupMenuItem( child:
                  Text("Settings"), value: 'Settings', ), ); return list;
                  &braces;, )), ),
                </SyntaxHighlighter>
              </Typography>
            </Container>
          </div>
          <hr />
        </div>
        <hr />
        <div className="row">
          <div className="col">
            <img src={PB_2} alt="PB_2" />
            <h5>1. This example is a Sectioned PopUp Menu with Button.</h5>

            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  Card( elevation: 4, child: ListTile( title: Text( 'Sectioned
                  Popup menu', style: TextStyle(), ), trailing: PopupMenuButton(
                  icon: Icon( Icons.more_vert, color: Color, ), onSelected:
                  (value) &braces; Do Something &braces;, offset: Offset(0,
                  100), itemBuilder: (context) &braces; var list = List();
                  list.add( PopupMenuItem( child: Text("Select Language"),
                  value: 'Select Language', ), ); list.add(PopupMenuDivider(
                  height: 2, )); list.add( PopupMenuItem( child:
                  Text("English"), value: 'English', ), ); list.add(
                  PopupMenuItem( child: Text("Spanish"), value: 'Spanish', ), );
                  list.add( PopupMenuItem( child: Text("Arabic"), value:
                  'Arabic', ), ); list.add( PopupMenuItem( child: Text("Hindi"),
                  value: 'Hindi', ), ); list.add( PopupMenuItem( child:
                  Text("Gujarati"), value: 'Gujarati', ), ); return list;
                  &braces;, )), ),
                </SyntaxHighlighter>
              </Typography>
            </Container>
          </div>
        </div>
        <hr />
        <div className="row">
          <div className="col">
            <img src={PB_3} alt="PB_3" />
            <h5>1. This example is a PopUp Menu with Icons.</h5>

            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  Card( elevation: 4, child: ListTile( title: Text( 'Popup menu
                  with Icons', style: TextStyle(), ), trailing: PopupMenuButton(
                  icon: Icon( Icons.more_vert, color: Color, ), onSelected:
                  (value) &braces; toast(value); &braces;, offset: Offset(0,
                  100), itemBuilder: (context) &braces; var list = List();
                  list.add( PopupMenuItem( child: ListTile( leading: Icon(
                  Icons.file_upload, color: Colors.black, ), title:
                  Text('Upload'), ), value: 'Upload', ), ); list.add(
                  PopupMenuItem( child: ListTile( leading: Icon( Icons.bookmark,
                  color: Colors.black, ), title: Text('Bookmark'), ), value:
                  'Bookmark', ), ); list.add( PopupMenuItem( child: ListTile(
                  leading: Icon( Icons.share, color: Colors.black, ), title:
                  Text('Share'), ), value: 'Share', ), ); return list; &braces;,
                  )), ),
                </SyntaxHighlighter>
              </Typography>
            </Container>
          </div>
        </div>
      </div>
    </>
  );
};

export default PopUpMenuButtons;
