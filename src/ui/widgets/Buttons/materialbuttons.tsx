import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import MB_1 from "../../../assets/Buttons/MB_1.png";
import MB_2 from "../../../assets/Buttons/MB_2.png";
import MB_3 from "../../../assets/Buttons/MB_3.png";
import MB_4 from "../../../assets/Buttons/MB_4.png";
import MB_5 from "../../../assets/Buttons/MB_5.png";
import MB_6 from "../../../assets/Buttons/MB_6.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const MaterialButtons = (props: Props) => {
  return (
    <>
      <div className="container d-flex m-0">
        <div className="row">
          <div className="col-md-6">
            <h2>
              <u>Material Button</u>
            </h2>

            <img src={MB_1} alt="MB_1" />
            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  MaterialButton( onPressed: () &braces; Do something &braces;,
                  child: Text( 'Default Material button', style: TextStyle(), ),
                  ),
                </SyntaxHighlighter>
              </Typography>
            </Container>
            <hr />

            <img src={MB_2} alt="MB_2" />
            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  MaterialButton( onPressed: null, child: Text( 'Disable
                  Material button', style: TextStyle(size: 16), ), ),
                </SyntaxHighlighter>
              </Typography>
            </Container>
            <hr />

            <img src={MB_3} alt="MB_3" />
            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  MaterialButton( onPressed: () &braces; Do Something &braces;,
                  shape: Border.all(color: Color), child: Text( 'Border Material
                  button', style: TextStyle(), ), ),
                </SyntaxHighlighter>
              </Typography>
            </Container>
          </div>

          <div className="col-md-6 pt-5">
            <img src={MB_4} alt="MB_4" />
            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  MaterialButton( onPressed: () &braces; Do Something &braces;,
                  color: getColorFromHex('#8998FF'), child: Text( "Color Fill
                  Material button", style: TextStyle(color: Colors.white), ), ),
                </SyntaxHighlighter>
              </Typography>
            </Container>
            <hr />

            <img src={MB_5} alt="MB_5" />
            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  MaterialButton( onPressed: () &braces; Do Something &braces;,
                  shape: RoundedRectangleBorder( borderRadius:
                  BorderRadius.circular(30), side: BorderSide( color:
                  Colors.green, )), child: Text( 'Rounded Material button',
                  style: TextStyle(), ), ),
                </SyntaxHighlighter>
              </Typography>
            </Container>
            <hr />

            <img src={MB_6} alt="MB_6" />
            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  MaterialButton( onPressed: () &braces; Do Something &braces;,
                  shape: RoundedRectangleBorder( borderRadius:
                  BorderRadius.circular(30)), color: getColorFromHex('#f2866c'),
                  child: Text( 'Rounded color fill Material button', style:
                  TextStyle(color: Colors.white), ), ),
                </SyntaxHighlighter>
              </Typography>
            </Container>
          </div>
        </div>
      </div>
    </>
  );
};

export default MaterialButtons;
