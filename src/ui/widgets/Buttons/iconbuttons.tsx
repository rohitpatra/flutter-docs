import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import IC_1 from "../../../assets/Buttons/IC_1.png";
import IC_2 from "../../../assets/Buttons/IC_2.png";
import IC_3 from "../../../assets/Buttons/IC_3.png";
import IC_4 from "../../../assets/Buttons/IC_4.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const IconButtons = (props: Props) => {
  return (
    <>
      <div className="container d-flex m-0">
        <div className="row">
          <div className="col-md-6">
            <h2>
              <u>Icon Buttons</u>
            </h2>
            <div className="text-center">
              <img src={IC_1} alt="IC_1" />
              <Container className="m-0">
                <Typography component="div" style={{ height: "50%" }}>
                  <SyntaxHighlighter
                    language="dart"
                    style={materialDark}
                    wrapLines={true}
                  >
                    Ink( decoration: const ShapeDecoration( color:
                    Colors.lightBlue, shape: CircleBorder(), ), child:
                    IconButton( icon: Icon( Icons.file_upload, color:
                    Colors.white, ), iconSize: 30, onPressed: () &braces; Do
                    Something &braces;, ), ),
                  </SyntaxHighlighter>
                </Typography>
              </Container>
              <hr />

              <img src={IC_2} alt="IC_2" />
              <Container className="m-0">
                <Typography component="div" style={{ height: "50%" }}>
                  <SyntaxHighlighter
                    language="dart"
                    style={materialDark}
                    wrapLines={true}
                  >
                    IconButton( icon: Row( mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center, children: [
                    Icon( Icons.save_alt, ), 10.width, Text('Download', style:
                    TextStyle()), ], ), onPressed: () &braces; Do Something
                    &braces;, ),
                  </SyntaxHighlighter>
                </Typography>
              </Container>
            </div>
          </div>

          <div className="col-md-6 pt-5">
            <div className="text-center">
              <img src={IC_3} alt="IC_3" />
              <Container className="m-0">
                <Typography component="div" style={{ height: "50%" }}>
                  <SyntaxHighlighter
                    language="dart"
                    style={materialDark}
                    wrapLines={true}
                  >
                    Ink( decoration: ShapeDecoration( color: Colors.greenAccent,
                    shape: CircleBorder(side: BorderSide(width: 2, color:
                    Colors.black)), ), child: IconButton( icon: Icon(
                    Icons.share, color: Colors.white, ), iconSize: 30,
                    onPressed: () &braces; Do Something &braces;, ), ),
                  </SyntaxHighlighter>
                </Typography>
              </Container>
              <hr />

              <img src={IC_4} alt="IC_4" />
              <Container className="m-0">
                <Typography component="div" style={{ height: "50%" }}>
                  <SyntaxHighlighter
                    language="dart"
                    style={materialDark}
                    wrapLines={true}
                  >
                    IconButton( icon: Icon( Icons.message, color:
                    appStore.iconColor, ), iconSize: 30, onPressed: () &braces;
                    Do Something &braces;, ),
                  </SyntaxHighlighter>
                </Typography>
              </Container>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default IconButtons;
