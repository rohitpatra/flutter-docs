import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import FAB_1 from "../../../assets/Buttons/FAB_1.png";
import FAB_2 from "../../../assets/Buttons/FAB_2.png";
import FAB_3 from "../../../assets/Buttons/FAB_3.png";
import FAB_4 from "../../../assets/Buttons/FAB_4.png";
import FAB_5 from "../../../assets/Buttons/FAB_5.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const FloatingActionButtons = (props: Props) => {
  return (
    <>
      <div className="container d-flex m-0">
        <div className="row">
          <div className="col-md-6">
            <h2>
              <u>Outline Buttons</u>
            </h2>

            <img src={FAB_1} alt="FAB_1" />
            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  FloatingActionButton( heroTag: '1', elevation: 5, onPressed:
                  () &braces; Do Something &braces;, child: Icon( Icons.add,
                  color: Colors.white, ), ),
                </SyntaxHighlighter>
              </Typography>
            </Container>
            <hr />

            <img src={FAB_2} alt="FAB_2" />
            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  FloatingActionButton( heroTag: '2', backgroundColor:
                  getColorFromHex('#8998FF'), child: Text( 'Add', style:
                  TextStyle( color: Colors.white, ), ), onPressed: () &braces;
                  Do Something &braces;, ),
                </SyntaxHighlighter>
              </Typography>
            </Container>
            <hr />

            <img src={FAB_3} alt="FAB_3" />
            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  FloatingActionButton( heroTag: '3', shape: CircleBorder(side:
                  BorderSide(color: Colors.black)), backgroundColor:
                  Colors.blue, child: Icon( Icons.warning, color: Colors.white,
                  ), onPressed: () &braces; Do Something &braces;, ), ),
                </SyntaxHighlighter>
              </Typography>
            </Container>
          </div>

          <div className="col-md-6 pt-5">
            <img src={FAB_4} alt="FAB_4" />
            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  Center( child: FloatingActionButton( heroTag: '4', shape:
                  RoundedRectangleBorder( borderRadius:
                  BorderRadius.circular(10)), backgroundColor: Colors.amber,
                  child: Icon( Icons.shopping_cart, color: Colors.white, ),
                  onPressed: () &braces; Do Something &braces;, ), ),
                </SyntaxHighlighter>
              </Typography>
            </Container>
            <hr />

            <img src={FAB_5} alt="FAB_5" />
            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  Row( mainAxisSize: MainAxisSize.min, crossAxisAlignment:
                  CrossAxisAlignment.center, mainAxisAlignment:
                  MainAxisAlignment.center, children: [
                  FloatingActionButton.extended( heroTag: '5', label: Text(
                  "Add", style: TextStyle(color: Colors.white), ),
                  backgroundColor: Colors.green, icon: Icon( Icons.add, color:
                  Colors.white, ), onPressed: () &braces; Do Something
                  &braces;,), 10.width, FloatingActionButton.extended( heroTag:
                  '6', elevation: 4, shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0)), label: Text( "Take
                  pic", style: TextStyle(color: Colors.white), ),
                  backgroundColor: Colors.deepOrangeAccent, icon: Icon(
                  Icons.add_a_photo, color: Colors.white, ), onPressed: ()
                  &braces; Do Something &braces;,), ], ),
                </SyntaxHighlighter>
              </Typography>
            </Container>
          </div>
        </div>
      </div>
    </>
  );
};

export default FloatingActionButtons;
