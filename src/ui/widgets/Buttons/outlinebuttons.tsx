import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import OB_1 from "../../../assets/Buttons/OB_1.png";
import OB_2 from "../../../assets/Buttons/OB_2.png";
import OB_3 from "../../../assets/Buttons/OB_3.png";
import OB_4 from "../../../assets/Buttons/OB_4.png";
import OB_5 from "../../../assets/Buttons/OB_5.png";
import OB_6 from "../../../assets/Buttons/OB_6.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const OutlineButtons = (props: Props) => {
  return (
    <>
      <div className="container d-flex m-0">
        <div className="row">
          <div className="col-md-6">
            <h2>
              <u>Outline Buttons</u>
            </h2>

            <img src={OB_1} alt="OB_1" />
            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  OutlineButton( borderSide: BorderSide( color: Color, style:
                  BorderStyle.solid, width: 0.8, ), onPressed: () &braces; Do
                  Something &braces;, child: Text( 'Default Outline button',
                  style: TextStyle(), ), ),
                </SyntaxHighlighter>
              </Typography>
            </Container>
            <hr />

            <img src={OB_2} alt="OB_2" />
            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  OutlineButton.icon( borderSide: BorderSide( color: Color,
                  style: BorderStyle.solid, width: 0.8, ), onPressed: ()
                  &braces; Do Something; &braces;, icon: Icon( Icons.add, color:
                  Color, ), label: Text( 'Outline button with icon', style:
                  TextStyle(), )),
                </SyntaxHighlighter>
              </Typography>
            </Container>
            <hr />

            <img src={OB_3} alt="OB_3" />
            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  OutlineButton( borderSide: BorderSide( color: Color, style:
                  BorderStyle.solid, width: 0.8, ), onPressed: null, child:
                  Text( 'Disable Outline button', style: TextStyle(size: 16), ),
                  ),
                </SyntaxHighlighter>
              </Typography>
            </Container>
          </div>

          <div className="col-md-6 pt-5">
            <img src={OB_4} alt="OB_4" />
            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  OutlineButton( borderSide: BorderSide( color: Color, style:
                  BorderStyle.solid, width: 0.8, ), onPressed: () &braces; Do
                  Something &braces;, shape: Border.all(color: Color), child:
                  Text( 'Border Outline button', style: TextStyle(), ), ),
                </SyntaxHighlighter>
              </Typography>
            </Container>
            <hr />

            <img src={OB_5} alt="OB_5" />
            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  OutlineButton( borderSide: BorderSide( color: Color, style:
                  BorderStyle.solid, width: 0.8, ), onPressed: () &braces; Do
                  Something &braces;, shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30), side: BorderSide(
                  color: Colors.green, )), child: Text( 'Rounded Outline
                  button', style: TextStyle(), ), ),
                </SyntaxHighlighter>
              </Typography>
            </Container>
            <hr />

            <img src={OB_6} alt="OB_6" />
            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  OutlineButton( borderSide: BorderSide( color: Color, style:
                  BorderStyle.solid, width: 0.8, ), onPressed: () &braces; Do
                  Something &braces;, shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10), side: BorderSide(
                  color: Colors.red, )), child: Text( 'Customize Rounded Outline
                  button', style: TextStyle(), ), ),
                </SyntaxHighlighter>
              </Typography>
            </Container>
          </div>
        </div>
      </div>
    </>
  );
};

export default OutlineButtons;
