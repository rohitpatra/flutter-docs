import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import DropDown_1 from "../../../assets/Buttons/DropDown_1.png";
import DropDown_2 from "../../../assets/Buttons/DropDown_2.png";
import DropDown_3 from "../../../assets/Buttons/DropDown_3.png";
import DropDown_4 from "../../../assets/Buttons/DropDown_4.png";
import DropDown_5 from "../../../assets/Buttons/DropDown_5.png";
import DropDown_6 from "../../../assets/Buttons/DropDown_6.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const Dropdown = (props: Props) => {
  return (
    <>
      <div className="container m-0">
        <div className="row">
          <div className="col-lg-6">
            <h2>
              <u>Button Dropdown</u>
            </h2>
            <img src={DropDown_1} alt="DropDown_1" />
            <img src={DropDown_2} alt="DropDown_2" />
          </div>
          <div className="col-lg-6 pt-5">
            <h5>1. This example is a Dropdown with Button actions.</h5>

            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  Card( elevation: 4, child: DropdownButton( isExpanded: true,
                  dropdownColor: Color, value: selectedIndexCategory, style:
                  TextStyle(), icon: Icon( Icons.keyboard_arrow_down, color:
                  Color, ), underline: 0, onChanged: (newValue) &braces;
                  setState(() &braces; selectedIndexCategory = newValue;
                  &braces;); &braces;, items: listOfCategory.map((category)
                  &braces; return DropdownMenuItem( child: Text(category, style:
                  TextStyle()), value: category, ); &braces;).toList(), )),
                </SyntaxHighlighter>
              </Typography>
            </Container>
            <hr />

            <h5>
              2. In this example a local variable selectedIndexCategory is
              initialized.
            </h5>
            <h5>
              3. Dropdown items are mapped from array of strings and passed to
              item props.
            </h5>

            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <div
                  style={{
                    background: "#ffffff",
                    overflow: "auto",
                    width: "auto",
                    border: "solid gray",
                    borderWidth: ".1em .1em .1em .8em",
                    padding: ".2em .6em",
                  }}
                >
                  <pre style={{ margin: 0, lineHeight: "125%" }}>
                    {"  "}List<span style={{ color: "#333333" }}>&lt;</span>
                    <span style={{ color: "#333399", fontWeight: "bold" }}>
                      String
                    </span>
                    <span style={{ color: "#333333" }}>&gt;</span>{" "}
                    listOfCategory <span style={{ color: "#333333" }}>=</span> [
                    <span style={{ backgroundColor: "#fff0f0" }}>'It'</span>,{" "}
                    <span style={{ backgroundColor: "#fff0f0" }}>
                      'Computer Science'
                    </span>
                    ,{" "}
                    <span style={{ backgroundColor: "#fff0f0" }}>
                      'Business'
                    </span>
                    ,{" "}
                    <span style={{ backgroundColor: "#fff0f0" }}>
                      'Data Science'
                    </span>
                    ,{" "}
                    <span style={{ backgroundColor: "#fff0f0" }}>
                      'Infromation Technologies'
                    </span>
                    ,{" "}
                    <span style={{ backgroundColor: "#fff0f0" }}>'Health'</span>
                    ,{" "}
                    <span style={{ backgroundColor: "#fff0f0" }}>
                      'Physics'
                    </span>
                    ];{"\n"}
                    <span style={{ color: "#333399", fontWeight: "bold" }}>
                      String
                    </span>{" "}
                    selectedIndexCategory{" "}
                    <span style={{ color: "#333333" }}>=</span>{" "}
                    <span style={{ backgroundColor: "#fff0f0" }}>
                      'Business'
                    </span>
                    ;{"\n"}
                  </pre>
                </div>
              </Typography>
            </Container>
          </div>
        </div>
        <hr />
        <div className="row">
          <div className="col-lg-6">
            <img src={DropDown_3} alt="DropDown_3" />
            <img src={DropDown_4} alt="DropDown_4" />
          </div>
          <div className="col-lg-6">
            <h5>1. This example is a Dropdown with a Hint.</h5>

            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  Card( elevation: 4, child: DropdownButton( value:
                  dropdownNames, underline: 0, dropdownColor: Color, hint: Text(
                  'Choose Text Style', style: TextStyle(size: 16), ), icon:
                  Icon( Icons.arrow_drop_down, color: Color, ), onChanged:
                  (String newValue) &braces; setState(() &braces; dropdownNames
                  = newValue; &braces;); &braces;, items: ['Normal', 'Bold',
                  'Italic', 'Underline'].map((String value) &braces; return
                  DropdownMenuItem( value: value, child: Tooltip(message: value,
                  child: Container(margin: EdgeInsets.only(left: 4, right: 4),
                  child: Text(value, style: TextStyle()))), );
                  &braces;).toList(), ),),
                </SyntaxHighlighter>
              </Typography>
            </Container>

            <h5>
              2. In this example Local Variable dropdownNames is initialized and
              passed to onChange method .
            </h5>

            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <div
                  style={{
                    background: "#ffffff",
                    overflow: "auto",
                    width: "auto",
                    border: "solid gray",
                    borderWidth: ".1em .1em .1em .8em",
                    padding: ".2em .6em",
                  }}
                >
                  <pre style={{ margin: 0, lineHeight: "125%" }}>
                    {" "}
                    <span style={{ color: "#333399", fontWeight: "bold" }}>
                      String
                    </span>{" "}
                    dropdownNames;{"\n"}
                    {"\n"}
                    {"                    "}
                    <span style={{ color: "#997700", fontWeight: "bold" }}>
                      onChanged:
                    </span>{" "}
                    (newValue) {"{"}
                    {"\n"}
                    {"                      "}setState(() {"{"}
                    {"\n"}
                    {"                       "}dropdownNames{" "}
                    <span style={{ color: "#333333" }}>=</span> newValue;{"\n"}
                    {"                      "}
                    {"}"});{"\n"}
                    {"                    "}
                    {"}"},{"\n"}
                  </pre>
                </div>
              </Typography>
            </Container>
          </div>
        </div>
        <hr />
        <div className="row">
          <div className="col-lg-6">
            <img src={DropDown_5} alt="DropDown_5" />
            <img style={{ width: "100%" }} src={DropDown_6} alt="DropDown_6" />
          </div>
          <div className="col-lg-6">
            <h5>1. This example is a scrollable Dropdown.</h5>

            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  Card( elevation: 4, child:DropdownButton( value:
                  dropdownScrollable, underline: 0, dropdownColor: Color, icon:
                  Icon( Icons.arrow_drop_down, color: Color, ), onChanged:
                  (String newValue) &braces; setState(() &braces;
                  dropdownScrollable = newValue; &braces;); &braces;, items: [
                  'I', 'He', 'She', 'You', 'We', 'They', 'Am', 'Is', 'Are', 'A',
                  'An', 'Me', 'His', 'Her', 'Your', 'Our', 'The', 'i', 'he',
                  'she', 'you', 'we', 'they', 'am', 'is', 'are', 'a', 'an',
                  'me', 'his', 'her', 'your', 'our', 'the', ].map((String value)
                  &braces; return DropdownMenuItem( value: value, child:
                  Text(value, style: TextStyle()), ); &braces;).toList(), ),),
                </SyntaxHighlighter>
              </Typography>
            </Container>
            <hr />
            <h5>
              2. In this example Local Variable dropdownScrollable is
              initialized and passed to onChange method .
            </h5>

            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <div
                  style={{
                    background: "#ffffff",
                    overflow: "auto",
                    width: "auto",
                    border: "solid gray",
                    borderWidth: ".1em .1em .1em .8em",
                    padding: ".2em .6em",
                  }}
                >
                  <pre style={{ margin: 0, lineHeight: "125%" }}>
                    {"  "}
                    <span style={{ color: "#333399", fontWeight: "bold" }}>
                      String
                    </span>{" "}
                    dropdownScrollable{" "}
                    <span style={{ color: "#333333" }}>=</span>{" "}
                    <span style={{ backgroundColor: "#fff0f0" }}>'I'</span>;
                    {"\n"}
                    {"  "}
                    <span style={{ color: "#997700", fontWeight: "bold" }}>
                      onChanged:
                    </span>{" "}
                    (
                    <span style={{ color: "#333399", fontWeight: "bold" }}>
                      String
                    </span>{" "}
                    newValue) {"{"}
                    {"\n"}
                    {"   "}setState(() {"{"}
                    {"\n"}
                    {"     "}dropdownScrollable{" "}
                    <span style={{ color: "#333333" }}>=</span> newValue;{"\n"}
                    {"       "}
                    {"}"});{"\n"}
                    {"    "}
                    {"}"},{"\n"}
                  </pre>
                </div>
              </Typography>
            </Container>
          </div>
        </div>
      </div>
    </>
  );
};

export default Dropdown;
