import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import BS_1 from "../../../assets/Dialog_Panels/BS_1.png";
import BS_2 from "../../../assets/Dialog_Panels/BS_2.png";
import BS_3 from "../../../assets/Dialog_Panels/BS_3.png";
import BS_4 from "../../../assets/Dialog_Panels/BS_4.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const BottomSheet = (props: Props) => {
  return (
    <>
      <div className="container m-0">
        <div className="row">
          <div className="col-lg-6">
            <h2>
              <u>Bottom Sheets</u>
            </h2>
            <img src={BS_1} alt="BS_1" />
          </div>
          <div className="col-lg-6 pt-5">
            <h5>1. This example is a Simple Bottom Sheet.</h5>
            <h5>
              2. In this example a custom method mSimpleBottomSheet is declared.
            </h5>

            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  void mSimpleBottomSheet() &braces; Widget mOption(var icon,
                  var value) &braces; return Padding( padding: const
                  EdgeInsets.fromLTRB(16, 16, 16, 8), child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center, children: [
                  Icon( icon, size: 24, color: Color, ), 16.width, Text(value,
                  style: TextStyle(size: 16, color: Color)) ], ), ); &braces;
                  showModalBottomSheet( context: context, backgroundColor:
                  appStore.scaffoldBackground, builder: (builder) &braces;
                  return Container( height: 160.0, color: Colors.transparent,
                  child: Column( children: [ mOption(Icons.share,
                  "Share").onTap(() &braces; finish(context); &braces;),
                  mOption(Icons.link, "Get Link").onTap(() &braces;
                  finish(context); &braces;), mOption(Icons.edit, "Edit
                  Name").onTap(() &braces; finish(context); &braces;), ], ), );
                  &braces;); &braces;
                </SyntaxHighlighter>
              </Typography>
            </Container>
            <hr />
          </div>
        </div>
        <hr />
        <div className="row">
          <div className="col-lg-6">
            <img src={BS_2} alt="BS_2" />
          </div>
          <div className="col-lg-6">
            <h5>1. This example is a Bottom Sheet With Rounded Corner.</h5>
            <h5>
              2. In this example a custom method mCornerBottomSheet is declared.
            </h5>

            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  mCornerBottomSheet(BuildContext aContext) &braces;
                  showModalBottomSheet( context: aContext, backgroundColor:
                  Color, shape: RoundedRectangleBorder( borderRadius:
                  BorderRadius.vertical(top: Radius.circular(25.0)), ), builder:
                  (builder) &braces; return Container( height: 250.0, padding:
                  EdgeInsets.all(16), child: Column( crossAxisAlignment:
                  CrossAxisAlignment.start, children: [ Text( "Information",
                  style: TextStyle(color: Color), ), Divider(height: 5, color:
                  Color.withOpacity(0.5)), Text("Lorem ipsum is a placeholder
                  text commonly used to demonstrate the visual form of a
                  document or a typeface without relying on meaningful
                  content.", style: TextStyle(color: Color)), ], ), );
                  &braces;); &braces;
                </SyntaxHighlighter>
              </Typography>
            </Container>
          </div>
        </div>
        <hr />
        <div className="row">
          <div className="col-lg-6">
            <img src={BS_3} alt="BS_3" />
          </div>
          <div className="col-lg-6">
            <h5>1. This example is a Bottom Sheet With Scrollable Content.</h5>
            <h5>
              2. In this example a custom method mExpandedSheet is declared.
            </h5>

            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  mExpandedSheet(BuildContext context) &braces;
                  showModalBottomSheet( context: context, isScrollControlled:
                  true, isDismissible: true, shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.vertical(top:
                  Radius.circular(25.0)), ), backgroundColor:
                  Colors.transparent, builder: (context) =&gt;
                  DraggableScrollableSheet( initialChildSize: 0.45,
                  minChildSize: 0.2, maxChildSize: 1, builder: (context,
                  scrollController) &braces; return Container( color: Color,
                  child: GestureDetector( onTap: () &braces; finish(context);
                  &braces;, child: ListView.builder( controller:
                  scrollController, itemBuilder: (context, index) &braces;
                  return ListTile( title: Text( 'Item $index', style:
                  TextStyle(color: Color), ), ); &braces;, itemCount: 20, ), ),
                  ); &braces;, ), ); &braces;
                </SyntaxHighlighter>
              </Typography>
            </Container>
            <hr />
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6">
            <img src={BS_4} alt="BS_4" />
          </div>
          <div className="col-lg-6">
            <h5>1. This example is a Bottom Sheet With Form.</h5>

            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  mFormBottomSheet(BuildContext aContext) &braces;
                  showModalBottomSheet( backgroundColor: Colors.transparent,
                  context: aContext, isScrollControlled: true, builder:
                  (BuildContext context) &braces; return Container( decoration:
                  BoxDecoration(borderRadius: BorderRadius.only(topLeft:
                  Radius.circular(20), topRight: Radius.circular(20)), color:
                  appStore.appBarColor), padding: EdgeInsets.all(16), child:
                  Column( mainAxisSize: MainAxisSize.min, crossAxisAlignment:
                  CrossAxisAlignment.start, children: [ Text( "Add Review",
                  style: boldTextStyle(color: appStore.textPrimaryColor), ),
                  Divider().paddingOnly(top: 16, bottom: 16), Text( "Email",
                  style: primaryTextStyle(color: appStore.textPrimaryColor), ),
                  8.height, editTextStyle("Enter Email"), 16.height, Text(
                  "Description", style: primaryTextStyle(color:
                  appStore.textPrimaryColor), ), 8.height,
                  editTextStyle("Description"), 16.height, RatingBar(
                  initialRating: 5, minRating: 5, direction: Axis.horizontal,
                  allowHalfRating: true, itemCount: 5, itemPadding:
                  EdgeInsets.symmetric(horizontal: 4.0), itemBuilder: (context,
                  _) =&gt; Icon( Icons.star, color: Colors.amber, ),
                  onRatingUpdate: (rating) &braces; print(rating); &braces;,
                  ).center(), 30.height, GestureDetector( onTap: () &braces;
                  finish(context); &braces;, child: Container( width:
                  MediaQuery.of(context).size.width, decoration:
                  boxDecoration(bgColor: appColorPrimary, radius: 16), padding:
                  EdgeInsets.fromLTRB(16, 16, 16, 16), child: Center( child:
                  Text( "Submit", style: primaryTextStyle(color: white), ), ),
                  ), ) ], ), ); &braces;, ); &braces;
                </SyntaxHighlighter>
              </Typography>
            </Container>

            <h5>
              2. In this example a custom method mFormBottomSheet and
              editTextStyle is declared
            </h5>

            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <div
                  style={{
                    background: "#ffffff",
                    overflow: "auto",
                    width: "auto",
                    border: "solid gray",
                    borderWidth: ".1em .1em .1em .8em",
                    padding: ".2em .6em",
                  }}
                >
                  <pre style={{ margin: 0, lineHeight: "125%" }}>
                    Padding editTextStyle(
                    <span style={{ color: "#008800", fontWeight: "bold" }}>
                      var
                    </span>{" "}
                    hintText) {"{"}
                    {"\n"}
                    {"  "}
                    <span style={{ color: "#008800", fontWeight: "bold" }}>
                      return
                    </span>{" "}
                    Padding({"\n"}
                    {"    "}
                    <span style={{ color: "#997700", fontWeight: "bold" }}>
                      padding:
                    </span>{" "}
                    <span style={{ color: "#008800", fontWeight: "bold" }}>
                      const
                    </span>{" "}
                    EdgeInsets.fromLTRB(
                    <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                      0
                    </span>
                    ,{" "}
                    <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                      0
                    </span>
                    ,{" "}
                    <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                      0
                    </span>
                    ,{" "}
                    <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                      0
                    </span>
                    ),{"\n"}
                    {"    "}
                    <span style={{ color: "#997700", fontWeight: "bold" }}>
                      child:
                    </span>{" "}
                    TextFormField({"\n"}
                    {"      "}
                    <span style={{ color: "#997700", fontWeight: "bold" }}>
                      style:
                    </span>{" "}
                    TextStyle(
                    <span style={{ color: "#997700", fontWeight: "bold" }}>
                      fontSize:
                    </span>{" "}
                    <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                      16
                    </span>
                    ,{" "}
                    <span style={{ color: "#997700", fontWeight: "bold" }}>
                      fontFamily:
                    </span>{" "}
                    fontRegular),{"\n"}
                    {"      "}
                    <span style={{ color: "#997700", fontWeight: "bold" }}>
                      decoration:
                    </span>{" "}
                    InputDecoration({"\n"}
                    {"        "}
                    <span style={{ color: "#997700", fontWeight: "bold" }}>
                      contentPadding:
                    </span>{" "}
                    EdgeInsets.fromLTRB(
                    <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                      24
                    </span>
                    ,{" "}
                    <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                      16
                    </span>
                    ,{" "}
                    <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                      24
                    </span>
                    ,{" "}
                    <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                      16
                    </span>
                    ),{"\n"}
                    {"        "}
                    <span style={{ color: "#997700", fontWeight: "bold" }}>
                      hintText:
                    </span>{" "}
                    hintText,{"\n"}
                    {"        "}
                    <span style={{ color: "#997700", fontWeight: "bold" }}>
                      hintStyle:
                    </span>{" "}
                    primaryTextStyle(
                    <span style={{ color: "#997700", fontWeight: "bold" }}>
                      color:
                    </span>{" "}
                    appStore.isDarkModeOn{" "}
                    <span style={{ color: "#333333" }}>?</span>{" "}
                    white.withOpacity(
                    <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                      0.5
                    </span>
                    ) <span style={{ color: "#333333" }}>:</span> grey),{"\n"}
                    {"        "}
                    <span style={{ color: "#997700", fontWeight: "bold" }}>
                      filled:
                    </span>{" "}
                    <span style={{ color: "#008800", fontWeight: "bold" }}>
                      true
                    </span>
                    ,{"\n"}
                    {"        "}
                    <span style={{ color: "#997700", fontWeight: "bold" }}>
                      fillColor:
                    </span>{" "}
                    appStore.appBarColor,{"\n"}
                    {"        "}
                    <span style={{ color: "#997700", fontWeight: "bold" }}>
                      enabledBorder:
                    </span>{" "}
                    OutlineInputBorder(
                    <span style={{ color: "#997700", fontWeight: "bold" }}>
                      borderRadius:
                    </span>{" "}
                    BorderRadius.circular(
                    <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                      16
                    </span>
                    ),{" "}
                    <span style={{ color: "#997700", fontWeight: "bold" }}>
                      borderSide:
                    </span>{" "}
                    BorderSide(
                    <span style={{ color: "#997700", fontWeight: "bold" }}>
                      color:
                    </span>{" "}
                    appStore.iconSecondaryColor,{" "}
                    <span style={{ color: "#997700", fontWeight: "bold" }}>
                      width:
                    </span>{" "}
                    <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                      1.0
                    </span>
                    )),{"\n"}
                    {"        "}
                    <span style={{ color: "#997700", fontWeight: "bold" }}>
                      focusedBorder:
                    </span>{" "}
                    OutlineInputBorder(
                    <span style={{ color: "#997700", fontWeight: "bold" }}>
                      borderRadius:
                    </span>{" "}
                    BorderRadius.circular(
                    <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                      16
                    </span>
                    ),{" "}
                    <span style={{ color: "#997700", fontWeight: "bold" }}>
                      borderSide:
                    </span>{" "}
                    BorderSide(
                    <span style={{ color: "#997700", fontWeight: "bold" }}>
                      color:
                    </span>{" "}
                    appStore.iconSecondaryColor,{" "}
                    <span style={{ color: "#997700", fontWeight: "bold" }}>
                      width:
                    </span>{" "}
                    <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                      1.0
                    </span>
                    )),{"\n"}
                    {"      "}),{"\n"}
                    {"    "}),{"\n"}
                    {"  "});{"\n"}
                    {"}"}
                    {"\n"}
                  </pre>
                </div>
              </Typography>
            </Container>
            <hr />
          </div>
        </div>
      </div>
    </>
  );
};

export default BottomSheet;
