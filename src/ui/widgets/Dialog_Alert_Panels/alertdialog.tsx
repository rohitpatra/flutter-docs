import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import AD_1 from "../../../assets/Dialog_Panels/AD_1.png";
import AD_2 from "../../../assets/Dialog_Panels/AD_2.png";
import AD_3 from "../../../assets/Dialog_Panels/AD_3.png";
import AD_4 from "../../../assets/Dialog_Panels/AD_4.png";
import AD_5 from "../../../assets/Dialog_Panels/AD_5.png";
import AD_6 from "../../../assets/Dialog_Panels/AD_6.png";
import AD_7 from "../../../assets/Dialog_Panels/AD_7.png";
import AD_8 from "../../../assets/Dialog_Panels/AD_8.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const AlertDialog = (props: Props) => {
  return (
    <div className="container m-0">
      <div className="row">
        <div className="col-lg-6">
          <h2>
            <u>Alert Dialog</u>
          </h2>

          <img src={AD_1} alt="AD_1" />

          <h5>1. This example is a Simple Alert Dialog.</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                AlertDialog( backgroundColor: Color, title: Text( "Alert Title",
                style: TextStyle(color: Color), ), content: Text( "Alert
                Message", style: TextStyle(color: Color), ), actions: [], );{" "}
              </SyntaxHighlighter>
            </Typography>
          </Container>
          <hr />
          <img src={AD_2} alt="AD_2" />

          <h5>
            1. This example is a Confirmation Alert Dialog with Action Button.
          </h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                AlertDialog( backgroundColor: Color, title: Text("Confirmation",
                style: TextStyle(color: Color)), content: Text( "Are you sure
                you want to logout?", style: secondaryTextStyle(color: Color),
                ), actions: [ FlatButton( child: Text( "Yes", style:
                TextStyle(color: Color), ), onPressed: () &braces;
                Navigator.of(context).pop(); &braces;, ), FlatButton( child:
                Text("No", style: TextStyle(color: Color)), onPressed: ()
                &braces; Navigator.of(context).pop(); &braces;, ), ], );
              </SyntaxHighlighter>
            </Typography>
          </Container>
          <hr />
          <img src={AD_5} alt="AD_5" />

          <h5>1. This example is a Custom Achievement Dialog .</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                Dialog( shape: RoundedRectangleBorder( borderRadius:
                BorderRadius.circular(16), ), elevation: 0.0, backgroundColor:
                Colors.transparent, child: Container( decoration: new
                BoxDecoration( color: appStore.scaffoldBackground, shape:
                BoxShape.rectangle, borderRadius: BorderRadius.circular(16),
                boxShadow: [ BoxShadow(color: Colors.black26, blurRadius: 10.0,
                offset: const Offset(0.0, 10.0)), ], ), width:
                MediaQuery.of(context).size.width, child: Column( mainAxisSize:
                MainAxisSize.min, children: [ GestureDetector( onTap: ()
                &braces; finish(context); &braces;, child: Container(padding:
                EdgeInsets.all(16), alignment: Alignment.centerRight, child:
                Icon(Icons.close, color: appStore.textPrimaryColor)), ),
                Image(image:
                AssetImage('images/widgets/materialWidgets/mwDialogAlertPanelWidgets/widgettrophy.png'),
                height: 80, fit: BoxFit.cover), 24.height,
                Text('Congratulations', style: boldTextStyle(color:
                appStore.textPrimaryColor, size: 24)), 16.height, Padding(
                padding: const EdgeInsets.only(left: 16, right: 16), child:
                Text("Lorem Ipsum is simply dummy text of the printing and
                typesetting industry.", style: secondaryTextStyle(color:
                appStore.textSecondaryColor)), ), 30.height, ], ), ), );
              </SyntaxHighlighter>
            </Typography>
          </Container>
          <hr />
          <img src={AD_7} alt="AD_7" />

          <h5>1. This example is a Custom Delete Dialog .</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                Dialog( shape: RoundedRectangleBorder( borderRadius:
                BorderRadius.circular(8), ), elevation: 0.0, backgroundColor:
                Colors.transparent, child: Container( decoration: new
                BoxDecoration( color: appStore.scaffoldBackground, shape:
                BoxShape.rectangle, borderRadius: BorderRadius.circular(8),
                boxShadow: [ BoxShadow(color: Colors.black26, blurRadius: 10.0,
                offset: const Offset(0.0, 10.0)), ], ), width:
                MediaQuery.of(context).size.width, child: Column( mainAxisSize:
                MainAxisSize.min, children: [ ClipRRect( borderRadius:
                BorderRadius.only(topLeft: Radius.circular(8), topRight:
                Radius.circular(8)), child: Image(width:
                MediaQuery.of(context).size.width, image:
                AssetImage('images/widgets/materialWidgets/mwDialogAlertPanelWidgets/widget_delete.jpg'),
                height: 120, fit: BoxFit.cover), ), 24.height, Text('Delete
                folder?', style: boldTextStyle(color: appStore.textPrimaryColor,
                size: 18)), 16.height, Padding( padding: const
                EdgeInsets.only(left: 16, right: 16), child: Text("This will
                also permanently delete file inside the folder", style:
                secondaryTextStyle(color: appStore.textSecondaryColor)), ),
                16.height, Padding( padding: const EdgeInsets.all(16.0), child:
                Row( children: [ Expanded( child: Container( padding:
                EdgeInsets.all(8), decoration: boxDecoration(color:
                Colors.blueAccent, radius: 8, bgColor:
                appStore.scaffoldBackground), child: Center( child: RichText(
                text: TextSpan( children: [ WidgetSpan(child: Padding(padding:
                EdgeInsets.only(right: 8.0), child: Icon(Icons.close, color:
                Colors.blueAccent, size: 18))), TextSpan(text: "Cancel", style:
                TextStyle(fontSize: 16.0, color: Colors.blueAccent, fontFamily:
                fontRegular)), ], ), ), ), ).onTap(()&braces; finish(context);
                &braces;), ), 16.width, Expanded( child: Container( padding:
                EdgeInsets.all(8), decoration: boxDecoration(bgColor:
                Colors.blueAccent, radius: 8), child: Center( child: RichText(
                text: TextSpan( children: [ WidgetSpan(child: Padding(padding:
                EdgeInsets.only(right: 8.0), child: Icon(Icons.delete, color:
                Colors.white, size: 18))), TextSpan(text: "Delete", style:
                TextStyle(fontSize: 16.0, color: Colors.white, fontFamily:
                fontRegular)), ], ), ), ), ).onTap(()&braces; finish(context);
                &braces;), ) ], ), ), 16.height, ], ), ), );
              </SyntaxHighlighter>
            </Typography>
          </Container>
          <hr />
        </div>
        <div className="col-lg-6">
          <img src={AD_3} alt="AD_3" />

          <h5>1. This example is a Alert Dialog with custom shape.</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                AlertDialog( backgroundColor: Color, content: Column(
                mainAxisSize: MainAxisSize.min, crossAxisAlignment:
                CrossAxisAlignment.start, children: [ Text("Hold On", style:
                TextStyle(color: Color)), Text( "Lorem Ipsum is simply dummy
                text of the printing and typesetting industry.", style:
                TextStyle(color: Color), ), GestureDetector( onTap: () &braces;
                Navigator.of(context).pop(); &braces;, child: Align( alignment:
                Alignment.topRight, child: Container( decoration:
                boxDecoration(bgColor: Color, radius: 10), padding:
                EdgeInsets.fromLTRB(16, 8, 16, 8), child: text("Ok", textColor:
                white, fontSize: 16.0), ), ), ) ], ), contentPadding:
                EdgeInsets.fromLTRB(16, 16, 16, 16), shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.only(topRight:
                Radius.circular(50), bottomLeft: Radius.circular(50))), );
              </SyntaxHighlighter>
            </Typography>
          </Container>
          <hr />
          <img src={AD_4} alt="AD_4" />

          <h5>1. This example is a Warning Alert Dialog .</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                AlertDialog( backgroundColor: Color, content: Container( width:
                MediaQuery.of(context).size.width, child: Column( mainAxisSize:
                MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.center,
                children: [ Stack( alignment: Alignment.center, children: [
                Container(height: 120, color: Color), Column( children: [
                Icon(Icons.warning, color: white, size: 32), Text('OOPs...',
                textAlign: TextAlign.center, style: TextStyle(color: white,
                size: 18)), ], ) ], ), Padding( padding: const
                EdgeInsets.only(left: 16, right: 16), child: Text("Something
                Went Wrong", style: TextStyle()), ), GestureDetector( onTap: ()
                &braces; Navigator.of(context).pop(); &braces;, child:
                Container( decoration: boxDecoration(bgColor: Color, radius:
                10), padding: EdgeInsets.fromLTRB(16, 8, 16, 8), child:
                text("Try again", textColor: white, fontSize: 16.0), ), ), ], ),
                ), contentPadding: EdgeInsets.all(0), );
              </SyntaxHighlighter>
            </Typography>
          </Container>
          <hr />
          <img src={AD_6} alt="AD_6" />

          <h5>1. This example is a Dialog with Input TextField .</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                Dialog( shape: RoundedRectangleBorder( borderRadius:
                BorderRadius.circular(16), ), elevation: 0.0, backgroundColor:
                Colors.transparent, child: Container( padding:
                EdgeInsets.all(16), decoration: new BoxDecoration( color:
                appStore.scaffoldBackground, shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(16), boxShadow: [ BoxShadow(
                color: Colors.black26, blurRadius: 10.0, offset: const
                Offset(0.0, 10.0), ), ], ), child: Column( crossAxisAlignment:
                CrossAxisAlignment.start, mainAxisSize: MainAxisSize.min, // To
                make the card compact children: [ GestureDetector( onTap: ()
                &braces; finish(context); &braces;, child: Container(padding:
                EdgeInsets.all(4), alignment: Alignment.centerRight, child:
                Icon(Icons.close, color: appStore.textPrimaryColor)), ),
                Text('Contact', style: boldTextStyle(color:
                appStore.textPrimaryColor, size: 20)), 16.height, TextFormField(
                cursorColor: appStore.textPrimaryColor, decoration:
                InputDecoration( contentPadding: EdgeInsets.fromLTRB(4, 8, 4,
                8), hintText: 'Enter Contact Number', hintStyle:
                secondaryTextStyle(color: appStore.textSecondaryColor, size:
                16), enabledBorder: UnderlineInputBorder(borderSide:
                BorderSide(color: appStore.textPrimaryColor, width: 0.0)),
                focusedBorder: UnderlineInputBorder(borderSide:
                BorderSide(color: appStore.textPrimaryColor, width: 0.0)), ),
                keyboardType: TextInputType.number, inputFormatters:
                [WhitelistingTextInputFormatter.digitsOnly], style:
                primaryTextStyle(color: appStore.textPrimaryColor), ),
                30.height, GestureDetector( onTap: () &braces; finish(context);
                &braces;, child: Container( width:
                MediaQuery.of(context).size.width, decoration:
                boxDecoration(bgColor: Colors.indigo, radius: 10), padding:
                EdgeInsets.fromLTRB(16, 8, 16, 8), child: Center( child:
                Text("Apply", style: boldTextStyle(color: white)), ), ), ),
                16.height, ], ), ), );
              </SyntaxHighlighter>
            </Typography>
          </Container>
          <hr />
          <img src={AD_8} alt="AD_8" />

          <h5>1. This example is a Dialog with Account Selection .</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                Dialog( shape: RoundedRectangleBorder(borderRadius:
                BorderRadius.circular(8)), elevation: 0.0, backgroundColor:
                Colors.transparent, child: Container( padding:
                EdgeInsets.all(16.0), decoration: new BoxDecoration( color:
                appStore.scaffoldBackground, shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(8), boxShadow: [ BoxShadow(
                color: Colors.black26, blurRadius: 10.0, offset: const
                Offset(0.0, 10.0), ), ], ), width:
                MediaQuery.of(context).size.width, child: Column( mainAxisSize:
                MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start,
                children: [ Row( children: [ GestureDetector( onTap: () &braces;
                finish(context); &braces;, child: Icon(Icons.close, color:
                appStore.textPrimaryColor), ), 16.width, Text('Account', style:
                boldTextStyle(color: appStore.textPrimaryColor, size: 20),
                textAlign: TextAlign.center) ], ), 30.height, Row( children: [
                CircleAvatar( radius: 16, backgroundImage:
                AssetImage('images/widgets/materialWidgets/mwDialogAlertPanelWidgets/ic_user.jpg'),
                ), 10.width, Column( crossAxisAlignment:
                CrossAxisAlignment.start, children: [ Text("John Smith", style:
                primaryTextStyle(color: appStore.textPrimaryColor, size: 20)),
                FittedBox(child: Text("Johnsmith@gmail.com", style:
                secondaryTextStyle(color: appStore.textSecondaryColor, size:
                20))), ], ) ], ), 20.height, Center( child: Container(
                decoration: boxDecoration(radius: 20, color:
                Theme.of(context).dividerColor, bgColor:
                appStore.scaffoldBackground), padding: EdgeInsets.fromLTRB(16,
                4, 16, 4), child: Text("Manage your account", style:
                primaryTextStyle(color: appStore.textPrimaryColor, size: 20)),
                ), ), 20.height, Divider(thickness: 1, color:
                Theme.of(context).dividerColor), 20.height, RichText( text:
                TextSpan( children: [ WidgetSpan(child: Padding(padding:
                EdgeInsets.only(right: 16.0), child: Icon(Icons.settings, color:
                appStore.iconColor, size: 18))), TextSpan(text: "Photo
                Settings", style: TextStyle(fontSize: 16.0, color:
                appStore.textPrimaryColor, fontFamily: fontRegular)), ], ), ),
                20.height, RichText( text: TextSpan( children: [
                WidgetSpan(child: Padding(padding: EdgeInsets.only(right: 16.0),
                child: Icon(Icons.help_outline, color: appStore.iconColor, size:
                18))), TextSpan(text: "Help and feedback", style:
                TextStyle(fontSize: 16.0, color: appStore.textPrimaryColor,
                fontFamily: fontRegular)), ], ), ), 20.height,
                Divider(thickness: 1, color: Theme.of(context).dividerColor),
                20.height, Row( mainAxisAlignment: MainAxisAlignment.center,
                children: [ Text("Privacy", style: secondaryTextStyle(color:
                appStore.textSecondaryColor)), Container(decoration:
                BoxDecoration(shape: BoxShape.circle, color:
                appStore.textSecondaryColor), width: 4, height: 4, margin:
                EdgeInsets.only(left: 8, right: 8)), Text("Terms", style:
                secondaryTextStyle(color: appStore.textSecondaryColor)),
                Container(decoration: BoxDecoration(shape: BoxShape.circle,
                color: appStore.textSecondaryColor), width: 4, height: 4,
                margin: EdgeInsets.only(left: 8, right: 8)), Text("Policy",
                style: secondaryTextStyle(color: appStore.textSecondaryColor))
                ], ), 16.height, ], ), ), );
              </SyntaxHighlighter>
            </Typography>
          </Container>
          <hr />
        </div>
      </div>
    </div>
  );
};

export default AlertDialog;
