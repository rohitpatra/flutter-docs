import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import SNB_1 from "../../../assets/Dialog_Panels/SNB_1.png";
import SNB_2 from "../../../assets/Dialog_Panels/SNB_2.png";
import SNB_3 from "../../../assets/Dialog_Panels/SNB_3.png";
import SNB_4 from "../../../assets/Dialog_Panels/SNB_4.png";
import SNB_5 from "../../../assets/Dialog_Panels/SNB_5.png";
import SNB_6 from "../../../assets/Dialog_Panels/SNB_6.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const SnackBars = (props: Props) => {
  return (
    <div className="container m-0">
      <div className="row">
        <div className="col-lg-6">
          <h2>
            <u>Snack Bar</u>
          </h2>

          <img src={SNB_1} alt="SNB_1" />

          <h5>1. This example is a SnackBar with Actions.</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                SnackBar( content: Text('This is SnackBar with Action', style:
                TextStyle(color: Colors.white)), action: SnackBarAction( label:
                'Undo', textColor: Colors.white, onPressed: () &braces;
                Fluttertoast.showToast(msg: 'Undo pressed'); &braces;), ),
              </SyntaxHighlighter>
            </Typography>
          </Container>
          <hr />
          <img src={SNB_2} alt="SNB_2" />

          <h5>1. This example is a Customized SnackBar .</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                SnackBar( content: Text('This is custom SnackBar', style:
                TextStyle(color: Colors.white)), backgroundColor: Color, ));
              </SyntaxHighlighter>
            </Typography>
          </Container>
          <hr />
          <img src={SNB_3} alt="SNB_3" />

          <h5>1. This example is a Infinite SnackBar with action.</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                SnackBar( content: Text('This is infinite SnackBar with some
                info', style: TextStyle(color: Colors.white)), duration:
                Duration(days: 365), action: SnackBarAction( label: 'Ok',
                onPressed: () &braces; Do Something &braces;), ));{" "}
              </SyntaxHighlighter>
            </Typography>
          </Container>
          <hr />
        </div>
        <div className="col-lg-6 pt-5">
          <img src={SNB_5} alt="SNB_5" />

          <h5>1. This example is a Rounded Floating SnackBar.</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                scaffoldKey.currentState.showSnackBar(SnackBar( content:
                Text('This is Rounded SnackBar', style: TextStyle(color:
                Colors.white)), shape: RoundedRectangleBorder( borderRadius:
                BorderRadius.all(Radius.circular(30))), behavior:
                SnackBarBehavior.floating, ));
              </SyntaxHighlighter>
            </Typography>
          </Container>
          <hr />
          <img src={SNB_6} alt="SNB_6" />

          <h5>1. This example is a Bordered SnackBar.</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                SnackBar( content: Text('This is Bordered SnackBar', style:
                TextStyle(color: Color)), shape: Border.all(color: Color),
                backgroundColor: Color, behavior: SnackBarBehavior.floating, );
              </SyntaxHighlighter>
            </Typography>
          </Container>
          <hr />
          <img src={SNB_4} alt="SNB_4" />

          <h5>1. This example is a Floating SnackBar .</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                SnackBar( content: Text('This is Floating SnackBar', style:
                TextStyle(color: Colors.white)), behavior:
                SnackBarBehavior.floating, );
              </SyntaxHighlighter>
            </Typography>
          </Container>
        </div>
      </div>
    </div>
  );
};

export default SnackBars;
