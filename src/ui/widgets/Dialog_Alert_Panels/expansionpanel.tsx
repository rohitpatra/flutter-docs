import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import ExP from "../../../assets/Dialog_Panels/ExP.png";

interface Props {}

const ExpansionPanel = (props: Props) => {
  return (
    <div className="container m-0">
      <h2>
        <u>Expansion Panel</u>
      </h2>
      <img src={ExP} alt="Exp" />

      <h5>1. This example is a Expansion Panel Sheet.</h5>

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            class _MyStatefulWidgetState extends State&lt;MyStatefulWidget&gt;
            &braces; List _data = generateItems(8); @override Widget
            build(BuildContext context) &braces; return SingleChildScrollView(
            child: Container( child: Theme( data:
            Theme.of(context).copyWith(cardColor: appStore.scaffoldBackground),
            child: _buildPanel(), ), ), ); &braces; Widget _buildPanel()
            &braces; return CustomTheme( child: ExpansionPanelList(
            dividerColor: Theme.of(context).dividerColor.withOpacity(0.5),
            expansionCallback: (int index, bool isExpanded) &braces; setState(()
            &braces; _data[index].isExpanded = !isExpanded; &braces;); &braces;,
            children: _data.map&lt;ExpansionPanel&gt;((Item item) &braces;
            return ExpansionPanel( headerBuilder: (BuildContext context, bool
            isExpanded) &braces; return ListTile(title: Text(item.headerValue,
            style: primaryTextStyle(color: appStore.textPrimaryColor)));
            &braces;, body: ListTile( title: Text(item.expandedValue, style:
            primaryTextStyle(color: appStore.textPrimaryColor)), subtitle:
            Padding( padding: EdgeInsets.only(bottom: 16), child: Text('Lorem
            Ipsum is simply dummy text of the printing and typesetting
            industry.', style: boldTextStyle(color:
            appStore.textSecondaryColor)), ), ), isExpanded: item.isExpanded, );
            &braces;).toList(), ), ); &braces; &braces;
          </SyntaxHighlighter>
        </Typography>
      </Container>
      <h5>2. In this example a local variable _data is declared.</h5>
      <h5>
        3. Custom method generateItems is declared and passed to _data variable.
      </h5>

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <div
            style={{
              background: "#ffffff",
              overflow: "auto",
              width: "auto",
              border: "solid gray",
              borderWidth: ".1em .1em .1em .8em",
              padding: ".2em .6em",
            }}
          >
            <pre style={{ margin: 0, lineHeight: "125%" }}>
              List<span style={{ color: "#333333" }}>&lt;</span>Item
              <span style={{ color: "#333333" }}>&gt;</span> _data{" "}
              <span style={{ color: "#333333" }}>=</span> generateItems(
              <span style={{ color: "#6600EE", fontWeight: "bold" }}>8</span>);
              {"\n"}
              {"\n"}List<span style={{ color: "#333333" }}>&lt;</span>Item
              <span style={{ color: "#333333" }}>&gt;</span> generateItems(
              <span style={{ color: "#333399", fontWeight: "bold" }}>
                int
              </span>{" "}
              numberOfItems) {"{"}
              {"\n"}
              {"  "}
              <span style={{ color: "#008800", fontWeight: "bold" }}>
                return
              </span>{" "}
              List.generate(numberOfItems, (
              <span style={{ color: "#333399", fontWeight: "bold" }}>int</span>{" "}
              index) {"{"}
              {"\n"}
              {"    "}
              <span style={{ color: "#008800", fontWeight: "bold" }}>
                return
              </span>{" "}
              Item(
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                headerValue:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>'Item </span>
              <span style={{ backgroundColor: "#eeeeee" }}>$</span>index
              <span style={{ backgroundColor: "#fff0f0" }}>'</span>,{" "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                expandedValue:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>
                'This is item number{" "}
              </span>
              <span style={{ backgroundColor: "#eeeeee" }}>$</span>index
              <span style={{ backgroundColor: "#fff0f0" }}>'</span>);{"\n"}
              {"  "}
              {"}"});{"\n"}
              {"}"}
              {"\n"}
              {"\n"}
              <span style={{ color: "#008800", fontWeight: "bold" }}>
                class
              </span>{" "}
              <span style={{ color: "#BB0066", fontWeight: "bold" }}>
                MyStatefulWidget
              </span>{" "}
              <span style={{ color: "#008800", fontWeight: "bold" }}>
                extends
              </span>{" "}
              StatefulWidget {"{"}
              {"\n"}
              {"  "}MyStatefulWidget({"{"}Key key{"}"}){" "}
              <span style={{ color: "#333333" }}>:</span>{" "}
              <span style={{ color: "#008800", fontWeight: "bold" }}>
                super
              </span>
              (
              <span style={{ color: "#997700", fontWeight: "bold" }}>key:</span>{" "}
              key);
              {"\n"}
              {"\n"}
              {"  "}
              <span style={{ color: "#FF0000", backgroundColor: "#FFAAAA" }}>
                @
              </span>
              override{"\n"}
              {"  "}_MyStatefulWidgetState createState(){" "}
              <span style={{ color: "#333333" }}>=&gt;</span>{" "}
              _MyStatefulWidgetState();
              {"\n"}
              {"}"}
              {"\n"}
            </pre>
          </div>
        </Typography>
      </Container>
    </div>
  );
};

export default ExpansionPanel;
