import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import image_1 from "../../assets/screen_appbar.jpg";
import image_2 from "../../assets/image_2.jpeg";
import image_3 from "../../assets/image_3.jpeg";
import image_4 from "../../assets/image_4.jpeg";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const styles = {
  width: "25%",
};

const AppBar = (props: Props) => {
  return (
    <>
      <h2>
        <u>App Bar</u>
      </h2>
      <h5>1. This Widget is simple design with no children or functions</h5>

      <img style={styles} src={image_1} alt="image_1" />
      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            import'package:flutter/material.dart';AppBar(title:Text('Without
            Back Button & Actions',style:boldTextStyle(color:
            appStore.textPrimaryColor,),),backgroundColor:
            appStore.appBarColor,automaticallyImplyLeading:false,),
          </SyntaxHighlighter>
        </Typography>
      </Container>
      <hr />
      <h5>
        2. Center the title in AppBar with setting center title property to
        true.
      </h5>

      <img style={styles} src={image_2} alt="image_2" />
      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter language="dart" style={materialDark}>
            import'package:flutter/material.dart'; AppBar( title: Text('Center
            Title', style: boldTextStyle(color: appStore.textPrimaryColor)),
            centerTitle: true, backgroundColor: appStore.appBarColor, leading:
            leadingWidget(), ),
          </SyntaxHighlighter>
        </Typography>
      </Container>
      <h5>
        2.1 leadingWidget() is used and passed to leading attribute of AppBar
        widget. Here BackButton Widget is returned.
      </h5>

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <div
            style={{
              background: "#ffffff",
              overflow: "auto",
              width: "auto",
              border: "solid gray",
              borderWidth: ".1em .1em .1em .8em",
              padding: ".2em .6em",
            }}
          >
            <pre style={{ margin: 0, lineHeight: "125%" }}>
              Widget leadingWidget() {"{"}
              {"\n"}
              {"    "}
              <span style={{ color: "#008800", fontWeight: "bold" }}>
                return
              </span>{" "}
              BackButton({"\n"}
              {"     "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                onPressed:
              </span>{" "}
              () {"{"}
              {"\n"}
              {"                  "}toast(
              <span style={{ backgroundColor: "#fff0f0" }}>'Back button'</span>
              );{"\n"}
              {"                    "}
              {"}"},{"\n"}
              {"                "});{"\n"}
              {"           "}
              {"}"};{"\n"}
            </pre>
          </div>
        </Typography>
      </Container>
      <hr />
      <h5> 3. This Widget is simple design With Single Action</h5>

      <img style={styles} src={image_3} alt="image_3" />
      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter language="dart" style={materialDark}>
            AppBar( title: Text('With Single Action', style:
            boldTextStyle(color: appStore.textPrimaryColor)), leading:
            leadingWidget(), actions: [ IconButton( icon: Icon(Icons.settings,
            color: appStore.textPrimaryColor), onPressed: () &rbrace;
            toast('Settings'); &rbrace;, ) ], backgroundColor:
            appStore.appBarColor, ),
          </SyntaxHighlighter>
        </Typography>
      </Container>
      <hr />
      <h5> 4. This Widget is simple design With Multiple Actions</h5>

      <img style={styles} src={image_4} alt="image_4" />
      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter language="dart" style={materialDark}>
            AppBar( backgroundColor: appStore.appBarColor, leading: IconButton(
            icon: Icon(Icons.menu, color: appStore.textPrimaryColor), onPressed:
            () `&rbrace;` toast('Drawer'); `&rbrace;`, ), title: Text('Page
            Title', style: boldTextStyle(color: appStore.textPrimaryColor)),
            actions: [ IconButton( icon: Icon(Icons.share, color:
            appStore.textPrimaryColor), onPressed: () `&rbrace;` toast('Share');
            `&rbrace;`, ), IconButton( icon: Icon(Icons.search, color:
            appStore.textPrimaryColor), onPressed: () `&rbrace;`
            toast('Search'); `&rbrace;`, ), IconButton( icon:
            Icon(Icons.more_vert, color: appStore.textPrimaryColor), onPressed:
            () `&rbrace;` toast('Menu'); `&rbrace;`, ), ], ),
          </SyntaxHighlighter>
        </Typography>
      </Container>
    </>
  );
};

export default AppBar;
