import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import TabBar_1 from "../../assets/TabBar/TabBar_1.png";
import TabBar_2 from "../../assets/TabBar/TabBar_2.png";
import TabBar_3 from "../../assets/TabBar/TabBar_3.png";
import TabBar_4 from "../../assets/TabBar/TabBar_4.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const TabBar = (props: Props) => {
  return (
    <>
      <div className="container m-0">
        <div className="col">
          <h2>
            <u>Tab Bar</u>
          </h2>
          <img src={TabBar_1} alt="TabBar_1" />
        </div>
        <div className="col">
          <h5>1.This example is a Simple TabBar with labels.</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                SafeArea( child: DefaultTabController( length: 3, child:
                Scaffold( appBar: AppBar( backgroundColor: Color, title: Text(
                'Simple TabBar', style: boldTextStyle(color: Color, size: 20),
                ), bottom: TabBar( onTap: (index) &braces; print(index);
                &braces;, indicatorColor: Colors.blue, labelColor: Color,
                labelStyle: TextStyle(), tabs: [ Tab( text: "Home", ), Tab(
                text: "Articles", ), Tab( text: "User", ), ], ), ), body:
                TabBarView( children: [ Container( padding: EdgeInsets.all(16),
                alignment: Alignment.center, width: context.width(), child:
                Column( crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center, mainAxisSize:
                MainAxisSize.min, children: [ Text( 'Home', style: TextStyle(
                color: Color, fontSize: 24), ), 15.height, ], ), ), Container(
                padding: EdgeInsets.all(16), alignment: Alignment.center, width:
                context.width(), child: Column( crossAxisAlignment:
                CrossAxisAlignment.center, mainAxisAlignment:
                MainAxisAlignment.center, mainAxisSize: MainAxisSize.min,
                children: [ Text( 'Articles', style: TextStyle( color: Color,
                fontSize: 24), ), 15.height, ], ), ), Container( padding:
                EdgeInsets.all(16), alignment: Alignment.center, width:
                context.width(), child: Column( crossAxisAlignment:
                CrossAxisAlignment.center, mainAxisAlignment:
                MainAxisAlignment.center, mainAxisSize: MainAxisSize.min,
                children: [ Text( 'User', style: TextStyle( color: Color,
                fontSize: 24), ), 15.height, ], ), ), ], ), ), ), );
              </SyntaxHighlighter>
            </Typography>
          </Container>
        </div>
      </div>
      <hr />
      <div className="container m-0">
        <div className="col">
          <img src={TabBar_2} alt="TabBar_2" />
        </div>
        <div className="col pt-0">
          <h5>1.This example is a Simple TabBar with Title and Icons.</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                SafeArea( child: DefaultTabController( length: 3, child:
                Scaffold( appBar: AppBar( backgroundColor: Color, title:
                Text('TabBar with Title and Icon', style: TextStyle(color:
                Color, size: 20)), bottom: TabBar( onTap: (index) &braces;
                print(index); &braces;, labelStyle: primaryTextStyle(),
                indicatorColor: Colors.blue, physics: BouncingScrollPhysics(),
                labelColor: Color, tabs: [ Tab( child: Row( mainAxisAlignment:
                MainAxisAlignment.center, crossAxisAlignment:
                CrossAxisAlignment.center, mainAxisSize: MainAxisSize.min,
                children: [ Icon( Icons.home, color: Color, ), 5.width, Text(
                'Home', ), ], ), ), Tab( child: Row( mainAxisAlignment:
                MainAxisAlignment.center, crossAxisAlignment:
                CrossAxisAlignment.center, mainAxisSize: MainAxisSize.min,
                children: [ Icon( Icons.insert_drive_file, color: Color, ),
                5.width, Text( 'Article', ), ], ), ), Tab( child: Row(
                mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment:
                CrossAxisAlignment.center, mainAxisSize: MainAxisSize.min,
                children: [ Icon( Icons.supervised_user_circle, color: Color, ),
                5.width, Text( 'User', ), ], ), ), ], ), ), body: TabBarView(
                children: [ Container( padding: EdgeInsets.all(16), alignment:
                Alignment.center, width: context.width(), child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center, mainAxisSize:
                MainAxisSize.min, children: [ Text( 'Home', style:
                TextStyle(color: Color, fontSize: 24), ), 15.height, Column(
                mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment:
                CrossAxisAlignment.start, children: [], ) ], ), ), Container(
                padding: EdgeInsets.all(16), alignment: Alignment.center, width:
                context.width(), child: Column( crossAxisAlignment:
                CrossAxisAlignment.center, mainAxisAlignment:
                MainAxisAlignment.center, mainAxisSize: MainAxisSize.min,
                children: [ Text( 'Articles', style: TextStyle(color: Color,
                fontSize: 24), ), 15.height, Column( mainAxisAlignment:
                MainAxisAlignment.start, crossAxisAlignment:
                CrossAxisAlignment.start, children: [], ) ], ), ), Container(
                padding: EdgeInsets.all(16), alignment: Alignment.center, width:
                context.width(), child: Column( crossAxisAlignment:
                CrossAxisAlignment.center, mainAxisAlignment:
                MainAxisAlignment.center, mainAxisSize: MainAxisSize.min,
                children: [ Text( 'User', style: TextStyle(color: Color,
                fontSize: 24), ), 15.height, Column( mainAxisAlignment:
                MainAxisAlignment.start, crossAxisAlignment:
                CrossAxisAlignment.start, children: [], ) ], ), ), ], ), ), ),
                );{" "}
              </SyntaxHighlighter>
            </Typography>
          </Container>
        </div>
      </div>
      <hr />
      <div className="container m-0">
        <div className="col">
          <img src={TabBar_3} alt="TabBar_3" />
        </div>
        <div className="col pt-0">
          <h5>1.This example is a Simple TabBar with Only Icons.</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                SafeArea( child: Scaffold( appBar: AppBar( backgroundColor:
                appStore.appBarColor, title: Text( 'TabBar with Icon', style:
                boldTextStyle(color: Color, size: 20), ), bottom: TabBar(
                controller: _tabController, onTap: (index) &rbrace;
                print(index); &rbrace;, indicatorColor: Colors.blue, tabs: [
                Tab( icon: Image.asset(
                'images/widgets/materialWidgets/mwAppStructureWidgets/TabBar/home.png',
                height: 25, width: 25, color: _tabController.index != 0 ? Color
                : Colors.blue, )), Tab( icon: Image.asset(
                'images/widgets/materialWidgets/mwAppStructureWidgets/TabBar/marketplace.png',
                height: 25, width: 25, color: _tabController.index != 1 ? Color
                : Colors.blue, )), Tab( icon: Image.asset(
                'images/widgets/materialWidgets/mwAppStructureWidgets/TabBar/group.png',
                height: 25, width: 25, color: _tabController.index != 2 ? Color
                : Colors.blue, )), Tab( icon: Image.asset(
                'images/widgets/materialWidgets/mwAppStructureWidgets/TabBar/video.png',
                height: 25, width: 25, color: _tabController.index != 3 ? Color
                : Colors.blue, )), Tab( icon: Image.asset(
                'images/widgets/materialWidgets/mwAppStructureWidgets/TabBar/notification.png',
                height: 25, width: 25, color: _tabController.index != 4 ? Color
                : Colors.blue, )), Tab( icon: Image.asset(
                'images/widgets/materialWidgets/mwAppStructureWidgets/TabBar/list.png',
                height: 25, width: 25, color: _tabController.index != 5 ? Color
                : Colors.blue, )), ], ), ), body: TabBarView( controller:
                _tabController, children: [ Container( padding:
                EdgeInsets.all(16), alignment: Alignment.center, width:
                context.width(), child: Column( crossAxisAlignment:
                CrossAxisAlignment.center, mainAxisAlignment:
                MainAxisAlignment.center, mainAxisSize: MainAxisSize.min,
                children: [ Text( 'Home', style: TextStyle(color: Color,
                fontSize: 24), ), 15.height, Column( mainAxisAlignment:
                MainAxisAlignment.start, crossAxisAlignment:
                CrossAxisAlignment.start, children: [], ) ], ), ), Container(
                padding: EdgeInsets.all(16), alignment: Alignment.center, width:
                context.width(), child: Column( crossAxisAlignment:
                CrossAxisAlignment.center, mainAxisAlignment:
                MainAxisAlignment.center, mainAxisSize: MainAxisSize.min,
                children: [ Text( 'Marketplace', style: TextStyle(color: Color,
                fontSize: 24), ), 15.height, Column( mainAxisAlignment:
                MainAxisAlignment.start, crossAxisAlignment:
                CrossAxisAlignment.start, children: [], ) ], ), ), Container(
                padding: EdgeInsets.all(16), alignment: Alignment.center, width:
                context.width(), child: Column( crossAxisAlignment:
                CrossAxisAlignment.center, mainAxisAlignment:
                MainAxisAlignment.center, mainAxisSize: MainAxisSize.min,
                children: [ Text( 'Groups', style: TextStyle(color: Color,
                fontSize: 24), ), 15.height, Column( mainAxisAlignment:
                MainAxisAlignment.start, crossAxisAlignment:
                CrossAxisAlignment.start, children: [], ) ], ), ), Container(
                padding: EdgeInsets.all(16), alignment: Alignment.center, width:
                context.width(), child: Column( crossAxisAlignment:
                CrossAxisAlignment.center, mainAxisAlignment:
                MainAxisAlignment.center, mainAxisSize: MainAxisSize.min,
                children: [ Text( 'Watch', style: TextStyle(color: Color,
                fontSize: 24), ), 15.height, Column( mainAxisAlignment:
                MainAxisAlignment.start, crossAxisAlignment:
                CrossAxisAlignment.start, children: [], ) ], ), ), Container(
                padding: EdgeInsets.all(16), alignment: Alignment.center, width:
                context.width(), child: Column( crossAxisAlignment:
                CrossAxisAlignment.center, mainAxisAlignment:
                MainAxisAlignment.center, mainAxisSize: MainAxisSize.min,
                children: [ Text( 'Notifications', style: TextStyle(color:
                Color, fontSize: 24), ), 15.height, Column( mainAxisAlignment:
                MainAxisAlignment.start, crossAxisAlignment:
                CrossAxisAlignment.start, children: [], ) ], ), ), Container(
                padding: EdgeInsets.all(16), alignment: Alignment.center, width:
                context.width(), child: Column( crossAxisAlignment:
                CrossAxisAlignment.center, mainAxisAlignment:
                MainAxisAlignment.center, mainAxisSize: MainAxisSize.min,
                children: [ Text( 'Menu', style: TextStyle(color: Color,
                fontSize: 24), ), 15.height, Column( mainAxisAlignment:
                MainAxisAlignment.start, crossAxisAlignment:
                CrossAxisAlignment.start, children: [], ) ], ), ), ], ), ));
              </SyntaxHighlighter>
            </Typography>
          </Container>
          <hr />

          <h5>
            2.A local variable _tabController is iniatlized inside initState()
            and passed to controller props.
          </h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <div
                style={{
                  background: "#ffffff",
                  overflow: "auto",
                  width: "auto",
                  border: "solid gray",
                  borderWidth: ".1em .1em .1em .8em",
                  padding: ".2em .6em",
                }}
              >
                <pre style={{ margin: 0, lineHeight: "125%" }}>
                  {" "}
                  TabController _tabController;{"\n"}
                  <span
                    style={{ color: "#FF0000", backgroundColor: "#FFAAAA" }}
                  >
                    @
                  </span>
                  override{"\n"}
                  {"  "}
                  <span style={{ color: "#333399", fontWeight: "bold" }}>
                    void
                  </span>{" "}
                  initState() {"{"}
                  {"\n"}
                  {"    "}
                  <span style={{ color: "#008800", fontWeight: "bold" }}>
                    super
                  </span>
                  .initState();{"\n"}
                  {"    "}_tabController{" "}
                  <span style={{ color: "#333333" }}>=</span>{" "}
                  <span style={{ color: "#008800", fontWeight: "bold" }}>
                    new
                  </span>{" "}
                  TabController(
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    vsync:
                  </span>{" "}
                  <span style={{ color: "#008800", fontWeight: "bold" }}>
                    this
                  </span>
                  ,{" "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    length:
                  </span>{" "}
                  <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                    6
                  </span>
                  );{"\n"}
                  {"    "}_tabController.addListener(_handleTabSelection);{"\n"}
                  {"  "}
                  {"}"}
                  {"\n"}
                </pre>
              </div>
            </Typography>
          </Container>
        </div>
      </div>
      <hr />
      <div className="container m-0">
        <div className="col">
          <img src={TabBar_4} alt="TabBar_4" />
        </div>
        <div className="col pt-0">
          <h5>1.This example is a Scrollable TabBar with Title and Icons.</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                SafeArea( child: DefaultTabController( length: 6, child:
                Scaffold( appBar: AppBar( backgroundColor: Color, title: Text(
                'Scrollable Tab', style: TextStyle(color: Color, size: 20), ),
                bottom: TabBar( onTap: (index) &braces; print(index); &braces;,
                isScrollable: true, labelStyle: TextStyle(), indicatorColor:
                Colors.blue, tabs: [ Tab( child: Text('Home'), ), Tab( child:
                Text('MarketPlace'), ), Tab( child: Text('Group'), ), Tab(
                child: Text('Watch'), ), Tab( child: Text('Notifications'), ),
                Tab( child: Text('Menu'), ), ], ), ), body: TabBarView(
                children: [ Container( padding: EdgeInsets.all(16), alignment:
                Alignment.center, width: context.width(), child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center, mainAxisSize:
                MainAxisSize.min, children: [ Text( 'Home', style:
                TextStyle(color: Color, fontSize: 24), ), ], ), ), Container(
                padding: EdgeInsets.all(16), alignment: Alignment.center, width:
                context.width(), child: Column( crossAxisAlignment:
                CrossAxisAlignment.center, mainAxisAlignment:
                MainAxisAlignment.center, mainAxisSize: MainAxisSize.min,
                children: [ Text( 'MarketPlace', style: TextStyle(color: Color,
                fontSize: 24), ), ], ), ), Container( padding:
                EdgeInsets.all(16), alignment: Alignment.center, width:
                context.width(), child: Column( crossAxisAlignment:
                CrossAxisAlignment.center, mainAxisAlignment:
                MainAxisAlignment.center, mainAxisSize: MainAxisSize.min,
                children: [ Text( 'Group', style: TextStyle(color: Color,
                fontSize: 24), ), ], ), ), Container( padding:
                EdgeInsets.all(16), alignment: Alignment.center, width:
                context.width(), child: Column( crossAxisAlignment:
                CrossAxisAlignment.center, mainAxisAlignment:
                MainAxisAlignment.center, mainAxisSize: MainAxisSize.min,
                children: [ Text( 'Watch', style: TextStyle(color: Color,
                fontSize: 24), ), ], ), ), Container( padding:
                EdgeInsets.all(16), alignment: Alignment.center, width:
                context.width(), child: Column( crossAxisAlignment:
                CrossAxisAlignment.center, mainAxisAlignment:
                MainAxisAlignment.center, mainAxisSize: MainAxisSize.min,
                children: [ Text( 'Notifications', style: TextStyle(color:
                Color, fontSize: 24), ), ], ), ), Container( padding:
                EdgeInsets.all(16), alignment: Alignment.center, width:
                context.width(), child: Column( crossAxisAlignment:
                CrossAxisAlignment.center, mainAxisAlignment:
                MainAxisAlignment.center, mainAxisSize: MainAxisSize.min,
                children: [ Text( 'Menu', style: TextStyle(color: Color,
                fontSize: 24), ), ], ), ), ], ), ), ), );
              </SyntaxHighlighter>
            </Typography>
          </Container>
        </div>
      </div>
    </>
  );
};

export default TabBar;
