import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import SAB_1 from "../../assets/SliverAppbar/SAB_1.png";
import SAB_2 from "../../assets/SliverAppbar/SAB_2.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const SliverAppBar = (props: Props) => {
  return (
    <>
      <div className="container m-0">
        <div className="row">
          <div className="col-lg-6">
            <h2>
              <u>Sliver App Bar</u>
            </h2>
            <img src={SAB_1} alt="SAB_1" />
          </div>
          <div className="col-lg-6 pt-5">
            <h5>1.This example is a Sliver AppBar with Listview.</h5>

            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  Scaffold( body: NestedScrollView( headerSliverBuilder:
                  (BuildContext context, bool innerBoxIsScrolled) &braces;
                  return [ SliverAppBar( leading: IconButton( icon:
                  Icon(Icons.filter_1), color: Color onPressed: () &braces; Do
                  something &braces;, ), expandedHeight: 220.0, floating: true,
                  pinned: true, snap: false, elevation: 50, backgroundColor:
                  Color, flexibleSpace: FlexibleSpaceBar( centerTitle: true,
                  title: Text('SliverAppBar with ListView', style:
                  TextStyle(color: Colors.white)), background:
                  CachedNetworkImage( placeholder: Image.asset('images.jpg',
                  fit: BoxFit.cover); imageUrl:
                  'https://images.pexels.com/photos/443356/pexels-photo-443356.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
                  fit: BoxFit.cover, )), ), ]; &braces;, body: Container( child:
                  ListView.builder( itemCount: 50, itemBuilder: (BuildContext
                  context, int index) &braces; return ListTile( title: Text(
                  'Item $&braces;index.toString()&braces;', style:
                  TextStyle(fontSize: 20.0, color: Colors.black), ), );
                  &braces;), ), ), );
                </SyntaxHighlighter>
              </Typography>
            </Container>
            <hr />
            <h5>2.The Required Imports used here as follows.</h5>

            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <div
                  style={{
                    background: "#ffffff",
                    overflow: "auto",
                    width: "auto",
                    border: "solid gray",
                    borderWidth: ".1em .1em .1em .8em",
                    padding: ".2em .6em",
                  }}
                >
                  <pre style={{ margin: 0, lineHeight: "125%" }}>
                    <span style={{ color: "#008800", fontWeight: "bold" }}>
                      import
                    </span>{" "}
                    <span style={{ backgroundColor: "#fff0f0" }}>
                      'package:cached_network_image/cached_network_image.dart'
                    </span>
                    ;{"\n"}
                    <span style={{ color: "#008800", fontWeight: "bold" }}>
                      import
                    </span>{" "}
                    <span style={{ backgroundColor: "#fff0f0" }}>
                      'package:flutter/material.dart'
                    </span>
                    ;{"\n"}
                  </pre>
                </div>
              </Typography>
            </Container>
          </div>
        </div>
        <hr />
        <div className="row">
          <div className="col-lg-6">
            <img src={SAB_2} alt="SAB_2" />
          </div>
          <div className="col-lg-6">
            <h5>1.This example is a Sliver ApppBar with Parallax effect.</h5>

            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <SyntaxHighlighter
                  language="dart"
                  style={materialDark}
                  wrapLines={true}
                >
                  Scaffold( body: NestedScrollView( headerSliverBuilder:
                  ((BuildContext context, bool innerBoxIsScrolled) &braces;
                  return [ SliverAppBar( expandedHeight: 250, pinned: true,
                  flexibleSpace: FlexibleSpaceBar( background:
                  CachedNetworkImage( imageUrl:
                  "https://images.pexels.com/photos/443356/pexels-photo-443356.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
                  fit: BoxFit.cover, ), collapseMode: CollapseMode.parallax,
                  title: Text( 'Parallax SliverAppBar', style: TextStyle(),
                  maxLines: 1, overflow: TextOverflow.ellipsis,
                  ).visible(innerBoxIsScrolled), ), backgroundColor: Color,
                  leading: BackButton( color: Color, onPressed: () async
                  &braces; finish(context); &braces;, ), ), ]; &braces;), body:
                  Container( padding: EdgeInsets.all(16), child:
                  SingleChildScrollView( child: Column( children: [ Text(text,
                  textAlign: TextAlign.justify, style: TextStyle()), ], ), ), ),
                  ), );
                </SyntaxHighlighter>
              </Typography>
            </Container>
            <hr />
            <h5>2.The Required Imports used here as follows.</h5>

            <Container className="m-0">
              <Typography component="div" style={{ height: "50%" }}>
                <div
                  style={{
                    background: "#ffffff",
                    overflow: "auto",
                    width: "auto",
                    border: "solid gray",
                    borderWidth: ".1em .1em .1em .8em",
                    padding: ".2em .6em",
                  }}
                >
                  <pre style={{ margin: 0, lineHeight: "125%" }}>
                    <span style={{ color: "#008800", fontWeight: "bold" }}>
                      import
                    </span>{" "}
                    <span style={{ backgroundColor: "#fff0f0" }}>
                      'package:cached_network_image/cached_network_image.dart'
                    </span>
                    ;{"\n"}
                    <span style={{ color: "#008800", fontWeight: "bold" }}>
                      import
                    </span>{" "}
                    <span style={{ backgroundColor: "#fff0f0" }}>
                      'package:flutter/material.dart'
                    </span>
                    ;{"\n"}
                  </pre>
                </div>
              </Typography>
            </Container>
          </div>
        </div>
      </div>
    </>
  );
};

export default SliverAppBar;
