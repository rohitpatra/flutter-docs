import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import text_1 from "../../../assets/Info_Display/text_1.png";
import text_2 from "../../../assets/Info_Display/text_2.png";
import text_3 from "../../../assets/Info_Display/text_3.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const RichText = (props: Props) => {
  return (
    <div className="container m-0">
      <h2>
        <u>Rich Text</u>
      </h2>

      <img src={text_1} alt="text_1" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            RichText( text: TextSpan( children: &lt;TextSpan&gt;[ TextSpan(text:
            "Don't have an account?", style: primaryTextStyle(color:
            appStore.textPrimaryColor)), TextSpan( text: ' Sign Up ',
            recognizer: TapGestureRecognizer() ..onTap = () &braces; Do
            Something &braces;, style: primaryTextStyle(color: Colors.blue), ),
            ], ), ),
          </SyntaxHighlighter>
        </Typography>
      </Container>
      <hr />

      <img src={text_2} alt="text_2" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            RichText( text: TextSpan( style:
            Theme.of(context).textTheme.bodyText2, children: [ WidgetSpan(child:
            Padding(padding: EdgeInsets.only(right: 10), child:
            Icon(Icons.settings, size: 18))), TextSpan(text: "Settings", style:
            TextStyle(color: Color)), ], ), ), RichText( text: TextSpan(
            children: [ TextSpan(text: "Profile", style: TextStyle(color:
            Color)), WidgetSpan(child: Padding(padding: EdgeInsets.only(left:
            10), child: Icon(Icons.person, size: 18))), ], ), ),
          </SyntaxHighlighter>
        </Typography>
      </Container>

      <hr />

      <img src={text_3} alt="text_3" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            RichText( text: TextSpan( children: &lt;TextSpan&gt;[ TextSpan(
            text: 'Click', recognizer: TapGestureRecognizer() ..onTap = ()
            &braces; Do Something &braces;, style: TextStyle(fontWeight:
            FontWeight.normal, fontSize: 16, decoration:
            TextDecoration.underline, color: Colors.blue), ), TextSpan(text: "
            here", style: primaryTextStyle()) ], ), ), 10.height, RichText(
            text: TextSpan( children: &lt;TextSpan&gt;[TextSpan(text: 'Bold',
            recognizer: TapGestureRecognizer()..onTap = () &braces;Do Something
            &braces;, style: TextStyle(size: 18)), TextSpan(text: " Style",
            style: TextStyle())], ), ), 10.height, RichText( text: TextSpan(
            text: lipsum.createWord(numWords: 2), style: TextStyle(color:
            Colors.red, size: 18), children: &lt;TextSpan&gt;[ TextSpan(text:
            lipsum.createParagraph(numSentences: 1), style: TextStyle(color:
            Colors.blueGrey)), ], ), )
          </SyntaxHighlighter>
        </Typography>
      </Container>
    </div>
  );
};

export default RichText;
