import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import chip_1 from "../../../assets/Info_Display/chip_1.png";
import chip_2 from "../../../assets/Info_Display/chip_2.png";
import chip_3 from "../../../assets/Info_Display/chip_3.png";
import chip_4 from "../../../assets/Info_Display/chip_4.png";
import chip_5 from "../../../assets/Info_Display/chip_5.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const Chips = (props: Props) => {
  return (
    <div className="container m-0">
      <div className="row">
        <div className="col-lg-6">
          <h2>
            <u>Chips</u>
          </h2>

          <img src={chip_1} alt="chip_1" />

          <h5>1. This example is a Simple Chip.</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                Row( children: [ Chip( deleteIcon: Icon(Icons.close), onDeleted:
                () &braces; setState(() &braces;&braces;); &braces;, label:
                Text("Task 1"), ).paddingLeft(16), Chip(label: Text('Task
                2')).paddingLeft(16) ], ),
              </SyntaxHighlighter>
            </Typography>
          </Container>
          <hr />
          <img src={chip_2} alt="chip_2" />

          <h5>1. This example is a Choice Chip .</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                Wrap( children: List.generate( 3, (int index) &braces; return
                ChoiceChip( label: Text('Item $index'), selectedColor:
                appColorPrimary, selected: _value == index, onSelected: (bool
                selected) &braces; setState(() &braces; _value = selected ?
                index : null; &braces;); &braces;, ).paddingLeft(16); &braces;,
                ).toList(), ),
              </SyntaxHighlighter>
            </Typography>
          </Container>
          <hr />
          <img src={chip_5} alt="chip_5" />

          <h5>1. This example is a Multi Selection Chip.</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                MultiSelectChip( programmingList, onSelectionChanged:
                (selectedList) &braces; setState(() &braces;
                selectedProgrammingList = selectedList; &braces;); &braces;,
                ).paddingLeft(16.0)
              </SyntaxHighlighter>
            </Typography>
          </Container>

          <h5>
            2. In this example a local variable selectedProgrammingList is
            declared and passed to onSelectionChanged prop
          </h5>
          <h5>
            3. Here an array of list programmingList is passed as argument
          </h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <div
                style={{
                  background: "#ffffff",
                  overflow: "auto",
                  width: "auto",
                  border: "solid gray",
                  borderWidth: ".1em .1em .1em .8em",
                  padding: ".2em .6em",
                }}
              >
                <pre style={{ margin: 0, lineHeight: "125%" }}>
                  List<span style={{ color: "#333333" }}>&lt;</span>
                  <span style={{ color: "#333399", fontWeight: "bold" }}>
                    String
                  </span>
                  <span style={{ color: "#333333" }}>&gt;</span>{" "}
                  selectedProgrammingList{" "}
                  <span style={{ color: "#333333" }}>=</span> List();{"\n"}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    onSelectionChanged:
                  </span>{" "}
                  (selectedList) {"{"}
                  {"\n"}
                  {"                    "}setState(() {"{"}
                  {"\n"}
                  {"                      "}selectedProgrammingList{" "}
                  <span style={{ color: "#333333" }}>=</span> selectedList;
                  {"\n"}
                  {"                      "}
                  {"}"});{"\n"}
                  {"                     "}
                  {"}"},{"\n"}
                  {"                "}){"\n"}
                </pre>
              </div>
            </Typography>
          </Container>

          <hr />
        </div>
        <div className="col-lg-6 pt-5">
          <img src={chip_3} alt="chip_3" />

          <h5>1. This example is a Filter Chip.</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                Wrap( children: List.generate( 3, (int index) &braces; return
                FilterChip( label: Text('Item $index'), labelStyle: TextStyle(
                color: _value == index ? white : Color), selected: _value ==
                index, selectedColor: Color, checkmarkColor: Color, onSelected:
                (bool selected) &braces; setState(() &braces; _value = index;
                &braces;); &braces;, ).paddingLeft(16); &braces;, ).toList(), ),
              </SyntaxHighlighter>
            </Typography>
          </Container>
          <hr />
          <img src={chip_4} alt="chip_4" />

          <h5>1. This example is a Action Chip.</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                Row( children: [ ActionChip( avatar: CircleAvatar(
                backgroundColor: Colors.grey.shade800, child: Text('L')), label:
                Text('Lee'), onPressed: () &braces; print( "If you stand for
                nothing, Burr, what’ll you fall for?"); &braces;,
                ).paddingLeft(16.0), ActionChip( avatar: CircleAvatar(
                backgroundColor: Colors.grey.shade800, backgroundImage:
                AssetImage(
                'images/widgets/materialWidgets/mwInformationDisplayWidgets/gridview/ic_item4.jpg')),
                label: Text('John Smith'), onPressed: () &braces; print( "If you
                stand for nothing, Burr, what’ll you fall for?"); &braces;,
                ).paddingLeft(16.0), ], ),
              </SyntaxHighlighter>
            </Typography>
          </Container>
        </div>
      </div>
    </div>
  );
};

export default Chips;
