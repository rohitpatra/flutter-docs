import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import grid from "../../../assets/Info_Display/grid.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const GridView = (props: Props) => {
  return (
    <div className="container m-0">
      <h2>
        <u>Grid View</u>
      </h2>

      <img style={{ width: "40%" }} src={grid} alt="Card" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            class GridViewState extends State&lt;GridView&gt; &braces;
            List&lt;ItemModel&gt; Listing; @override void initState() &braces;
            super.initState(); init(); &braces; init() async &braces; Listing =
            getData(); setState(() &braces;&braces;); &braces; @override void
            setState(fn) &braces; if (mounted) super.setState(fn); &braces;
            @override Widget build(BuildContext context) &braces; double
            cardWidth = context.width() / 2; double cardHeight =
            context.height() / 4; return SafeArea( child: Scaffold( appBar:
            appBar(context, 'Grid View'), body: GridView.builder(
            scrollDirection: Axis.vertical, itemCount: mListing.length, padding:
            EdgeInsets.all(16), shrinkWrap: true, gridDelegate:
            SliverGridDelegateWithFixedCrossAxisCount( crossAxisCount: 2,
            crossAxisSpacing: 16, mainAxisSpacing: 16, childAspectRatio:
            cardWidth / cardHeight), itemBuilder: (context, index) =&gt;
            Product(mListing[index], index), ), ), ); &braces; &braces; class
            Product extends StatelessWidget &braces; ItemModel model;
            Product(ItemModel model, int pos) &braces; this.model = model;
            &braces; @override Widget build(BuildContext context) &braces;
            return Container( child: Column( crossAxisAlignment:
            CrossAxisAlignment.start, mainAxisSize: MainAxisSize.min, children:
            [ ClipRRect( borderRadius: new BorderRadius.circular(12.0), child:
            Image.asset( model.img, fit: BoxFit.cover, height: context.height()
            / 6, width: MediaQuery.of(context).size.width, ), ),
            SizedBox(height: 4), Padding( padding: const EdgeInsets.only(left:
            4, right: 4), child: Row( crossAxisAlignment:
            CrossAxisAlignment.start, mainAxisAlignment:
            MainAxisAlignment.spaceBetween, children: [ Text(model.name, style:
            primaryTextStyle(color: appStore.textPrimaryColor)), ], ), ) ], ),
            ); &braces; &braces; class ItemModel &braces; var name = ""; var img
            = ""; &braces; List&lt;ItemModel&gt; getData() &braces;
            List&lt;ItemModel&gt; popularArrayList = List&t;ItemModel&gt;();
            ItemModel item1 = ItemModel(); item1.img =
            'images/widgets/materialWidgets/mwInformationDisplayWidgets/gridview/ic_item3.jpg';
            item1.name = "Black Jacket"; ItemModel item2 = ItemModel();
            item2.img =
            'images/widgets/materialWidgets/mwInformationDisplayWidgets/gridview/ic_item1.jpg';
            item2.name = "Denim Jacket"; ItemModel item3 = ItemModel();
            item3.img =
            'images/widgets/materialWidgets/mwInformationDisplayWidgets/gridview/ic_item5.jpg';
            item3.name = "Blazer"; ItemModel item4 = ItemModel(); item4.img =
            'images/widgets/materialWidgets/mwInformationDisplayWidgets/gridview/ic_item2.jpg';
            item4.name = "T-shirt"; ItemModel item5 = ItemModel(); item5.img =
            'images/widgets/materialWidgets/mwInformationDisplayWidgets/gridview/ic_item4.jpg';
            item5.name = "Sunglasses"; ItemModel item6 = ItemModel(); item6.img
            =
            'images/widgets/materialWidgets/mwInformationDisplayWidgets/gridview/ic_item7.jpg';
            item6.name = "Shirt"; popularArrayList.add(item1);
            popularArrayList.add(item2); popularArrayList.add(item3);
            popularArrayList.add(item4); popularArrayList.add(item5);
            popularArrayList.add(item6); return popularArrayList; &braces;
          </SyntaxHighlighter>
        </Typography>
      </Container>
    </div>
  );
};

export default GridView;
