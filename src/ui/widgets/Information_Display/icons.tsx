import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import SI_1 from "../../../assets/Info_Display/SI_1.png";
import SI_2 from "../../../assets/Info_Display/SI_2.png";
import SI_3 from "../../../assets/Info_Display/SI_3.png";
import SI_4 from "../../../assets/Info_Display/SI_4.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const Icons = (props: Props) => {
  return (
    <div className="container m-0">
      <h2>
        <u>Simple Icons</u>
      </h2>

      <img src={SI_1} alt="SI_1" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            Row( children: [ Padding( padding: EdgeInsets.all(16), child: Icon(
            Icons.email, size: 26, color: Color, ), ), Padding( padding:
            EdgeInsets.all(16), child: Icon( Icons.book, size: 26, color: Color,
            ), ) ], ),
          </SyntaxHighlighter>
        </Typography>
      </Container>
      <hr />

      <img src={SI_2} alt="SI_2" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            List isSelected = [false, false, false]; bool isPress = false; bool
            isBookMarkPress = true; ToggleButtons( selectedBorderColor: Color,
            selectedColor: Color, fillColor: Color, borderColor: Color,
            borderRadius: BorderRadius.all( Radius.circular(8), ), children: [
            Icon(Icons.format_bold, color: Color), Icon(Icons.format_italic,
            color: Color), Icon(Icons.format_underlined, color: Color), ],
            onPressed: (int index) &braces; setState(() &braces; for (int
            buttonIndex = 0; buttonIndex &lt; isSelected.length; buttonIndex++)
            &braces; if (buttonIndex == index) &braces; isSelected[buttonIndex]
            = true; &braces; else &braces; isSelected[buttonIndex] = false;
            &braces; &braces; &braces;); &braces;, isSelected: isSelected, ),
            Row( children: [ Padding( padding: EdgeInsets.all(16), child:
            IconButton( icon: Icon( isPress ? Icons.favorite_border :
            Icons.favorite, size: 40, color: isPress ? Color : DarkRed, ),
            onPressed: () &braces; setState(() &braces; isPress = !isPress;
            &braces;); &braces;, )), Padding( padding: EdgeInsets.all(16),
            child: IconButton( icon: Icon( isBookMarkPress ?
            Icons.bookmark_border : Icons.bookmark, size: 40, color:
            isBookMarkPress ? Color : Color, ), onPressed: () &braces;
            setState(() &braces; isBookMarkPress = !isBookMarkPress; &braces;);
            &braces;, )), ], ),
          </SyntaxHighlighter>
        </Typography>
      </Container>

      <hr />

      <img src={SI_3} alt="SI_3" />
      <img src={SI_4} alt="SI_4" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            AnimationController _animationController; bool isPlaying1 = false;
            bool isPlaying2 = false; bool isPlaying3 = false; Row( children: [
            IconButton( iconSize: 50, icon: AnimatedIcon( icon:
            AnimatedIcons.arrow_menu, progress: _animationController, color:
            Color, ), onPressed: () &braces; setState(() &braces; isPlaying1 =
            !isPlaying1; isPlaying1 ? _animationController.forward() :
            _animationController.reverse(); &braces;); &braces;,
            ).paddingLeft(16), IconButton( iconSize: 50, icon: AnimatedIcon(
            icon: AnimatedIcons.play_pause, progress: _animationController,
            color: Color, ), onPressed: () &braces; setState(() &braces;
            isPlaying3 = !isPlaying3; isPlaying3 ?
            _animationController.forward() : _animationController.reverse();
            &braces;); &braces;, ), ], ),
          </SyntaxHighlighter>
        </Typography>
      </Container>
    </div>
  );
};

export default Icons;
