import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import DT_1 from "../../../assets/Info_Display/DT_1.png";
import DT_2 from "../../../assets/Info_Display/DT_2.png";
import DT_3 from "../../../assets/Info_Display/DT_3.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const DataTable = (props: Props) => {
  return (
    <div className="container m-0">
      <h2>
        <u>Data Table</u>
      </h2>

      <img src={DT_1} alt="DT_1" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            SingleChildScrollView( scrollDirection: Axis.horizontal, child:
            DataTable( columns: &lt;DataColumn&gt;[ DataColumn(label:
            Text('Rank'), tooltip: 'Rank'), DataColumn(label: Text('Name')),
            DataColumn(label: Text('Email')), DataColumn(label:
            Text('Designation')), DataColumn(label: Text('BirthDate')),
            DataColumn(label: Text('Location')), ], rows: userdetails .map(
            (data) =&gt; DataRow( cells: [ DataCell(Text(data.rank, style:
            TextStyle())), DataCell(Text(data.name, style: TextStyle())),
            DataCell(Text(data.email, style: TextStyle())),
            DataCell(Text(data.designation, style: TextStyle())),
            DataCell(Text(data.birthday, style: TextStyle())),
            DataCell(Text(data.location, style: TextStyle())), ], ), )
            .toList(), ).visible(userdetails.isNotEmpty), ),
          </SyntaxHighlighter>
        </Typography>
      </Container>

      <h5>
        1. In this example an array of List userdetails is declared and passed
        to row prop.
      </h5>

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <div
            style={{
              background: "#ffffff",
              overflow: "auto",
              width: "auto",
              border: "solid gray",
              borderWidth: ".1em .1em .1em .8em",
              padding: ".2em .6em",
            }}
          >
            <pre style={{ margin: 0, lineHeight: "125%" }}>
              {"  "}List<span style={{ color: "#333333" }}>&lt;</span>user
              <span style={{ color: "#333333" }}>&gt;</span> userdetails{" "}
              <span style={{ color: "#333333" }}>=</span> [{"\n"}
              {"    "}user({"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                rank:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>'1'</span>,{"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                name:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>'john'</span>,{"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                email:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>
                'john@gmail.com'
              </span>
              ,{"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                designation:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>'Designer'</span>,
              {"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                birthday:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>'12-03-1997'</span>,
              {"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                location:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>'Mumbai'</span>,
              {"\n"}
              {"    "}),{"\n"}
              {"    "}user({"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                rank:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>'2'</span>,{"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                name:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>'Lee'</span>,{"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                email:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>
                'Lee@gmail.com'
              </span>
              ,{"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                designation:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>'Designer'</span>,
              {"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                birthday:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>'12-07-1997'</span>,
              {"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                location:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>'Mumbai'</span>,
              {"\n"}
              {"    "}),{"\n"}
              {"    "}user({"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                rank:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>'3'</span>,{"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                name:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>'Miller'</span>,
              {"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                email:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>
                'Miller@gmail.com'
              </span>
              ,{"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                designation:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>'Staff'</span>,{"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                birthday:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>'12-03-1997'</span>,
              {"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                location:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>'Mumbai'</span>,
              {"\n"}
              {"    "}),{"\n"}
              {"    "}user({"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                rank:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>'4'</span>,{"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                name:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>'killer'</span>,
              {"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                email:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>
                'killer@gmail.com'
              </span>
              ,{"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                designation:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>'Manager'</span>,
              {"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                birthday:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>'13-04-1997'</span>,
              {"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                location:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>'Mumbai'</span>,
              {"\n"}
              {"    "}),{"\n"}
              {"    "}user({"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                rank:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>'5'</span>,{"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                name:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>'Smiler'</span>,
              {"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                email:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>
                'Smiler@gmail.com'
              </span>
              ,{"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                designation:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>'Developer'</span>,
              {"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                birthday:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>'12-06-1997'</span>,
              {"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                location:
              </span>{" "}
              <span style={{ backgroundColor: "#fff0f0" }}>'Mumbai'</span>,
              {"\n"}
              {"    "}),{"\n"}
              {"  "}];{"\n"}
            </pre>
          </div>
        </Typography>
      </Container>
      <hr />

      <img src={DT_2} alt="DT_2" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            SingleChildScrollView( scrollDirection: Axis.horizontal, child:
            CustomTheme( child: DataTable( columns: &lt;DataColumn&gt;[
            DataColumn(label: Text('Rank'), tooltip: 'Rank'), DataColumn(label:
            Text('Name')), DataColumn(label: Text('Email')), DataColumn(label:
            Text('Designation')), DataColumn(label: Text('BirthDate')),
            DataColumn(label: Text('Location')), ], rows: userdetails .map(
            (data) =&gt; DataRow( selected: selectedList.contains(data),
            onSelectChanged: (b) &braces; onSelectedRow(b, data); &braces;,
            cells: [ DataCell(Text(data.rank, style: secondaryTextStyle())),
            DataCell(Text(data.name, style: secondaryTextStyle())),
            DataCell(Text(data.email, style: secondaryTextStyle())),
            DataCell(Text(data.designation, style: secondaryTextStyle())),
            DataCell(Text(data.birthday, style: secondaryTextStyle())),
            DataCell(Text(data.location, style: secondaryTextStyle())), ], ), )
            .toList(), ).visible(userdetails.isNotEmpty), ), ),
          </SyntaxHighlighter>
        </Typography>
      </Container>

      <h5>
        1. In this example an array of List userdetails is declared and passed
        to row prop.
      </h5>
      <h5>
        2. A custom method onSelectedRow is used to select data inside table.
      </h5>

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <div
            style={{
              background: "#ffffff",
              overflow: "auto",
              width: "auto",
              border: "solid gray",
              borderWidth: ".1em .1em .1em .8em",
              padding: ".2em .6em",
            }}
          >
            <pre style={{ margin: 0, lineHeight: "125%" }}>
              {"  "}onSelectedRow(
              <span style={{ color: "#333399", fontWeight: "bold" }}>
                bool
              </span>{" "}
              selected, user data) <span style={{ color: "#333333" }}>as</span>
              ync {"{"}
              {"\n"}
              {"    "}setState(() {"{"}
              {"\n"}
              {"      "}
              <span style={{ color: "#008800", fontWeight: "bold" }}>
                if
              </span>{" "}
              (selected) {"{"}
              {"\n"}
              {"        "}selectedList.add(data);{"\n"}
              {"      "}
              {"}"}{" "}
              <span style={{ color: "#008800", fontWeight: "bold" }}>else</span>{" "}
              {"{"}
              {"\n"}
              {"        "}selectedList.remove(data);{"\n"}
              {"      "}
              {"}"}
              {"\n"}
              {"    "}
              {"}"});{"\n"}
              {"  "}
              {"}"}
              {"\n"}
            </pre>
          </div>
        </Typography>
      </Container>
      <hr />

      <img src={DT_3} alt="DT_3" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            SingleChildScrollView( scrollDirection: Axis.horizontal, child:
            DataTable( sortAscending: sort, sortColumnIndex: 1, columns: [
            DataColumn(label: Text('Rank'), tooltip: 'Rank'), DataColumn( label:
            Text('Name'), onSort: (columnIndex, ascending) &braces; setState(()
            &braces; sort = !sort; &braces;); onSortColumn(columnIndex,
            ascending); &braces;, ), DataColumn(label: Text('Email')),
            DataColumn(label: Text('Designation')), DataColumn(label:
            Text('Birthdate')), DataColumn(label: Text('Location')), ], rows:
            userdetails .map( (data) =&gt; DataRow( cells: [
            DataCell(Text(data.rank, style: TextStyle())),
            DataCell(Text(data.name, style: TextStyle())),
            DataCell(Text(data.email, style: TextStyle())),
            DataCell(Text(data.designation, style: TextStyle())),
            DataCell(Text(data.birthday, style: TextStyle())),
            DataCell(Text(data.location, style: TextStyle())), ], ), )
            .toList(), ).visible(userdetails1.isNotEmpty), ),
          </SyntaxHighlighter>
        </Typography>
      </Container>

      <h5>
        1. In this example an array of List userdetails is declared and passed
        to row prop.
      </h5>
      <h5>
        2. A custom method onSortColumn is used to sort data inside table.
      </h5>
      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <div
            style={{
              background: "#ffffff",
              overflow: "auto",
              width: "auto",
              border: "solid gray",
              borderWidth: ".1em .1em .1em .8em",
              padding: ".2em .6em",
            }}
          >
            <pre style={{ margin: 0, lineHeight: "125%" }}>
              {"  "}onSortColumn(
              <span style={{ color: "#333399", fontWeight: "bold" }}>
                int
              </span>{" "}
              columnIndex,{" "}
              <span style={{ color: "#333399", fontWeight: "bold" }}>bool</span>{" "}
              <span style={{ color: "#333333" }}>as</span>cending) {"{"}
              {"\n"}
              {"    "}
              <span style={{ color: "#008800", fontWeight: "bold" }}>
                if
              </span>{" "}
              (columnIndex <span style={{ color: "#333333" }}>==</span>{" "}
              <span style={{ color: "#6600EE", fontWeight: "bold" }}>1</span>){" "}
              {"{"}
              {"\n"}
              {"      "}
              <span style={{ color: "#008800", fontWeight: "bold" }}>if</span> (
              <span style={{ color: "#333333" }}>as</span>cending) {"{"}
              {"\n"}
              {"        "}userdetails1.sort((a, b){" "}
              <span style={{ color: "#333333" }}>=&gt;</span>{" "}
              a.name.compareTo(b.name));
              {"\n"}
              {"      "}
              {"}"}{" "}
              <span style={{ color: "#008800", fontWeight: "bold" }}>else</span>{" "}
              {"{"}
              {"\n"}
              {"        "}userdetails1.sort((a, b){" "}
              <span style={{ color: "#333333" }}>=&gt;</span>{" "}
              b.name.compareTo(a.name));
              {"\n"}
              {"      "}
              {"}"}
              {"\n"}
              {"    "}
              {"}"}
              {"\n"}
              {"  "}
              {"}"}
              {"\n"}
            </pre>
          </div>
        </Typography>
      </Container>
    </div>
  );
};

export default DataTable;
