import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import PGB_1 from "../../../assets/Info_Display/PGB_1.png";
import PGB_2 from "../../../assets/Info_Display/PGB_2.png";
import PGB_3 from "../../../assets/Info_Display/PGB_3.png";
import PGB_4 from "../../../assets/Info_Display/PGB_4.png";
import PGB_5 from "../../../assets/Info_Display/PGB_5.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const ProgressBar = (props: Props) => {
  return (
    <div className="container m-0">
      <div className="row">
        <div className="col-lg-6">
          <h2>
            <u>Progress Bars</u>
          </h2>

          <img src={PGB_1} alt="PGB_1" />

          <h5>1. This example is a Circular Progress.</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                Row( children: [ Padding( padding: EdgeInsets.all(16.0), child:
                CircularProgressIndicator( backgroundColor: Color(0xffD6D6D6),
                strokeWidth: 4, valueColor:
                AlwaysStoppedAnimation(Colors.blueGrey), ), ), Container(
                alignment: Alignment.center, child: Card( semanticContainer:
                true, clipBehavior: Clip.antiAliasWithSaveLayer, elevation: 4,
                margin: EdgeInsets.all(4), shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(50.0), ), child: Container(
                width: 45, height: 45, padding: const EdgeInsets.all(8.0),
                child: CircularProgressIndicator( strokeWidth: 3, ), )), ), ],
                ),
              </SyntaxHighlighter>
            </Typography>
          </Container>
          <hr />
          <img src={PGB_2} alt="PGB_2" />

          <h5>1. This example is a Linear Progress Bar .</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                Container( margin: EdgeInsets.only(left: 16, right: 16, bottom:
                16, top: 16), child: LinearProgressIndicator( value:
                animation.value, backgroundColor: Color(0xffD6D6D6), valueColor:
                AlwaysStoppedAnimation(Colors.blueGrey), ), ),
              </SyntaxHighlighter>
            </Typography>
          </Container>

          <h5>
            2. In this example a local variable animation of type
            Animation&lt;double&gt; is initialized and passed to value prop.
          </h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <div
                style={{
                  background: "#ffffff",
                  overflow: "auto",
                  width: "auto",
                  border: "solid gray",
                  borderWidth: ".1em .1em .1em .8em",
                  padding: ".2em .6em",
                }}
              >
                <pre style={{ margin: 0, lineHeight: "125%" }}>
                  Animation<span style={{ color: "#333333" }}>&lt;</span>
                  <span style={{ color: "#333399", fontWeight: "bold" }}>
                    double
                  </span>
                  <span style={{ color: "#333333" }}>&gt;</span> animation;
                  {"\n"}
                  {"  "}init() <span style={{ color: "#333333" }}>as</span>ync{" "}
                  {"{"}
                  {"\n"}
                  {"    "}controller <span style={{ color: "#333333" }}>=</span>{" "}
                  AnimationController(
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    duration:
                  </span>{" "}
                  <span style={{ color: "#008800", fontWeight: "bold" }}>
                    const
                  </span>{" "}
                  Duration(
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    milliseconds:
                  </span>{" "}
                  <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                    2000
                  </span>
                  ),{" "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    vsync:
                  </span>{" "}
                  <span style={{ color: "#008800", fontWeight: "bold" }}>
                    this
                  </span>
                  );{"\n"}
                  {"    "}animation <span style={{ color: "#333333" }}>=</span>{" "}
                  Tween(
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    begin:
                  </span>{" "}
                  <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                    0.0
                  </span>
                  ,{" "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    end:
                  </span>{" "}
                  <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                    1.0
                  </span>
                  ).animate(controller){"\n"}
                  {"      "}..addListener(() {"{"}
                  {"\n"}
                  {"        "}setState(() {"{"}
                  {"}"});{"\n"}
                  {"      "}
                  {"}"});{"\n"}
                  {"    "}print(animation.value);{"\n"}
                  {"    "}controller.repeat();{"\n"}
                  {"  "}
                  {"}"}
                  {"\n"}LinearProgressIndicator({"\n"}
                  {"       "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    value:
                  </span>{" "}
                  animation.value,{"\n"});{"\n"}
                </pre>
              </div>
            </Typography>
          </Container>

          <hr />
          <img src={PGB_3} alt="PGB_3" />

          <h5>1. This example is a Linear Progress with thick corners.</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                Container( margin: EdgeInsets.only(left: 16, right: 16, top:
                16), width: MediaQuery.of(context).size.width, height: 10,
                child: ClipRRect( borderRadius:
                BorderRadius.all(Radius.circular(10)), child:
                LinearProgressIndicator( value: animation.value, valueColor: new
                AlwaysStoppedAnimation&lt;Color&gt;(Colors.blueGrey),
                backgroundColor: Color(0xffD6D6D6), ), ), ),
              </SyntaxHighlighter>
            </Typography>
          </Container>

          <h5>
            2. In this example a local variable animation of type
            Animation&lt;double&gt; is initialized and passed to value prop.
          </h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <div
                style={{
                  background: "#ffffff",
                  overflow: "auto",
                  width: "auto",
                  border: "solid gray",
                  borderWidth: ".1em .1em .1em .8em",
                  padding: ".2em .6em",
                }}
              >
                <pre style={{ margin: 0, lineHeight: "125%" }}>
                  Animation<span style={{ color: "#333333" }}>&lt;</span>
                  <span style={{ color: "#333399", fontWeight: "bold" }}>
                    double
                  </span>
                  <span style={{ color: "#333333" }}>&gt;</span> animation;
                  {"\n"}
                  {"  "}init() <span style={{ color: "#333333" }}>as</span>ync{" "}
                  {"{"}
                  {"\n"}
                  {"    "}controller <span style={{ color: "#333333" }}>=</span>{" "}
                  AnimationController(
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    duration:
                  </span>{" "}
                  <span style={{ color: "#008800", fontWeight: "bold" }}>
                    const
                  </span>{" "}
                  Duration(
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    milliseconds:
                  </span>{" "}
                  <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                    2000
                  </span>
                  ),{" "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    vsync:
                  </span>{" "}
                  <span style={{ color: "#008800", fontWeight: "bold" }}>
                    this
                  </span>
                  );{"\n"}
                  {"    "}animation <span style={{ color: "#333333" }}>=</span>{" "}
                  Tween(
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    begin:
                  </span>{" "}
                  <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                    0.0
                  </span>
                  ,{" "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    end:
                  </span>{" "}
                  <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                    1.0
                  </span>
                  ).animate(controller){"\n"}
                  {"      "}..addListener(() {"{"}
                  {"\n"}
                  {"        "}setState(() {"{"}
                  {"}"});{"\n"}
                  {"      "}
                  {"}"});{"\n"}
                  {"    "}print(animation.value);{"\n"}
                  {"    "}controller.repeat();{"\n"}
                  {"  "}
                  {"}"}
                  {"\n"}LinearProgressIndicator({"\n"}
                  {"       "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    value:
                  </span>{" "}
                  animation.value,{"\n"});{"\n"}
                </pre>
              </div>
            </Typography>
          </Container>
        </div>
        <div className="col-lg-6 pt-5">
          <img src={PGB_4} alt="PGB_4" />

          <h5>1. This example is a Circular Progress inside Dialog.</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                GestureDetector( onTap: () &braces; _onLoading(); &braces;,
                child: Container( margin: EdgeInsets.all(16), decoration:
                boxDecoration(bgColor: Colors.blueGrey, radius: 10), padding:
                EdgeInsets.fromLTRB(16, 8, 16, 8), child: Text("Circular
                Progress in Dialog", style: TextStyle(color: Colors.white)), ),
                ),
              </SyntaxHighlighter>
            </Typography>
          </Container>

          <h5>
            2. In this example a custom method _onLoading() is used and passed
            to onTap prop.
          </h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <div
                style={{
                  background: "#ffffff",
                  overflow: "auto",
                  width: "auto",
                  border: "solid gray",
                  borderWidth: ".1em .1em .1em .8em",
                  padding: ".2em .6em",
                }}
              >
                <pre style={{ margin: 0, lineHeight: "125%" }}>
                  {"  "}
                  <span
                    style={{ color: "#FF0000", backgroundColor: "#FFAAAA" }}
                  >
                    @
                  </span>
                  override{"\n"}
                  {"  "}Widget build(BuildContext context) {"{"}
                  {"\n"}
                  {"    "}
                  <span style={{ color: "#333399", fontWeight: "bold" }}>
                    void
                  </span>{" "}
                  _onLoading() {"{"}
                  {"\n"}
                  {"      "}showDialog({"\n"}
                  {"        "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    context:
                  </span>{" "}
                  context,{"\n"}
                  {"        "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    builder:
                  </span>{" "}
                  (BuildContext context) {"{"}
                  {"\n"}
                  {"          "}
                  <span style={{ color: "#008800", fontWeight: "bold" }}>
                    return
                  </span>{" "}
                  AlertDialog({"\n"}
                  {"            "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    backgroundColor:
                  </span>{" "}
                  appStore.scaffoldBackground,{"\n"}
                  {"            "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    shape:
                  </span>{" "}
                  RoundedRectangleBorder(
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    borderRadius:
                  </span>{" "}
                  BorderRadius.all(Radius.circular(
                  <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                    10.0
                  </span>
                  ))),{"\n"}
                  {"            "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    contentPadding:
                  </span>{" "}
                  EdgeInsets.all(
                  <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                    0.0
                  </span>
                  ),{"\n"}
                  {"            "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    insetPadding:
                  </span>{" "}
                  EdgeInsets.symmetric(
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    horizontal:
                  </span>{" "}
                  <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                    100
                  </span>
                  ),{"\n"}
                  {"            "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    content:
                  </span>{" "}
                  Padding({"\n"}
                  {"              "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    padding:
                  </span>{" "}
                  EdgeInsets.only(
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    top:
                  </span>{" "}
                  <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                    20
                  </span>
                  ,{" "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    bottom:
                  </span>{" "}
                  <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                    20
                  </span>
                  ),{"\n"}
                  {"              "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    child:
                  </span>{" "}
                  Column(
                  {"\n"}
                  {"                "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    mainAxisSize:
                  </span>{" "}
                  MainAxisSize.min,{"\n"}
                  {"                "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    children:
                  </span>{" "}
                  [{"\n"}
                  {"                  "}CircularProgressIndicator(),{"\n"}
                  {"                  "}SizedBox(
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    height:
                  </span>{" "}
                  <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                    16
                  </span>
                  ),{"\n"}
                  {"                  "}Text(
                  <span style={{ backgroundColor: "#fff0f0" }}>
                    "Please wait...."
                  </span>
                  ,{" "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    style:
                  </span>{" "}
                  primaryTextStyle(
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    color:
                  </span>{" "}
                  appStore.textPrimaryColor)),{"\n"}
                  {"                "}],{"\n"}
                  {"              "}),{"\n"}
                  {"            "}),{"\n"}
                  {"          "});{"\n"}
                  {"        "}
                  {"}"},{"\n"}
                  {"      "});{"\n"}
                  {"    "}
                  {"}"}
                  {"\n"}
                </pre>
              </div>
            </Typography>
          </Container>

          <hr />
          <img src={PGB_5} alt="PGB_5" />

          <h5>
            1. This example is a Circular Progress inside Dialog aligned
            Horizontally .
          </h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                GestureDetector( onTap: () &braces; _onHorizontalLoading();
                &braces;, child: Container( margin: EdgeInsets.all(16),
                decoration: boxDecoration(bgColor: Colors.blueGrey, radius: 10),
                padding: EdgeInsets.fromLTRB(16, 8, 16, 8), child:
                Text("Circular Progress in Dialog 2", style: TextStyle(color:
                Colors.white)), ), ),
              </SyntaxHighlighter>
            </Typography>
          </Container>

          <h5>
            2. In this example a custom method _onHorizontalLoading() is used
            and passed to onTap prop.
          </h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <div
                style={{
                  background: "#ffffff",
                  overflow: "auto",
                  width: "auto",
                  border: "solid gray",
                  borderWidth: ".1em .1em .1em .8em",
                  padding: ".2em .6em",
                }}
              >
                <pre style={{ margin: 0, lineHeight: "125%" }}>
                  {"  "}
                  <span
                    style={{ color: "#FF0000", backgroundColor: "#FFAAAA" }}
                  >
                    @
                  </span>
                  override{"\n"}
                  {"  "}Widget build(BuildContext context) {"{"}
                  {"\n"}
                  {"       "}
                  <span style={{ color: "#333399", fontWeight: "bold" }}>
                    void
                  </span>{" "}
                  _onHorizontalLoading() {"{"}
                  {"\n"}
                  {"      "}showDialog({"\n"}
                  {"        "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    context:
                  </span>{" "}
                  context,{"\n"}
                  {"        "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    builder:
                  </span>{" "}
                  (BuildContext context) {"{"}
                  {"\n"}
                  {"          "}
                  <span style={{ color: "#008800", fontWeight: "bold" }}>
                    return
                  </span>{" "}
                  AlertDialog({"\n"}
                  {"            "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    backgroundColor:
                  </span>{" "}
                  appStore.scaffoldBackground,{"\n"}
                  {"            "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    shape:
                  </span>{" "}
                  RoundedRectangleBorder(
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    borderRadius:
                  </span>{" "}
                  BorderRadius.all(Radius.circular(
                  <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                    10.0
                  </span>
                  ))),{"\n"}
                  {"            "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    contentPadding:
                  </span>{" "}
                  EdgeInsets.all(
                  <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                    0.0
                  </span>
                  ),{"\n"}
                  {"            "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    content:
                  </span>{" "}
                  Padding({"\n"}
                  {"              "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    padding:
                  </span>{" "}
                  EdgeInsets.only(
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    top:
                  </span>{" "}
                  <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                    20
                  </span>
                  ,{" "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    bottom:
                  </span>{" "}
                  <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                    20
                  </span>
                  ),{"\n"}
                  {"              "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    child:
                  </span>{" "}
                  Row(
                  {"\n"}
                  {"                "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    children:
                  </span>{" "}
                  [{"\n"}
                  {"                  "}
                  <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                    16.
                  </span>
                  width,
                  {"\n"}
                  {"                  "}CircularProgressIndicator({"\n"}
                  {"                    "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    backgroundColor:
                  </span>{" "}
                  Color(
                  <span style={{ color: "#005588", fontWeight: "bold" }}>
                    0xffD6D6D6
                  </span>
                  ),
                  {"\n"}
                  {"                    "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    strokeWidth:
                  </span>{" "}
                  <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                    4
                  </span>
                  ,{"\n"}
                  {"                    "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    valueColor:
                  </span>{" "}
                  AlwaysStoppedAnimation
                  <span style={{ color: "#333333" }}>&lt;</span>Color
                  <span style={{ color: "#333333" }}>&gt;</span>
                  (Colors.blueGrey),{"\n"}
                  {"                  "}),{"\n"}
                  {"                  "}
                  <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                    16.
                  </span>
                  width,
                  {"\n"}
                  {"                  "}Text({"\n"}
                  {"                    "}
                  <span style={{ backgroundColor: "#fff0f0" }}>
                    "Please Wait...."
                  </span>
                  ,{"\n"}
                  {"                    "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    style:
                  </span>{" "}
                  primaryTextStyle(
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    color:
                  </span>{" "}
                  appStore.textPrimaryColor),{"\n"}
                  {"                  "}),{"\n"}
                  {"                "}],{"\n"}
                  {"              "}),{"\n"}
                  {"            "}),{"\n"}
                  {"          "});{"\n"}
                  {"        "}
                  {"}"},{"\n"}
                  {"      "});{"\n"}
                  {"    "}
                  {"}"}
                  {"\n"}
                </pre>
              </div>
            </Typography>
          </Container>
        </div>
      </div>
    </div>
  );
};

export default ProgressBar;
