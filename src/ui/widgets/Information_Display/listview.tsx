import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import LV_1 from "../../../assets/Info_Display/LV_1.png";
import LV_2 from "../../../assets/Info_Display/LV_2.png";
import LV_3 from "../../../assets/Info_Display/LV_3.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const ListView = (props: Props) => {
  return (
    <div className="container m-0">
      <h2>
        <u>List View</u>
      </h2>

      <img src={LV_1} alt="LV_1" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            @override Widget build(BuildContext context) &braces; return
            SafeArea( child: Scaffold( appBar: appBar(context, 'Simple List
            View'), body: ListView.builder( scrollDirection: Axis.vertical,
            itemCount: dataList.length, shrinkWrap: true, padding:
            EdgeInsets.only(bottom: 16), itemBuilder: (context, index) &braces;
            return DataList(dataList[index], index); &races;), ), ); &races;
            &races; class DataList extends StatelessWidget &braces; DataModel
            model; DataList(DataModel model, int pos) &braces; this.model =
            model; &races; @override Widget build(BuildContext context) &braces;
            return Container( margin: EdgeInsets.only(left: 16, right: 16, top:
            16), decoration: boxDecoration( radius: 10, showShadow: true,
            bgColor: appStore.scaffoldBackground), padding:
            EdgeInsets.fromLTRB(20, 16, 20, 16), child: Row( children: [
            Image.asset( model.images, height: 40, width: 40, ), 16.width, Text(
            model.name, style: boldTextStyle(color: appStore.textPrimaryColor),
            maxLines: 1, ) ], ), ); &races; &races; class DataModel &braces; var
            images; var name; DataModel(&braces; this.images, this.name,
            &braces;); &braces; List dataList = [ DataModel( images:
            'images/widgets/materialWidgets/mwInformationDisplayWidgets/listView/airplane.png',
            name: 'Travels', ), DataModel( images:
            'images/widgets/materialWidgets/mwInformationDisplayWidgets/listView/book.png',
            name: 'Education', ), DataModel( images:
            'images/widgets/materialWidgets/mwInformationDisplayWidgets/listView/computer.png',
            name: 'Management', ), DataModel( images:
            'images/widgets/materialWidgets/mwInformationDisplayWidgets/listView/finance.png',
            name: 'Finance', ), DataModel( images:
            'images/widgets/materialWidgets/mwInformationDisplayWidgets/listView/food.png',
            name: 'Food', ), DataModel( images:
            'images/widgets/materialWidgets/mwInformationDisplayWidgets/listView/graphic.png',
            name: 'Business', ), ];
          </SyntaxHighlighter>
        </Typography>
      </Container>
      <hr />

      <img src={LV_2} alt="LV_2" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            class ListViewState extends State&lt;MWListView&gt; &braces; List
            Listing; @override void initState() &braces; super.initState();
            init(); &braces; init() async &braces; Listing = getData();
            setState(() &braces;&braces;); &braces; @override void setState(fn)
            &braces; if (mounted) super.setState(fn); &braces; @override Widget
            build(BuildContext context) &braces; return SafeArea( child:
            Scaffold( appBar: appBar(context, 'Horizontal List View'), body:
            Column( crossAxisAlignment: CrossAxisAlignment.start, mainAxisSize:
            MainAxisSize.min, children: [ 16.height, Text("Product", style:
            boldTextStyle(color: appStore.textPrimaryColor, size:
            18)).paddingLeft(16), SizedBox( height: 220, child:
            ListView.builder( scrollDirection: Axis.horizontal, itemCount:
            mListing.length, shrinkWrap: true, padding: EdgeInsets.only(right:
            16), itemBuilder: (context, index) &braces; return
            Product(mListing[index], index); &braces;, ), ), ], ), ), );
            &braces; &braces; class Product extends StatelessWidget &braces;
            ItemModel model; Product(ItemModel model, int pos) &braces;
            this.model = model; &braces; @override Widget build(BuildContext
            context) &braces; return Container( width:
            MediaQuery.of(context).size.width * 0.4, margin:
            EdgeInsets.only(left: 16, top: 16), child: Column(
            crossAxisAlignment: CrossAxisAlignment.start, children: [ ClipRRect(
            borderRadius: new BorderRadius.circular(12.0), child: Image.asset(
            model.img, fit: BoxFit.cover, height: 170, width:
            MediaQuery.of(context).size.width, ), ), SizedBox(height: 4),
            Padding( padding: const EdgeInsets.only(left: 4, right: 4), child:
            Row( crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text(model.name, style: primaryTextStyle(color:
            appStore.textPrimaryColor)), ], ), ) ], ), ); &braces; &braces;
            class ItemModel &braces; var name = ""; var img = ""; &braces; List
            getData() &braces; List popularArrayList = List(); ItemModel item1 =
            ItemModel(); item1.img =
            'images/widgets/materialWidgets/mwInformationDisplayWidgets/gridview/ic_item3.jpg';
            item1.name = "Black Jacket"; ItemModel item2 = ItemModel();
            item2.img =
            'images/widgets/materialWidgets/mwInformationDisplayWidgets/gridview/ic_item1.jpg';
            item2.name = "Denim Jacket"; ItemModel item3 = ItemModel();
            item3.img =
            'images/widgets/materialWidgets/mwInformationDisplayWidgets/gridview/ic_item5.jpg';
            item3.name = "Blazer"; ItemModel item4 = ItemModel(); item4.img =
            'images/widgets/materialWidgets/mwInformationDisplayWidgets/gridview/ic_item2.jpg';
            item4.name = "T-shirt"; ItemModel item5 = ItemModel(); item5.img =
            'images/widgets/materialWidgets/mwInformationDisplayWidgets/gridview/ic_item4.jpg';
            item5.name = "Sunglasses"; ItemModel item6 = ItemModel(); item6.img
            =
            'images/widgets/materialWidgets/mwInformationDisplayWidgets/gridview/ic_item7.jpg';
            item6.name = "Sunglasses"; popularArrayList.add(item1);
            popularArrayList.add(item2); popularArrayList.add(item3);
            popularArrayList.add(item4); popularArrayList.add(item5);
            popularArrayList.add(item6); return popularArrayList; &braces;
          </SyntaxHighlighter>
        </Typography>
      </Container>

      <hr />

      <img src={LV_3} alt="LV_3" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            class MWListViewScreen5 extends StatefulWidget &braces; static const
            tag = '/MWListViewScreen5'; List&lt;String&gt; item = [
            "Management", "Food", "Cafe", "Travel", "Education", "Finance",
            "Business", "Yoga & Gym", "Health" ]; @override
            _MWListViewScreen5State createState() =&gt;
            _MWListViewScreen5State(); &braces; class _MWListViewScreen5State
            extends State&lt;MWListViewScreen5&gt; &braces; @override void
            initState() &braces; super.initState(); init(); &braces; init()
            async &braces; setState(() &braces;&braces;); &braces; @override
            void setState(fn) &braces; if (mounted) super.setState(fn); &braces;
            @override Widget build(BuildContext context) &braces; void
            reorderData(int oldindex, int newindex) &braces; setState(()
            &braces; if (newindex &gt; oldindex) &braces; newindex -= 1;
            &braces; final items = widget.item.removeAt(oldindex);
            widget.item.insert(newindex, items); &braces;); &braces; return
            SafeArea( child: Scaffold( appBar: appBar(context, 'Reorderable List
            View'), body: CustomTheme( child: ReorderableListView( children: [
            for (final items in widget.item) Container( key: ValueKey(items),
            child: Column( mainAxisSize: MainAxisSize.min, children: [ ListTile(
            title: Text( items, style: boldTextStyle(), ), leading: Icon(
            Icons.menu, color: appStore.iconColor, ), subtitle: Text(
            lipsum.createParagraph(numSentences: 1), style:
            secondaryTextStyle(), maxLines: 1, ), ), Divider() ], ), ), ],
            onReorder: reorderData, ), ), ), ); &braces; &braces;
          </SyntaxHighlighter>
        </Typography>
      </Container>
    </div>
  );
};

export default ListView;
