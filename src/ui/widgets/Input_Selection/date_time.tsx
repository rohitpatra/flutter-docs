import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import DT_1 from "../../../assets/Input_Selection/DT_1.png";
import DT_2 from "../../../assets/Input_Selection/DT_2.png";
import DT_3 from "../../../assets/Input_Selection/DT_3.png";
import DT_4 from "../../../assets/Input_Selection/DT_4.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const DateTime = (props: Props) => {
  return (
    <div className="container m-0">
      <div className="row">
        <div className="col-lg-6">
          <h2>
            <u>Date Time Picker</u>
          </h2>
          <img src={DT_1} alt="DT_1" />
          <img src={DT_2} alt="DT_2" />
        </div>
        <div className="col-lg-6 pt-5">
          <h5>1. This example is a Date Picker with Calender Icon.</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                Card( elevation: 4, child: ListTile( onTap: () &braces;
                _selectDate(context); &braces;, title: Text( 'Select your
                Booking date', style: primaryTextStyle(), ), subtitle: Text(
                "$&braces;selectedDate.toLocal()&braces;".split(' ')[0], style:
                TextStyle(), ), trailing: IconButton( icon: Icon(
                Icons.date_range, color: Color, ), onPressed: () &braces;
                _selectDate(context); &braces;, ), )),
              </SyntaxHighlighter>
            </Typography>
          </Container>
          <hr />

          <h5>
            2. In this example a local variable selectedDate is initialized to
            DateTime.now().
          </h5>
          <h5>3. A custom Method _selectDate is passed to onTap props.</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <div
                style={{
                  background: "#ffffff",
                  overflow: "auto",
                  width: "auto",
                  border: "solid gray",
                  borderWidth: ".1em .1em .1em .8em",
                  padding: ".2em .6em",
                }}
              >
                <pre style={{ margin: 0, lineHeight: "125%" }}>
                  DateTime selectedDate{" "}
                  <span style={{ color: "#333333" }}>=</span> DateTime.now();
                  {"\n"}
                  {"\n"}Future<span style={{ color: "#333333" }}>&lt;</span>
                  <span style={{ color: "#333399", fontWeight: "bold" }}>
                    void
                  </span>
                  <span style={{ color: "#333333" }}>&gt;</span>{" "}
                  _selectDate(BuildContext context){" "}
                  <span style={{ color: "#333333" }}>as</span>ync {"{"}
                  {"\n"}
                  {"    "}
                  <span style={{ color: "#008800", fontWeight: "bold" }}>
                    final
                  </span>{" "}
                  DateTime picked <span style={{ color: "#333333" }}>=</span>{" "}
                  await showDatePicker(
                  {"\n"}
                  {"        "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    helpText:
                  </span>{" "}
                  <span style={{ backgroundColor: "#fff0f0" }}>
                    'Select your Booking date'
                  </span>
                  ,{"\n"}
                  {"        "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    cancelText:
                  </span>{" "}
                  <span style={{ backgroundColor: "#fff0f0" }}>'Not Now'</span>,
                  {"\n"}
                  {"        "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    confirmText:
                  </span>{" "}
                  <span style={{ backgroundColor: "#fff0f0" }}>"Book"</span>,
                  {"\n"}
                  {"        "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    fieldLabelText:
                  </span>{" "}
                  <span style={{ backgroundColor: "#fff0f0" }}>
                    'Booking Date'
                  </span>
                  ,{"\n"}
                  {"        "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    fieldHintText:
                  </span>{" "}
                  <span style={{ backgroundColor: "#fff0f0" }}>
                    'Month/Date/Year'
                  </span>
                  ,{"\n"}
                  {"        "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    errorFormatText:
                  </span>{" "}
                  <span style={{ backgroundColor: "#fff0f0" }}>
                    'Enter valid date'
                  </span>
                  ,{"\n"}
                  {"        "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    errorInvalidText:
                  </span>{" "}
                  <span style={{ backgroundColor: "#fff0f0" }}>
                    'Enter date in valid range'
                  </span>
                  ,{"\n"}
                  {"        "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    context:
                  </span>{" "}
                  context,{"\n"}
                  {"        "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    builder:
                  </span>{" "}
                  (BuildContext context, Widget child) {"{"}
                  {"\n"}
                  {"          "}
                  <span style={{ color: "#008800", fontWeight: "bold" }}>
                    return
                  </span>{" "}
                  CustomTheme({"\n"}
                  {"            "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    child:
                  </span>{" "}
                  child,
                  {"\n"}
                  {"          "});{"\n"}
                  {"        "}
                  {"}"},{"\n"}
                  {"        "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    initialDate:
                  </span>{" "}
                  selectedDate,{"\n"}
                  {"        "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    firstDate:
                  </span>{" "}
                  DateTime(
                  <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                    2015
                  </span>
                  ,{" "}
                  <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                    8
                  </span>
                  ),{"\n"}
                  {"        "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    lastDate:
                  </span>{" "}
                  DateTime(
                  <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                    2101
                  </span>
                  ));{"\n"}
                  {"    "}
                  <span style={{ color: "#008800", fontWeight: "bold" }}>
                    if
                  </span>{" "}
                  (picked <span style={{ color: "#333333" }}>!=</span>{" "}
                  <span style={{ color: "#008800", fontWeight: "bold" }}>
                    null
                  </span>{" "}
                  <span style={{ color: "#333333" }}>&amp;&amp;</span> picked{" "}
                  <span style={{ color: "#333333" }}>!=</span> selectedDate)
                  {"\n"}
                  {"      "}setState(() {"{"}
                  {"\n"}
                  {"        "}print(picked);{"\n"}
                  {"        "}selectedDate{" "}
                  <span style={{ color: "#333333" }}>=</span> picked;
                  {"\n"}
                  {"      "}
                  {"}"});{"\n"}
                  {"  "}
                  {"}"}
                  {"\n"}ListTile({"\n"}
                  {"  "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    onTap:
                  </span>{" "}
                  () {"{"}
                  {"\n"} _selectDate(context);{"\n"} {"}"},{"\n"});{"\n"}
                </pre>
              </div>
            </Typography>
          </Container>
        </div>
      </div>
      <hr />
      <div className="row">
        <div className="col-lg-6">
          <img src={DT_3} alt="DT_3" />
          <img src={DT_4} alt="DT_4" />
        </div>
        <div className="col-lg-6">
          <h5>1. This example is a Time Picker with Calender Icon.</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <SyntaxHighlighter
                language="dart"
                style={materialDark}
                wrapLines={true}
              >
                Card( elevation: 4, child: ListTile( onTap: () &braces;
                _selectTime(context); &braces;, title: Text( 'Select your
                checking time', style: TextStyle(), ), subtitle: Text(
                "$&braces;selectedTime.hour &lt; 10 ?
                "0$&braces;selectedTime.hour&braces;" :
                "$&braces;selectedTime.hour&braces;"&braces; :
                $&braces;selectedTime.minute &lt; 10 ?
                "0$&braces;selectedTime.minute&braces;" :
                "$&braces;selectedTime.minute&braces;"&braces;
                $&braces;selectedTime.period != DayPeriod.am ? 'PM' :
                'AM'&braces; ", style: TextStyle(), ), trailing: IconButton(
                icon: Icon( Icons.date_range, color: Color, ), onPressed: ()
                &braces; _selectDate(context); &braces;, ), )),
              </SyntaxHighlighter>
            </Typography>
          </Container>

          <h5>
            2. In this example a local variable selectedTime is initialized to
            TimeOfDay.now().
          </h5>
          <h5>3. A custom Method _selectTime is passed to onTap props.</h5>

          <Container className="m-0">
            <Typography component="div" style={{ height: "50%" }}>
              <div
                style={{
                  background: "#ffffff",
                  overflow: "auto",
                  width: "auto",
                  border: "solid gray",
                  borderWidth: ".1em .1em .1em .8em",
                  padding: ".2em .6em",
                }}
              >
                <pre style={{ margin: 0, lineHeight: "125%" }}>
                  TimeOfDay selectedTime{" "}
                  <span style={{ color: "#333333" }}>=</span> TimeOfDay.now();
                  {"\n"}
                  {"\n"}
                  {"  "}Future<span style={{ color: "#333333" }}>&lt;</span>Null
                  <span style={{ color: "#333333" }}>&gt;</span>{" "}
                  _selectTime(BuildContext context){" "}
                  <span style={{ color: "#333333" }}>as</span>ync {"{"}
                  {"\n"}
                  {"    "}
                  <span style={{ color: "#008800", fontWeight: "bold" }}>
                    final
                  </span>{" "}
                  TimeOfDay Picked <span style={{ color: "#333333" }}>=</span>{" "}
                  await showTimePicker({"\n"}
                  {"        "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    context:
                  </span>{" "}
                  context,{"\n"}
                  {"        "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    initialTime:
                  </span>{" "}
                  selectedTime,{"\n"}
                  {"        "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    builder:
                  </span>{" "}
                  (BuildContext context, Widget child) {"{"}
                  {"\n"}
                  {"          "}
                  <span style={{ color: "#008800", fontWeight: "bold" }}>
                    return
                  </span>{" "}
                  CustomTheme({"\n"}
                  {"            "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    child:
                  </span>{" "}
                  MediaQuery({"\n"}
                  {"              "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    data:
                  </span>
                  {"\n"}
                  {"                  "}MediaQuery.of(context).copyWith(
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    alwaysUse24HourFormat:
                  </span>{" "}
                  <span style={{ color: "#008800", fontWeight: "bold" }}>
                    false
                  </span>
                  ),{"\n"}
                  {"              "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    child:
                  </span>{" "}
                  child,
                  {"\n"}
                  {"            "}),{"\n"}
                  {"          "});{"\n"}
                  {"        "}
                  {"}"});{"\n"}
                  {"\n"}
                  {"    "}
                  <span style={{ color: "#008800", fontWeight: "bold" }}>
                    if
                  </span>{" "}
                  (Picked <span style={{ color: "#333333" }}>!=</span>{" "}
                  <span style={{ color: "#008800", fontWeight: "bold" }}>
                    null
                  </span>
                  ){"\n"}
                  {"      "}setState(() {"{"}
                  {"\n"}
                  {"        "}selectedTime{" "}
                  <span style={{ color: "#333333" }}>=</span> Picked;
                  {"\n"}
                  {"      "}
                  {"}"});{"\n"}
                  {"  "}
                  {"}"}
                  {"\n"}
                  {"\n"}ListTile({"\n"}
                  {"  "}
                  <span style={{ color: "#997700", fontWeight: "bold" }}>
                    onTap:
                  </span>{" "}
                  () {"{"}
                  {"\n"} _selectTime(context);{"\n"} {"}"},{"\n"});{"\n"}
                </pre>
              </div>
            </Typography>
          </Container>
        </div>
      </div>
      <hr />
    </div>
  );
};

export default DateTime;
