import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import RD_1 from "../../../assets/Input_Selection/RD_1.png";
import RD_2 from "../../../assets/Input_Selection/RD_2.png";
import RD_3 from "../../../assets/Input_Selection/RD_3.png";
import RD_4 from "../../../assets/Input_Selection/RD_4.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const Radio = (props: Props) => {
  return (
    <div className="container m-0">
      <h2>
        <u>Radio Button</u>
      </h2>

      <img src={RD_1} alt="RD_1" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            Theme( data: Theme.of(context).copyWith(unselectedWidgetColor:
            Color), child: Radio( value: 'Male', groupValue: gender, onChanged:
            (value) &braces; setState(() &braces; gender = value; &braces;);
            &braces;, ), ), Text('Male', style: TextStyle()), Theme( data:
            Theme.of(context).copyWith( unselectedWidgetColor: Color, ), child:
            Radio( value: 'Female', groupValue: gender, onChanged: (value)
            &braces; setState(() &braces; gender = value; &braces;); &braces;,
            ), ), Text('Female', style: TextStyle()),
          </SyntaxHighlighter>
        </Typography>
      </Container>

      <h5>
        1. In this example a local variable gender is declared and assigned in
        groupValue prop.
      </h5>

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <div
            style={{
              background: "#ffffff",
              overflow: "auto",
              width: "auto",
              border: "solid gray",
              borderWidth: ".1em .1em .1em .8em",
              padding: ".2em .6em",
            }}
          >
            <pre style={{ margin: 0, lineHeight: "125%" }}>
              <span style={{ color: "#008800", fontWeight: "bold" }}>var</span>{" "}
              gender;
              {"\n"}Radio({"\n"}
              {"  "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                groupValue:
              </span>{" "}
              gender,{"\n"}
              {"    "}),{"\n"}
            </pre>
          </div>
        </Typography>
      </Container>
      <hr />

      <img src={RD_2} alt="RD_2" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            Card( margin: EdgeInsets.only(top: 10), elevation: 4, clipBehavior:
            Clip.antiAliasWithSaveLayer, child: Theme( data:
            Theme.of(context).copyWith( unselectedWidgetColor: Color, ), child:
            RadioListTile( title: Text( 'Radio Button Tile', style: TextStyle(),
            ), subtitle: Text( 'Simple Radio button tile with title and
            subtitle', style: TextStyle(), ), value: 'Radio button tile',
            groupValue: gender, onChanged: (value) &braces; setState(() &braces;
            gender = value; &braces;); &braces;), ), ),
          </SyntaxHighlighter>
        </Typography>
      </Container>
      <hr />

      <img src={RD_3} alt="RD_3" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            Card( elevation: 4, margin: EdgeInsets.only(top: 10), clipBehavior:
            Clip.antiAliasWithSaveLayer, child: Theme( data:
            Theme.of(context).copyWith( unselectedWidgetColor: Color, ), child:
            RadioListTile( controlAffinity: ListTileControlAffinity.trailing,
            title: Text( 'Radio Button Tile', style: TextStyle(), ), subtitle:
            Text( 'Radio Button on the trailing side', style: TextStyle(), ),
            value: 'Radio Button on the trailing side', groupValue: gender,
            onChanged: (value) &braces; setState(() &braces; gender = value;
            &braces;); &braces;), ), ),
          </SyntaxHighlighter>
        </Typography>
      </Container>
      <hr />

      <img src={RD_4} alt="RD_4" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            Card( margin: EdgeInsets.only(top: 10), elevation: 4, clipBehavior:
            Clip.antiAliasWithSaveLayer, child: Theme( data:
            Theme.of(context).copyWith( unselectedWidgetColor: Color, ), child:
            RadioListTile( controlAffinity: ListTileControlAffinity.trailing,
            secondary: Container( height: 50, width: 50, decoration:
            BoxDecoration( image: DecorationImage( image: Image.asset(
            'profile.png', ).image), shape: BoxShape.circle, ), ), title: Text(
            'Radio Button Tile', style: TextStyle(), ), subtitle: Text( 'Radio
            Button Tile with leading and trailing side.', style: TextStyle(), ),
            value: 'Female', groupValue: gender, onChanged: (value) &braces;
            setState(() &braces; gender = value; &braces;); &braces;), ), ),
          </SyntaxHighlighter>
        </Typography>
      </Container>
    </div>
  );
};

export default Radio;
