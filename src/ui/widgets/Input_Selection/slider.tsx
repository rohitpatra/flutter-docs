import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import SL_1 from "../../../assets/Input_Selection/SL_1.png";
import SL_2 from "../../../assets/Input_Selection/SL_2.png";
import SL_3 from "../../../assets/Input_Selection/SL_3.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const Slider = (props: Props) => {
  return (
    <div className="container m-0">
      <h2>
        <u>Radio Button</u>
      </h2>

      <img src={SL_1} alt="SL_1" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            Slider( min: 0, max: 100, value: _value, onChanged: (value) &braces;
            setState(() &braces; _value = value; &braces;); &braces;, )
          </SyntaxHighlighter>
        </Typography>
      </Container>

      <h5>
        1. In this example a local variable _value is declared and assigned in
        value prop.
      </h5>

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <div
            style={{
              background: "#ffffff",
              overflow: "auto",
              width: "auto",
              border: "solid gray",
              borderWidth: ".1em .1em .1em .8em",
              padding: ".2em .6em",
            }}
          >
            <pre style={{ margin: 0, lineHeight: "125%" }}>
              <span style={{ color: "#333399", fontWeight: "bold" }}>
                double
              </span>{" "}
              _value <span style={{ color: "#333333" }}>=</span>{" "}
              <span style={{ color: "#6600EE", fontWeight: "bold" }}>0.33</span>
              ;{"\n"}
              Slider({"\n"}
              {"  "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                value:
              </span>{" "}
              _value,
              {"\n"});{"\n"}
            </pre>
          </div>
        </Typography>
      </Container>
      <hr />

      <img src={SL_2} alt="SL_2" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            SliderTheme( data: SliderTheme.of(context).copyWith(
            activeTrackColor: Colors.red[700], inactiveTrackColor:
            Colors.red[100], trackShape: RectangularSliderTrackShape(),
            trackHeight: 4.0, thumbColor: Colors.redAccent, thumbShape:
            RoundSliderThumbShape(enabledThumbRadius: 12.0), overlayColor:
            Colors.red.withAlpha(32), overlayShape:
            RoundSliderOverlayShape(overlayRadius: 28.0), ), child: Slider(
            label: 'Hello', value: _value, onChanged: (value) &braces;
            setState(() &braces; _value = value; &braces;); &braces;, ), ),
          </SyntaxHighlighter>
        </Typography>
      </Container>

      <h5>
        1. In this example a local variable _value is declared and assigned in
        value prop with custom colors.
      </h5>

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <div
            style={{
              background: "#ffffff",
              overflow: "auto",
              width: "auto",
              border: "solid gray",
              borderWidth: ".1em .1em .1em .8em",
              padding: ".2em .6em",
            }}
          >
            <pre style={{ margin: 0, lineHeight: "125%" }}>
              <span style={{ color: "#333399", fontWeight: "bold" }}>
                double
              </span>{" "}
              _value <span style={{ color: "#333333" }}>=</span>{" "}
              <span style={{ color: "#6600EE", fontWeight: "bold" }}>0.33</span>
              ;{"\n"}
              Slider({"\n"}
              {"  "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                value:
              </span>{" "}
              _value,
              {"\n"});{"\n"}
            </pre>
          </div>
        </Typography>
      </Container>

      <hr />

      <img src={SL_3} alt="SL_3" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            SliderTheme( data: SliderTheme.of(context).copyWith(
            activeTrackColor: Colors.orange[700], inactiveTrackColor:
            Colors.orange[100], trackShape: RoundedRectSliderTrackShape(),
            trackHeight: 4.0, thumbShape:
            RoundSliderThumbShape(enabledThumbRadius: 12.0), thumbColor:
            Colors.orangeAccent, overlayColor: Colors.red.withAlpha(32),
            overlayShape: RoundSliderOverlayShape(overlayRadius: 28.0),
            tickMarkShape: RoundSliderTickMarkShape(), activeTickMarkColor:
            Colors.orange[700], inactiveTickMarkColor: Colors.orange[100],
            valueIndicatorShape: PaddleSliderValueIndicatorShape(),
            valueIndicatorColor: Colors.orangeAccent, valueIndicatorTextStyle:
            TextStyle( color: Colors.white, ), ), child: RangeSlider( values:
            _currentRangeValues, min: 0, max: 100, divisions: 100, labels:
            RangeLabels( _currentRangeValues.start.round().toString(),
            _currentRangeValues.end.round().toString(), ), onChanged:
            (RangeValues values) &braces; setState(() &braces;
            _currentRangeValues = values; &braces;); &braces;, ), ),
          </SyntaxHighlighter>
        </Typography>
      </Container>

      <h5>
        1. In this example a local variable _currentRangeValues is declared and
        assigned in value prop with custom colors.
      </h5>

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <div
            style={{
              background: "#ffffff",
              overflow: "auto",
              width: "auto",
              border: "solid gray",
              borderWidth: ".1em .1em .1em .8em",
              padding: ".2em .6em",
            }}
          >
            <pre style={{ margin: 0, lineHeight: "125%" }}>
              RangeValues _currentRangeValues{" "}
              <span style={{ color: "#333333" }}>=</span>{" "}
              <span style={{ color: "#008800", fontWeight: "bold" }}>
                const
              </span>{" "}
              RangeValues(
              <span style={{ color: "#6600EE", fontWeight: "bold" }}>
                20
              </span>,{" "}
              <span style={{ color: "#6600EE", fontWeight: "bold" }}>100</span>
              );{"\n"}
              {"\n"}Slider({"\n"}
              {"  "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                value:
              </span>{" "}
              _currentRangeValues,
              {"\n"});{"\n"}
            </pre>
          </div>
        </Typography>
      </Container>
      <hr />
    </div>
  );
};

export default Slider;
