import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Switch_1 from "../../../assets/Input_Selection/Switch_1.png";
import Switch_2 from "../../../assets/Input_Selection/Switch_2.png";
import Switch_3 from "../../../assets/Input_Selection/Switch_3.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const Switch = (props: Props) => {
  return (
    <div className="container m-0">
      <h2>
        <u>Switch</u>
      </h2>

      <img src={Switch_1} alt="Switch_1" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            Switch( value: isSwitched, onChanged: (value) &braces; setState(()
            &braces; isSwitched = value; print(isSwitched); &braces;); &braces;,
            activeTrackColor: Colors.lightGreenAccent, activeColor:
            Colors.green, ),
          </SyntaxHighlighter>
        </Typography>
      </Container>

      <h5>
        1. In this example a local variable isSwitched is declared and assigned
        in value prop.
      </h5>

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <div
            style={{
              background: "#f8f8f8",
              overflow: "auto",
              width: "auto",
              border: "solid gray",
              borderWidth: ".1em .1em .1em .8em",
              padding: ".2em .6em",
            }}
          >
            <pre style={{ margin: 0, lineHeight: "125%" }}>
              {" "}
              <span style={{ color: "#B00040" }}>bool</span> isSwitched{" "}
              <span style={{ color: "#666666" }}>=</span>{" "}
              <span style={{ color: "#008000", fontWeight: "bold" }}>
                false
              </span>
              ;{"\n"}
              Switch({"                        "}
              {"\n"} <span style={{ color: "#A0A000" }}>value:</span>isSwitched,
              {"\n"}){"\n"}
            </pre>
          </div>
        </Typography>
      </Container>
      <hr />

      <img src={Switch_2} alt="Switch_2" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            Transform.scale( scale: 1.5, child: Switch( inactiveThumbImage:
            Image.asset('moon.png').image, inactiveThumbColor:
            Colors.transparent, inactiveTrackColor: Color(0xFF848B9B),
            activeThumbImage: Image.asset('sun.png').image, hoverColor:
            Colors.red, value: isSwitched, onChanged: (value) &braces;
            setState(() &braces; isSwitched = value; print(isSwitched);
            &braces;); &braces;, activeTrackColor: Colors.orangeAccent,
            activeColor: Colors.orange, ), ),
          </SyntaxHighlighter>
        </Typography>
      </Container>

      <h5>
        1. In this example a local variable isSwitched is declared and assigned
        in value prop.
      </h5>

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <div
            style={{
              background: "#f8f8f8",
              overflow: "auto",
              width: "auto",
              border: "solid gray",
              borderWidth: ".1em .1em .1em .8em",
              padding: ".2em .6em",
            }}
          >
            <pre style={{ margin: 0, lineHeight: "125%" }}>
              {" "}
              <span style={{ color: "#B00040" }}>bool</span> isSwitched{" "}
              <span style={{ color: "#666666" }}>=</span>{" "}
              <span style={{ color: "#008000", fontWeight: "bold" }}>
                false
              </span>
              ;{"\n"}
              Switch({"                        "}
              {"\n"} <span style={{ color: "#A0A000" }}>value:</span>isSwitched,
              {"\n"}){"\n"}
            </pre>
          </div>
        </Typography>
      </Container>

      <hr />

      <img src={Switch_3} alt="Switch_3" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            Row( mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Transform.scale( scale: 1.2, child: Switch( value: isSwitched,
            onChanged: (value) &braces; setState(() &braces; isSwitched = value;
            print(isSwitched); &braces;); &braces;, activeTrackColor:
            Colors.lightGreenAccent, activeColor: Colors.green, ), ),
            Transform.scale( scale: 1.7, child: Switch( value: isSwitched,
            onChanged: (value) &braces; setState(() &braces; isSwitched = value;
            print(isSwitched); &braces;); &braces;, activeTrackColor:
            Colors.lightGreenAccent, activeColor: Colors.green, ), ),
            Transform.scale( scale: 2, child: Switch( value: isSwitched,
            onChanged: (value) &braces; setState(() &braces; isSwitched = value;
            print(isSwitched); &braces;); &braces;, activeTrackColor:
            Colors.lightGreenAccent, activeColor: Colors.green, ), ), ], ),
          </SyntaxHighlighter>
        </Typography>
      </Container>

      <h5>
        1. In this example a local variable isSwitched is declared and assigned
        in value prop.
      </h5>

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <div
            style={{
              background: "#f8f8f8",
              overflow: "auto",
              width: "auto",
              border: "solid gray",
              borderWidth: ".1em .1em .1em .8em",
              padding: ".2em .6em",
            }}
          >
            <pre style={{ margin: 0, lineHeight: "125%" }}>
              {" "}
              <span style={{ color: "#B00040" }}>bool</span> isSwitched{" "}
              <span style={{ color: "#666666" }}>=</span>{" "}
              <span style={{ color: "#008000", fontWeight: "bold" }}>
                false
              </span>
              ;{"\n"}
              Switch({"                        "}
              {"\n"} <span style={{ color: "#A0A000" }}>value:</span>isSwitched,
              {"\n"}){"\n"}
            </pre>
          </div>
        </Typography>
      </Container>
      <hr />
    </div>
  );
};

export default Switch;
