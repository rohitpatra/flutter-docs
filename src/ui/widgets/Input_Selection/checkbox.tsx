import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import CB_1 from "../../../assets/Input_Selection/CB_1.png";
import CB_2 from "../../../assets/Input_Selection/CB_2.png";
import CB_3 from "../../../assets/Input_Selection/CB_3.png";
import CB_4 from "../../../assets/Input_Selection/CB_4.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const Checkbox = (props: Props) => {
  return (
    <div className="container m-0">
      <h2>
        <u>Checkbox</u>
      </h2>

      <img src={CB_1} alt="CB_1" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            Row( mainAxisSize: MainAxisSize.max, mainAxisAlignment:
            MainAxisAlignment.spaceAround, crossAxisAlignment:
            CrossAxisAlignment.start, children: [ Theme( data:
            Theme.of(context).copyWith( unselectedWidgetColor: Color, ), child:
            Checkbox( activeColor: Colors.grey, value: isChecked1, onChanged:
            (bool value) &braces; setState(() &braces; isChecked1 = value;
            &braces;); &braces;, ), ), Theme( data: Theme.of(context).copyWith(
            unselectedWidgetColor: Color, ), child: Checkbox( value: null,
            onChanged: (checked) &braces;&braces;, activeColor:
            Colors.redAccent, tristate: true, ), ), Theme( data:
            Theme.of(context).copyWith( unselectedWidgetColor: Color, ), child:
            Checkbox( value: isChecked3, onChanged: (checked) &braces;
            setState(() &braces; isChecked3 = checked; &braces;); &braces;,
            activeColor: Colors.redAccent, tristate: false, ), ), ] );
          </SyntaxHighlighter>
        </Typography>
      </Container>

      <h5>
        1. In this example a local boolean variable isChecked is declared and
        assigned in onChanged method.
      </h5>

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <div
            style={{
              background: "#ffffff",
              overflow: "auto",
              width: "auto",
              border: "solid gray",
              borderWidth: ".1em .1em .1em .8em",
              padding: ".2em .6em",
            }}
          >
            <pre style={{ margin: 0, lineHeight: "125%" }}>
              <span style={{ color: "#333399", fontWeight: "bold" }}>bool</span>{" "}
              isChecked <span style={{ color: "#333333" }}>=</span>{" "}
              <span style={{ color: "#008800", fontWeight: "bold" }}>
                false
              </span>
              ;{"\n"}
              {"  "}
              {"\n"}Checkbox({"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                value:
              </span>{" "}
              isChecked,{"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                onChanged:
              </span>{" "}
              (
              <span style={{ color: "#333399", fontWeight: "bold" }}>bool</span>{" "}
              value) {"{"}
              {"\n"}
              {"      "}setState(() {"{"}
              {"\n"}
              {"        "}isChecked <span style={{ color: "#333333" }}>=</span>{" "}
              value;
              {"\n"}
              {"            "}
              {"}"});{"\n"}
              {"          "}
              {"}"},{"\n"}
              {"        "}),{"\n"}
            </pre>
          </div>
        </Typography>
      </Container>
      <hr />

      <img src={CB_2} alt="CB_2" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            Row( mainAxisSize: MainAxisSize.max, mainAxisAlignment:
            MainAxisAlignment.spaceAround, crossAxisAlignment:
            CrossAxisAlignment.center, children: [ Container( width: 25, height:
            25, decoration: new BoxDecoration( color: isChecked1 == true ? Color
            : Colors.transparent, border: Border.all( width: 1, color: Color, ),
            borderRadius: new BorderRadius.circular(5), ), child: Theme( data:
            ThemeData( unselectedWidgetColor: Colors.transparent, ), child:
            Checkbox( value: isChecked1, onChanged: (state) &braces; setState(()
            &braces; return isChecked1 = state; &braces;); &braces;,
            activeColor: Colors.transparent, checkColor: Colors.white,
            materialTapTargetSize: MaterialTapTargetSize.padded, ), ), ),
            Container( width: 25, height: 25, decoration: new BoxDecoration(
            border: Border.all( width: 1, color: Color, ), borderRadius: new
            BorderRadius.circular(20), ), child: Theme( data: ThemeData(
            unselectedWidgetColor: Colors.transparent, ), child: Checkbox(
            value: isChecked2, onChanged: (state) &braces; setState(() &braces;
            return isChecked2 = state; &braces;); &braces;, activeColor:
            Colors.transparent, checkColor: Colors.black, materialTapTargetSize:
            MaterialTapTargetSize.padded, ), ), ), Container( width: 25, height:
            25, decoration: new BoxDecoration( border: Border.all( width: 1,
            color: Color, ), borderRadius: new BorderRadius.circular(10), ),
            child: Theme( data: ThemeData( unselectedWidgetColor:
            Colors.transparent, ), child: Checkbox( value: isChecked3,
            onChanged: (state) &braces; setState(() &braces; return isChecked3 =
            state; &braces;); &braces;, activeColor: Colors.transparent,
            checkColor: Colors.black, materialTapTargetSize:
            MaterialTapTargetSize.padded, ), ), ), ], ),
          </SyntaxHighlighter>
        </Typography>
      </Container>

      <h5>
        1. In this example a local boolean variable isChecked is declared and
        assigned in onChanged method.
      </h5>

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <div
            style={{
              background: "#ffffff",
              overflow: "auto",
              width: "auto",
              border: "solid gray",
              borderWidth: ".1em .1em .1em .8em",
              padding: ".2em .6em",
            }}
          >
            <pre style={{ margin: 0, lineHeight: "125%" }}>
              <span style={{ color: "#333399", fontWeight: "bold" }}>bool</span>{" "}
              isChecked <span style={{ color: "#333333" }}>=</span>{" "}
              <span style={{ color: "#008800", fontWeight: "bold" }}>
                false
              </span>
              ;{"\n"}
              {"  "}
              {"\n"}Checkbox({"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                value:
              </span>{" "}
              isChecked,{"\n"}
              {"      "}
              <span style={{ color: "#997700", fontWeight: "bold" }}>
                onChanged:
              </span>{" "}
              (
              <span style={{ color: "#333399", fontWeight: "bold" }}>bool</span>{" "}
              value) {"{"}
              {"\n"}
              {"      "}setState(() {"{"}
              {"\n"}
              {"        "}isChecked <span style={{ color: "#333333" }}>=</span>{" "}
              value;
              {"\n"}
              {"            "}
              {"}"});{"\n"}
              {"          "}
              {"}"},{"\n"}
              {"        "}),{"\n"}
            </pre>
          </div>
        </Typography>
      </Container>
      <hr />

      <img src={CB_3} alt="CB_3" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            Container( margin: EdgeInsets.only(bottom: 5), child: Card(
            elevation: 4, child: Theme( data: Theme.of(context).copyWith(
            unselectedWidgetColor: Color, ), child: CheckboxListTile( value:
            isChecked, checkColor: Color, secondary: Container( height: 40,
            width: 40, decoration: BoxDecoration( image: DecorationImage( image:
            Image.asset( 'profile.png', ).image), shape: BoxShape.circle, ), ),
            onChanged: (checked) &braces; setState(() &braces; isChecked =
            checked; &braces;); &braces;, title: Text( 'Checkbox Tile', style:
            TextStyle(size: 18), ), subtitle: Text( 'Custom Leading value ',
            style: TextStyle(), ), ), ), ), ),
          </SyntaxHighlighter>
        </Typography>
      </Container>
      <hr />
      <img src={CB_4} alt="CB_4" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            Container( margin: EdgeInsets.only(bottom: 5), child: Card(
            elevation: 4, child: Theme( data: Theme.of(context).copyWith(
            unselectedWidgetColor: Color, ), child: CheckboxListTile(
            checkColor: Color, controlAffinity: ListTileControlAffinity.leading,
            secondary: Icon( Icons.subscriptions, color: appStore.iconColor, ),
            value: isChecked, onChanged: (checked) &braces; setState(() &braces;
            isChecked = checked; &braces;); &braces;, title: Text( 'Checkbox
            Tile', style: TextStyle(size: 18), ), subtitle: Text( 'Custom
            Trailing value', style: TextStyle(), ), ), ), ), ),{" "}
          </SyntaxHighlighter>
        </Typography>
      </Container>
      <hr />
    </div>
  );
};

export default Checkbox;
