import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import RTF_1 from "../../../../assets/Input_Selection/RTF_1.png";
import RTF_2 from "../../../../assets/Input_Selection/RTF_2.png";
import RTF_3 from "../../../../assets/Input_Selection/RTF_3.png";
import RTF_4 from "../../../../assets/Input_Selection/RTF_4.png";
import RTF_5 from "../../../../assets/Input_Selection/RTF_5.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const Rounded = (props: Props) => {
  return (
    <div className="container m-0">
      <h2>
        <u>Rounded TextField</u>
      </h2>

      <img src={RTF_1} alt="RTF_1" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            TextField( style: TextStyle(), decoration: InputDecoration(
            labelText: "UserName", alignLabelWithHint: false, fillColor:
            Colors.blue, labelStyle: TextStyle(color: Colors.white), filled:
            true, ), cursorColor: white, keyboardType: TextInputType.text,
            textInputAction: TextInputAction.done, ),
          </SyntaxHighlighter>
        </Typography>
      </Container>
      <hr />
      <img src={RTF_2} alt="RTF_2" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            TextField( style: TextStyle(), decoration: InputDecoration(
            focusedBorder: OutlineInputBorder( borderRadius:
            BorderRadius.circular(25.0), borderSide: BorderSide(color: Color),
            ), enabledBorder: OutlineInputBorder( borderRadius:
            BorderRadius.circular(25.0), borderSide: BorderSide(width: 1, color:
            Color), ), hintText: "Password", hintStyle: TextStyle(color: Color),
            alignLabelWithHint: false, filled: true), cursorColor: isDarkModeOn
            ? white : blackColor, obscureText: true, keyboardType:
            TextInputType.text, textInputAction: TextInputAction.done, ),
          </SyntaxHighlighter>
        </Typography>
      </Container>
      <hr />
      <img src={RTF_3} alt="RTF_3" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            TextField( style: TextStyle(), decoration: InputDecoration(
            focusedBorder: OutlineInputBorder( borderRadius:
            BorderRadius.circular(10.0), borderSide: BorderSide(color: Color),
            ), enabledBorder: OutlineInputBorder( borderRadius:
            BorderRadius.circular(10.0), borderSide: BorderSide(width: 1, color:
            Color), ), hintText: "New Phone Number", hintStyle: TextStyle(color:
            Color), labelStyle: TextStyle(color: Color), labelText: "New Phone
            Number", alignLabelWithHint: false, filled: true, ), cursorColor:
            isDarkModeOn ? white : blackColor, keyboardType:
            TextInputType.phone, textInputAction: TextInputAction.done, ),
          </SyntaxHighlighter>
        </Typography>
      </Container>
      <hr />
      <img src={RTF_4} alt="RTF_4" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            TextField( style: TextStyle(), cursorColor: Colors.white,
            decoration: InputDecoration( border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0), ), hintText: "Address",
            hintStyle: TextStyle(color: Colors.white30), alignLabelWithHint:
            false, fillColor: Colors.blue, filled: true, ), textInputAction:
            TextInputAction.done, ),
          </SyntaxHighlighter>
        </Typography>
      </Container>
      <hr />
      <img src={RTF_5} alt="RTF_5" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            TextField( style: TextStyle(), decoration: InputDecoration(
            focusedBorder: OutlineInputBorder( borderRadius:
            BorderRadius.circular(25.0), borderSide: BorderSide(color: Color),
            ), enabledBorder: OutlineInputBorder( borderRadius:
            BorderRadius.circular(25.0), borderSide: BorderSide(width: 1, color:
            Color), ), labelText: "Ticket Form", hintText: "write....",
            hintStyle: TextStyle(color: Color), labelStyle: TextStyle(color:
            Color), alignLabelWithHint: true, filled: true, ), cursorColor:
            isDarkModeOn ? white : blackColor, keyboardType:
            TextInputType.multiline, maxLines: 4, textInputAction:
            TextInputAction.done, ),
          </SyntaxHighlighter>
        </Typography>
      </Container>
      <hr />
    </div>
  );
};

export default Rounded;
