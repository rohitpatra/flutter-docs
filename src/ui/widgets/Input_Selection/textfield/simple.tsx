import React from "react";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import dart from "react-syntax-highlighter/dist/esm/languages/prism/dart";
import { materialDark } from "react-syntax-highlighter/dist/esm/styles/prism";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import TF_1 from "../../../../assets/Input_Selection/TF_1.png";
import TF_2 from "../../../../assets/Input_Selection/TF_2.png";
import TF_3 from "../../../../assets/Input_Selection/TF_3.png";
import TF_4 from "../../../../assets/Input_Selection/TF_4.png";
import TF_5 from "../../../../assets/Input_Selection/TF_5.png";

interface Props {}

SyntaxHighlighter.registerLanguage("dart", dart);

const Simple = (props: Props) => {
  return (
    <div className="container m-0">
      <h2>
        <u>Simple TextField</u>
      </h2>

      <img src={TF_1} alt="TF_1" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            TextField( style: TextStyle(), decoration: InputDecoration(
            focusedBorder: UnderlineInputBorder( borderSide: BorderSide(color:
            Color), ), enabledBorder: UnderlineInputBorder( borderSide:
            BorderSide(color: Color), ), labelText: 'Username', labelStyle:
            TextStyle(size: 14), filled: true, ), cursorColor: isDarkModeOn ?
            white : blackColor, keyboardType: TextInputType.text,
            textInputAction: TextInputAction.done, ),
          </SyntaxHighlighter>
        </Typography>
      </Container>
      <hr />
      <img src={TF_2} alt="TF_2" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            TextField( style: TextStyle(), decoration: InputDecoration(
            focusedBorder: UnderlineInputBorder( borderSide: BorderSide(color:
            Color), ), enabledBorder: UnderlineInputBorder( borderSide:
            BorderSide(color: Color), ), labelText: "E-Mail", labelStyle:
            TextStyle(size: 14), prefixIcon: Icon( Icons.email, color: Color, ),
            filled: true, ), cursorColor: isDarkModeOn ? white : blackColor,
            keyboardType: TextInputType.emailAddress, textInputAction:
            TextInputAction.done, ),
          </SyntaxHighlighter>
        </Typography>
      </Container>
      <hr />
      <img src={TF_3} alt="TF_3" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            TextField( style: TextStyle(), obscureText: passwordVisible,
            textInputAction: TextInputAction.done, decoration: InputDecoration(
            labelText: 'Password', labelStyle: TextStyle(size: 14),
            focusedBorder: UnderlineInputBorder( borderSide: BorderSide(color:
            Color), ), enabledBorder: UnderlineInputBorder( borderSide:
            BorderSide(color: Color), ), filled: true, suffixIcon: IconButton(
            icon: Icon( passwordVisible ? Icons.visibility :
            Icons.visibility_off, color: Color, ), onPressed: () &braces;
            setState(() &braces; passwordVisible = !passwordVisible; &braces;);
            &braces;, )), cursorColor: isDarkModeOn ? white : blackColor, ),
          </SyntaxHighlighter>
        </Typography>
      </Container>

      <h5>
        1. In this example a local variable passwordVisible is declared and
        assigned in obscureText prop.
      </h5>

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <div
            style={{
              background: "#f8f8f8",
              overflow: "auto",
              width: "auto",
              border: "solid gray",
              borderWidth: ".1em .1em .1em .8em",
              padding: ".2em .6em",
            }}
          >
            <pre style={{ margin: 0, lineHeight: "125%" }}>
              {"  "}
              <span style={{ color: "#B00040" }}>
                bool
              </span> passwordVisible{" "}
              <span style={{ color: "#666666" }}>=</span>{" "}
              <span style={{ color: "#008000", fontWeight: "bold" }}>
                false
              </span>
              ;{"\n"}
              {"  "}TextField({"\n"}
              {"    "}
              <span style={{ color: "#A0A000" }}>obscureText:</span>{" "}
              passwordVisible,
              {"\n"}
              {"   "});{"\n"}
            </pre>
          </div>
        </Typography>
      </Container>
      <hr />
      <img src={TF_4} alt="TF_4" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            TextField( style: TextStyle(), decoration: InputDecoration(
            focusedBorder: UnderlineInputBorder( borderSide: BorderSide(color:
            Color), ), enabledBorder: UnderlineInputBorder( borderSide:
            BorderSide(color: Color), ), labelText: "Phone Number", labelStyle:
            TextStyle(size: 14), prefixIcon: Icon(Icons.phone, color:
            Colors.blue), filled: true, ), cursorColor: isDarkModeOn ? white :
            blackColor, keyboardType: TextInputType.number, textInputAction:
            TextInputAction.done, ),
          </SyntaxHighlighter>
        </Typography>
      </Container>
      <hr />
      <img src={TF_5} alt="TF_5" />

      <Container className="m-0">
        <Typography component="div" style={{ height: "50%" }}>
          <SyntaxHighlighter
            language="dart"
            style={materialDark}
            wrapLines={true}
          >
            TextField( style: TextStyle(), decoration: InputDecoration(
            focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color:
            Color)), enabledBorder: UnderlineInputBorder(borderSide:
            BorderSide(color: Color)), labelText: "Pincode", labelStyle:
            TextStyle(size: 14), filled: true, counterStyle: TextStyle(), ),
            maxLength: 6, cursorColor: isDarkModeOn ? white : blackColor,
            keyboardType: TextInputType.number, textInputAction:
            TextInputAction.done, ),
          </SyntaxHighlighter>
        </Typography>
      </Container>
      <hr />
    </div>
  );
};

export default Simple;
