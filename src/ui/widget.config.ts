const widgetConfig = {
  widgets: [
    {
      id: "app_bar",
      title: "AppBar",
      component: "AppBar",
    },
    {
      id: "bottom_navigation_bar",
      title: "Bottom Navigation Bar",
      component: "BottomNaviagtionBar",
    },
    {
      id: "drawer",
      title: "Drawer",
      component: "Drawer",
    },
  ],
};

export default widgetConfig;
