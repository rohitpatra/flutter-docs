export interface WidgetConfig {
  id: string;
  title: string;
  component: string;
}
