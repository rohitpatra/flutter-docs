import React from 'react';

const AppBarComponent = React.lazy(() => import('./ui/widgets/appbar'));
const BottomNavBarComponent = React.lazy(() => import('./ui/widgets/bottomNavBar'));
const DateTimeComponent = React.lazy(() => import('./ui/widgets/Input_Selection/date_time'));
const DrawerComponent = React.lazy(() => import('./ui/widgets/drawer'));
const SliverAppBarComponent = React.lazy(() => import('./ui/widgets/sliverAppBar'));
const TabBarComponent = React.lazy(() => import('./ui/widgets/tabbar'));
const DropdownComponent = React.lazy(() => import('./ui/widgets/Buttons/dropdown'));
const MaterialButtonsComponent = React.lazy(() => import('./ui/widgets/Buttons/materialbuttons'));
const OutlineButtonsComponent = React.lazy(() => import('./ui/widgets/Buttons/outlinebuttons'));
const PopUpMenuButtonsComponent = React.lazy(() => import('./ui/widgets/Buttons/popupmenu'));
const FloatingActionButtonsComponent = React.lazy(() => import('./ui/widgets/Buttons/floatingactionbuttons'));
const IconButtonsComponent = React.lazy(() => import('./ui/widgets/Buttons/iconbuttons'));
const CheckboxComponent = React.lazy(() => import('./ui/widgets/Input_Selection/checkbox'));
const RadioComponent = React.lazy(() => import('./ui/widgets/Input_Selection/radio'));
const SliderComponent = React.lazy(() => import('./ui/widgets/Input_Selection/slider'));
const SwitchComponent = React.lazy(() => import('./ui/widgets/Input_Selection/switch'));
const SimpleComponent = React.lazy(() => import('./ui/widgets/Input_Selection/textfield/simple'));
const RoundedComponent = React.lazy(() => import('./ui/widgets/Input_Selection/textfield/rounded'));
const AlertDialogComponent = React.lazy(() => import('./ui/widgets/Dialog_Alert_Panels/alertdialog'));
const BottomSheetComponent = React.lazy(() => import('./ui/widgets/Dialog_Alert_Panels/bottomsheet'));
const ExpansionPanelComponent = React.lazy(() => import('./ui/widgets/Dialog_Alert_Panels/expansionpanel'));
const SnackBarsComponent = React.lazy(() => import('./ui/widgets/Dialog_Alert_Panels/snackbar'));
const CardsComponent = React.lazy(() => import('./ui/widgets/Information_Display/card'));
const ChipsComponent = React.lazy(() => import('./ui/widgets/Information_Display/chips'));
const ProgressBarComponent = React.lazy(() => import('./ui/widgets/Information_Display/progressbar'));
const DataTableComponent = React.lazy(() => import('./ui/widgets/Information_Display/datatable'));
const GridViewComponent = React.lazy(() => import('./ui/widgets/Information_Display/gridview'));
const ListViewComponent = React.lazy(() => import('./ui/widgets/Information_Display/listview'));
const RichTextComponent = React.lazy(() => import('./ui/widgets/Information_Display/icons'));
const NotFoundComponent = React.lazy(() => import('./ui/notfound'));


const routes = [
    { path: '/AppBar', exact: true, name: 'AppBar', component: AppBarComponent },
    { path: '/BottomNavBar', exact: true, name: 'BottomNavBar', component: BottomNavBarComponent },
    { path: '/DateTime', exact: true, name: 'DateTime', component: DateTimeComponent },
    { path: '/Drawer', exact: true, name: 'Drawer', component: DrawerComponent },
    { path: '/SliverAppBar', exact: true, name: 'SliverAppBar', component: SliverAppBarComponent },
    { path: '/TabBar', exact: true, name: 'TabBar', component: TabBarComponent },
    { path: '/Dropdown', exact: true, name: 'Dropdown', component: DropdownComponent },
    { path: '/MaterialButtons', exact: true, name: 'MaterialButtons', component: MaterialButtonsComponent },
    { path: '/OutlineButtons', exact: true, name: 'OutlineButtons', component: OutlineButtonsComponent },
    { path: '/PopUpMenuButtons', exact: true, name: 'PopUpMenuButtons', component: PopUpMenuButtonsComponent },
    { path: '/FloatingActionButtons', exact: true, name: 'FloatingActionButtons', component: FloatingActionButtonsComponent },
    { path: '/IconButtons', exact: true, name: 'IconButtons', component: IconButtonsComponent },
    { path: '/Checkbox', exact: true, name: 'Checkbox', component: CheckboxComponent },
    { path: '/Radio', exact: true, name: 'Radio', component: RadioComponent },
    { path: '/Slider', exact: true, name: 'Slider', component: SliderComponent },
    { path: '/Switch', exact: true, name: 'Switch', component: SwitchComponent },
    { path: '/Simple', exact: true, name: 'Simple', component: SimpleComponent },
    { path: '/Rounded', exact: true, name: 'Rounded', component: RoundedComponent },
    { path: '/AlertDialog', exact: true, name: 'AlertDialog', component: AlertDialogComponent },
    { path: '/BottomSheet', exact: true, name: 'BottomSheet', component: BottomSheetComponent },
    { path: '/ExpansionPanel', exact: true, name: 'ExpansionPanel', component: ExpansionPanelComponent },
    { path: '/SnackBars', exact: true, name: 'SnackBars', component: SnackBarsComponent },
    { path: '/Cards', exact: true, name: 'Cards', component: CardsComponent },
    { path: '/Chips', exact: true, name: 'Chips', component: ChipsComponent },
    { path: '/ProgressBar', exact: true, name: 'ProgressBar', component: ProgressBarComponent },
    { path: '/DataTable', exact: true, name: 'DataTable', component: DataTableComponent },
    { path: '/GridView', exact: true, name: 'GridView', component: GridViewComponent },
    { path: '/ListView', exact: true, name: 'ListView', component: ListViewComponent },
    { path: '/RichText', exact: true, name: 'RichText', component: RichTextComponent },
    // { path: '/', exact: true, name: 'Home', component: NotFoundComponent },
];

export default routes;
